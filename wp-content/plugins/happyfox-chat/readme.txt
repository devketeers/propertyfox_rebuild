=== HappyFox Chat - Live Chat Plugin for Wordpress Websites ===
Contributors: hfsupport
Tags: banckle, Chat, chat online, chat software, click desk, clickdesk, contact plugin, contact us,customer support, IM Chat, live chat, live chat inc, live chat services, live chat software, live chatting, live help, live support, live web chat, livechat, olark, online chat, online support, php live chat, snapengage, support software, Website Chat, WordPress chat,wordpress live chat, wordpress live chat plugin, Zendesk, Zopim, Zopim live chat, HappyFox, Shopify, Pipedrive, sales chat, salesforce
Stable tag: 1.2.2
Requires at least: 3.5
Tested up to: 4.9.1

Voted No.1 Live chat software on ProductHunt. Fully loaded with features like unlimited chats, fully customizable widget, app integrations & more.

== Description ==
Convert anonymous visitors on your website to leads with HappyFox Chat - Live chat software for the next generation. Improve conversion rates 10X on your ecommerce store, by integrating with third party apps like Shopify, Google Analytics & more. Sign up Free here - [https://www.happyfoxchat.com](https://www.happyfoxchat.com)

**Why HappyFox Chat?**

Live chat is not just an “engagement” tool anymore. It’s a business to customer communication tool that can help you with customer service, marketing or sales! Here’s how HappyFox Chat is different and was voted as No.1 live chat tool on ProductHunt.

- Free for 14 days. No credit card required.
- Power packed features that helps you support customers better and faster
- Customizable widget with uploadable profile pictures.
- Mobile optimized.
- Super light weight widget that’s easy on page load time.
- Unlimited chats.
- Automatic smart triggers on custom rules (pages, time spent on page etc)
- Easy Turn On/Off switch (No more “chat offline”)
- Chat transfers (transfer chat to other agents easily).
- Canned responses (create your own custom pre-built replies to frequent questions).
- Chat history (see past conversations easily).
- Deep integration with third party apps like Google Analytics, Shopify, WordPress & more.
- Amazing and easy to use UI.
- In depth analytics
- Free native apps (Windows, Mac)
- Free iOS, Android apps.
- Free HappyFox Chat Reporting apps (iOS, Android).
- Integrated with 20+ apps.


**As a Customer support tool**

“Customer service is not just a department, it’s everyone’s job.”

Faster chats, happier customers - HappyFox Chat is faster and responsive than any other live chat software available today! (1/4th of a second, to be precise).
Amazing UI that enables you to do more chats, easily and effectively. No more ten tabs open, everything sits in one single sleek dashboard!
Offer a personalized experience by customizing your chat widget - colors, profile pictures, custom statuses, chat window titles.. you name it, we got it!
No more “going offline”. That’s so uncool. Switch your status on/off with a simple slider button and the widget appears/disappears on your site automatically. But we’ve got offline messages, just in case.

**As a Marketing tool**

“Everybody engages, winners convert.”

While traditional live chat software let’s you “engage” with visitors, HappyFox Chat takes it several notches up, and convert them to leads easily with easy CRM integrations!
Don’t lose another opportunity. Stay connected with your visitors - Free Windows app, Mac app & Mobile Apps (iOs/Android)
Increase “sticky-ness” of your website by  Lightweight tool with super easy installation!

**As a Sales tool**

“Always be closing.”

Convert visitors to leads and feed your sales pipeline via live chat. Integrate seamlessly with the CRM of your choice like Pipedrive, Salesforce etc.
Automatically pop up chat window on high-value visitors, like pricing page visitors spending more than X minutes on the page etc with custom triggers.
Keep your sales team connected with free Windows, Mac and mobile apps
Know who you are conversing with, their entire purchase history, past conversations etc and help more people.

Support - [support@happyfoxchat.com](mailto:support@happyfoxchat.com) Or Chat at [https://www.happyfoxchat.com](https://www.happyfoxchat.com)
Twitter - [@happyfoxchat](https://twitter.com/happyfoxchat)

== Screenshots ==
1. Personalize your live chat experience with custom widgets
2. HappyFox Chat Agent interface
3. Integration details in chat widget
4. Customizable look and feel
5. Chat history
6. App store

== Installation ==

**Setup**

1. Upload happyfox-chat directory to WordPress plugins directory (/wp-content/plugins/)
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Click the ‘HappyFox Chat' menu on the left.
4. Follow the instructions

**Usage**

1. Don't have a HappyFox Chat account? [Sign up for free](https://www.happyfoxchat.com/)
2. [Sign in](https://www.happyfoxchat.com/a/home/) to HappyFox Chat, go to the Apps section.
3. Choose HappyFox Chat plugin from the list of available plugins.
4. Connect HappyFox Chat account by entering the API key in Wordpress site given in your account.
5. All set! You can now see HappyFox Chat widget live in your Wordpress site.

== Frequently Asked Questions ==

= How does WordPress integration with HappyFox Chat work on the website? =
The WordPress integration with HappyFox Chat allows you to engage with visitors on your website and provide exceptional customer support. The plugin will help you install the live chat widget on your WordPress website easily, in under 5 minutes. Once the chat widget is installed, visitors on your site will be able to ask you questions and you'll be able to chat with them via the Web app, Desktop apps or Mobile apps.

= Is this a free live chat plugin? =
This is a premium live chat plugin. HappyFox Chat has a 14-day free trial on all the paid plans. On the free trial, you can try out all the core features like unlimited chats, concurrent chats, unbranding, triggered chats, widget customization, proactive chat, canned responses, reports, app integrations etc. After the trial, based on your requirement you can upgrade to any of the plans here - https://happyfoxchat.com/live-chat-pricing


= Can I have the chat history saved in archives? =
All chat transcripts get archived, but are made available on your HappyFox Chat window based on your plan. Popular and Mighty plans accommodate chat history for 1 and 3 years respectively. The Fantastic and Enterprise plans have an unlimited chat history. You can compare the feature list and select the ideal plan to suit your needs.

= Where can I find the documentation for HappyFox Chat and WordPress integrations? =
You can read more about how HappyFox Chat integrates with WordPress here: https://happyfoxchat.com/add-live-chat-to-website/wordpress-live-chat-plugin
Technical details on integration with WordPress and the process to install HappyFox chat plugins are available here: https://support.happyfoxchat.com/kb/article/335-enable-integration-with-wordpress

= What are the other integrations offered by HappyFox Chat? =
HappyFox chat provides integrations with loads of apps. Some of the popular ones are : [HappyFox Helpdesk](https://happyfoxchat.com/live-chat-integration/happyfox-helpdesk), [Pipedrive](https://happyfoxchat.com/live-chat-integration/pipedrive-crm), [Shopify](https://happyfoxchat.com/live-chat-for-ecommerce/shopify-live-chat-app), [Slack](https://happyfoxchat.com/live-chat-integration/slack), [WordPress](https://happyfoxchat.com/add-live-chat-to-website/wordpress-live-chat-plugin),
[Facebook Messenger](https://happyfoxchat.com/live-chat-integration/live-chat-facebook-messenger),
[Zapier](https://support.happyfoxchat.com/kb/article/334-enable-integration-with-zapier), [Magento](https://happyfoxchat.com/live-chat-for-ecommerce/magento-live-chat-extension), [Zendesk](https://happyfoxchat.com/live-chat-integration/zendesk-help-desk), [MixPanel](https://happyfoxchat.com/add-live-chat-to-website/wordpress-live-chat-plugin), [Salesforce](https://happyfoxchat.com/live-chat-integration/salesforce-crm) and many more. Find our complete list of integrations here: https://happyfoxchat.com/live-chat-integration

== Other notes ==
HappyFox Chat is the next generation Live Chat software for your website that integrates deeply with apps you already use. Deep integrations makes every chat conversation contextual and relevant letting you convert more visitors to customers.
Install HappyFox Chat now to make live chat more valuable than ever before.
