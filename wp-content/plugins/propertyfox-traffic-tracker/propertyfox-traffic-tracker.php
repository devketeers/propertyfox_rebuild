<?php

    /**
     * @link              https://propertyfox.co.za
     * @since             1.0.0
     * @package           Propertyfox_Traffic_Tracker
     *
     * @wordpress-plugin
     * Plugin Name:       Propertyfox Traffic Tracker
     * Plugin URI:        https://propertyfox.co.za
     * Description:       This plugin hooks into Gravity forms and integrates the fields data with Propertybase.
     * Version:           1.0.0
     * Author:            Johan Pretorius
     * Author URI:        https://propertyfox.co.za
     * License:           GPL-2.0+
     * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
     * Text Domain:       propertyfox-traffic-tracker
     * Domain Path:       /languages
     */

    // If this file is called directly, abort.
    if (!defined('WPINC')) {
        die;
    }

    /**
     * Currently plugin version.
     * Start at version 1.0.0 and use SemVer - https://semver.org
     * Rename this for your plugin and update it as you release new versions.
     */
    define('TRAFFIC_TRACKER_VERSION', '1.0.0');

    /**
     * The code that runs during plugin activation.
     * This action is documented in includes/class-propertyfox-traffic-tracker-activator.php
     */
    function activate_propertyfox_traffic_tracker() {
        require_once plugin_dir_path(__FILE__) . 'includes/class-propertyfox-traffic-tracker-activator.php';
        Propertyfox_Traffic_Tracker_Activator::activate();
    }

    /**
     * The code that runs during plugin deactivation.
     * This action is documented in includes/class-propertyfox-traffic-tracker-deactivator.php
     */
    function deactivate_propertyfox_traffic_tracker() {
        require_once plugin_dir_path(__FILE__) . 'includes/class-propertyfox-traffic-tracker-deactivator.php';
        Propertyfox_Traffic_Tracker_Deactivator::deactivate();
    }

    register_activation_hook(__FILE__, 'activate_propertyfox_traffic_tracker');
    register_deactivation_hook(__FILE__, 'deactivate_propertyfox_traffic_tracker');

    /**
     * The core plugin class that is used to define internationalization,
     * admin-specific hooks, and public-facing site hooks.
     */
    require plugin_dir_path(__FILE__) . 'includes/class-propertyfox-traffic-tracker.php';

    /**
     * Begins execution of the plugin.
     *
     * Since everything within the plugin is registered via hooks,
     * then kicking off the plugin from this point in the file does
     * not affect the page life cycle.
     *
     * @since    1.0.0
     */
    function run_propertyfox_traffic_tracker() {
        $plugin = new Propertyfox_Traffic_Tracker();
        $plugin->run();
    }

    run_propertyfox_traffic_tracker();