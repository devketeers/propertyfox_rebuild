<?php

    /**
     * Provide a public-facing view for the plugin
     *
     * This file is used to markup the public-facing aspects of the plugin.
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/public/partials
     */
?>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
