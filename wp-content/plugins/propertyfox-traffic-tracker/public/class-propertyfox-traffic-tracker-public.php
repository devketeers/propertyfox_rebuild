<?php

    /**
     * The public-facing functionality of the plugin.
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/public
     */

    /**
     * The public-facing functionality of the plugin.
     *
     * Defines the plugin name, version, and two examples hooks for how to
     * enqueue the public-facing stylesheet and JavaScript.
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/public
     * @author     Johan Pretorius <johan@propertyfox.co.za>
     */
    class Propertyfox_Traffic_Tracker_Public {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * The data of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      mixed $pb_traffic_tracker_options The data from the options page.
         */
        private $pb_traffic_tracker_options;

        /**
         * The utm data that will be sent to Propertybase
         *
         * @since    1.0.0
         * @access   private
         * @var      mixed $getUTM GET var
         */
        private $getUTM;

        /**
         * The utm data that will be sent to Propertybase
         *
         * @since    1.0.0
         * @access   private
         * @var      mixed $postUTM POST var
         */
        public $postUTM;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         * @param    string $plugin_name The name of the plugin.
         * @param    string $version The version of this plugin.
         */
        public function __construct($plugin_name, $version) {
            $this->plugin_name = $plugin_name;
            $this->version = $version;
            $this->pb_traffic_tracker_options = get_option($this->plugin_name);
            $this->getUTM = $this->getGetUTM($_GET);
            @$this->postUTM = $this->set_transient_data($_POST['mid']);
        }

        /**
         * Register the stylesheets for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_styles() {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Propertyfox_Traffic_Tracker_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Propertyfox_Traffic_Tracker_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/propertyfox-traffic-tracker-public.css', array(), $this->version, 'all');

        }

        /**
         * Register the JavaScript for the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts() {

            /**
             * This function is provided for demonstration purposes only.
             *
             * An instance of this class should be passed to the run() function
             * defined in Propertyfox_Traffic_Tracker_Loader as all of the hooks are defined
             * in that particular class.
             *
             * The Propertyfox_Traffic_Tracker_Loader will then create the relationship
             * between the defined hooks and the functions defined in this
             * class.
             */

            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/propertyfox-traffic-tracker-public.js', array('jquery'), $this->version, false);

        }

        /**
         * This is the function we will be hooking into to send the form to Propertybase
         *
         * @since    1.0.0
         *
         * @param $entry
         * @param $form
         */
        public function post_to_third_party($entry, $form) {
            global $post;

            //Set static id field for PB form identification
            $body['h_' . $this->pb_traffic_tracker_options['form_id']] = '';

            //Set form fo PB
            $body['contact[Website_Form__c]'] = $form['title'];
            $body['contact[LeadSource]'] = 'Website'; //@todo - might get removed make dynamic

            //The fields associated with the posted form
            $formFields = $this->setup_form_data()['fields'][$entry['form_id']];

            //Build all linked fields from settings page
            foreach ($formFields as $gfKey => $field) {
                $body[$field] = rgar(str_replace("'", "", $entry), $gfKey);
            }

            //Identifies additional fields that might be needed
            $linkFields = $this->setup_form_data()['link'][$entry['form_id']];

            //Set Linked Listings fields
            if ($linkFields['unique']) {
                $body['unique_id_field'] = $linkFields['unique'];
            }

            if ($linkFields['favorite']) {
                $body['favorite_unique_ids[]'] = get_post_meta($post->ID, $linkFields['favorite'], true);
            }

            //Identifies what type of PB object this form is
            $formObject = $this->setup_form_data()['form_object'][$entry['form_id']];

            //Setup Listing additional fields
            if ($formObject == 'listing') {
                $body['listing[Name]'] = rgar($entry, 6); //@todo - needs to be dynamic
                $body['contact[Contact_Type__c]'] = 'Seller';
                $body['property_owner_contact_field'] = 'PropertyOwnerContact__c';
            }

            //Setup Enquiry additional fields
            if ($formObject == 'request') {
                $body['contact[Contact_Type__c]'] = 'Buyer';
                $body['request[Enquiry_Type__c]'] = $form['title'];

                if (get_post_type($post->ID) == 'property') {
                    $body['request[pba__PropertyType__c]'] = get_post_meta($post->ID, 'pba__propertytype__c', true);

                    $body['request[pba__Area_pb__c]'] = get_post_meta($post->ID, 'pba__area_pb__c', true);
                    $body['request[pba__City_pb__c]'] = get_post_meta($post->ID, 'pba__city_pb__c', true);

                    $body['request[Listing_Price_max__c]'] = get_post_meta($post->ID, 'listing_price__c', true);
                    $body['request[Bedrooms_max__c]'] = get_post_meta($post->ID, 'bedrooms__c', true);
                    $body['request[Bathrooms_max__c]'] = get_post_meta($post->ID, 'bathrooms__c', true);
                } else {
                    //Inheritance forms
                    $body['favorite_unique_ids[]'] = get_post_meta($this->postUTM, $linkFields['favorite'], true);
                    $body['request[pba__PropertyType__c]'] = get_post_meta($this->postUTM, 'pba__propertytype__c', true);

                    $body['request[pba__Area_pb__c]'] = get_post_meta($this->postUTM, 'pba__area_pb__c', true);
                    $body['request[pba__City_pb__c]'] = get_post_meta($this->postUTM, 'pba__city_pb__c', true);

                    $body['request[Listing_Price_max__c]'] = get_post_meta($this->postUTM, 'listing_price__c', true);
                    $body['request[Bedrooms_max__c]'] = get_post_meta($this->postUTM, 'bedrooms__c', true);
                    $body['request[Bathrooms_max__c]'] = get_post_meta($this->postUTM, 'bathrooms__c', true);
                }
            }

            /**Output Testing area*/
            //$body['entry'] = json_encode(array_merge($body, $this->set_utm_data()));
            /**Output Testing area*/

            //Merge all the collected data together and send it off.
            $bodyContent = array_merge($body, $this->set_utm_data());

            $request = new WP_Http();

            ksort($bodyContent);

            //Build the url
            $post_url = $this->setup_form_data()['form_url'] . $this->setup_form_data()['form_id'];
            //$post_url = 'http://postb.in/tLM8KZS4';

            //Send the data
            $response = $request->post($post_url, array('body' => $bodyContent));
        }

        /**
         * This is the function is used to set transient data for book a viewing form
         *
         * @since    1.0.0
         *
         * @param $postUtmMeta
         * @return mixed
         */
        public function set_transient_data($postUtmMeta) {
            if (!isset($_COOKIE['listing_last_view'])) {
                setcookie('listing_last_view', $postUtmMeta, (time() + 900), COOKIEPATH, COOKIE_DOMAIN);
            }

            if ($postUtmMeta) {
                if ($_COOKIE['listing_last_view'] != $postUtmMeta) {
                    setcookie('listing_last_view', $postUtmMeta, (time() + 900), COOKIEPATH, COOKIE_DOMAIN);
                }
            }

            return $_COOKIE['listing_last_view'];
        }

        /**
         * Setup the data we will need for all the forms
         *
         * @since    1.0.0
         *
         * @return array
         *
         * Array @fields child Key = GF Form Id
         *
         */
        public function setup_form_data() {
            $createFieldMeta = [];
            if (!is_admin()) {
                $fields = unserialize($this->pb_traffic_tracker_options['fields']);
                $createFieldMeta['form_url'] = $this->pb_traffic_tracker_options['form_url'];
                $createFieldMeta['form_id'] = $this->pb_traffic_tracker_options['form_id'];
                $createFieldMeta['utm'] = unserialize($this->pb_traffic_tracker_options['utm']);
                foreach ($fields as $id => $value) {
                    foreach ($value as $key => $val) {
                        $createFieldMeta['form_object'][$key] = $id;
                        $createFieldMeta['link'][$key] = $val['pb_link'];
                        $createFieldMeta['fields'][$key] = $this->array_combine($val['pb_object'], $val['pb_field']);
                    }
                }
            }
            return $createFieldMeta;
        }

        /**
         * Setup the utm data into a usable array
         *
         * @since    1.0.0
         *
         * @return mixed
         *
         */
        public function set_utm_data() {
            $utmFields = $this->setup_form_data()['utm'];

            $cookieData = [];
            if (is_array($this->get_utm_cookie())) {
                $cookieData = $this->get_utm_cookie();
            }

            $utm = [];
            foreach ($utmFields as $utmId => $utmField) {
                //Only build data sets if the field has been set
                if ($utmField) {
                    if ($cookieData) {
                        if (rgar($cookieData, $utmId)) {
                            $utm['contact[' . $utmField . ']'] = rgar($cookieData, $utmId);
                        }
                    } else {
                        if (rgar($this->getUTM, $utmId)) {
                            $utm['contact[' . $utmField . ']'] = rgar($this->getUTM, $utmId);
                        } else {
                            $utm['contact[' . $utmField . ']'] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        }
                    }
                }
            }
            return $utm;
        }

        /**
         * Get all of the UTM data and return it to create the cookie for the user session.
         *
         * @since    1.0.0
         *
         * @param $getUtmMeta
         * @return mixed
         *
         */
        public function getGetUTM($getUtmMeta) {
            //Marketing sources
            $source = [
                'Google Adwords' => 'google',
                'Facebook'       => 'facebook',
                'LinkedIn'       => 'linkedin',
                'Instagram'      => 'instagram',
                'Twitter'        => 'twitter',
                'YouTube'        => 'youtube'
            ];

            if (!empty($getUtmMeta) && array_key_exists('utm_medium', $getUtmMeta)) {
                if ($getUtmMeta['utm_medium'] != 'organic' && isset($_GET['utm_source'])) {
                    //Paid
                    $getUtmMeta['utm_medium'] = 'Paid';
                    $getUtmMeta['utm_source'] = $this->array_find($_GET['utm_source'], $source);
                } elseif ($getUtmMeta['utm_medium'] == 'organic' && isset($_GET['utm_source'])) {
                    //Organic
                    $getUtmMeta['utm_medium'] = 'Organic';
                    $getUtmMeta['utm_source'] = $this->array_find($_GET['utm_source'], $source);
                }
            } else {
                if (!$this->get_utm_cookie()) {
                    //Everything else
                    $getUtmMeta['utm_medium'] = 'Organic';
                    $getUtmMeta['utm_source'] = 'Other';
                } else {
                    $getUtmMeta['utm_medium'] = $this->get_utm_cookie()['utm_medium'];
                    $getUtmMeta['utm_source'] = $this->get_utm_cookie()['utm_source'];
                }
            }

            /*            echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                        print_r($getUtmMeta);
                        echo '</pre>';*/

            return $getUtmMeta;
        }

        /**
         * Setup the cookie that will be used to send the form data
         *
         * @since    1.0.0
         *
         */
        public function set_utm_cookie() {
            if (!is_admin()) {
                if (empty($_COOKIE['utm_pf_track'])) {
                    $this->getUTM['utm_url'] = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    setcookie('utm_pf_track', serialize($this->getUTM), (time() + 3600), COOKIEPATH, COOKIE_DOMAIN);
                }
            }
        }

        /**
         * Return the utm cookie data or set the cookie and return the data
         *
         * @since    1.0.0
         *
         * @return mixed
         */
        public function get_utm_cookie() {
            @$cookie = $_COOKIE['utm_pf_track'];
            if (!empty($cookie)) {
                $this->set_utm_cookie();
            }
            return unserialize($cookie);
        }

        /**
         * Check if the utm data is provided by an external source
         *
         * @since    1.0.0
         *
         * @return bool
         */
        public function check_external_tracker() {
            if (array_key_exists("utm_source", $this->getUTM) || $this->get_utm_cookie()['utm_source'] != $this->getUTM['utm_source']) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Helper function to check array data
         *
         * @since    1.0.0
         *
         * @param $needle
         * @param array $haystack
         * @return bool|int|string
         */
        public function array_find($needle, array $haystack) {
            foreach ($haystack as $key => $value) {
                if (false !== stripos($value, $needle)) {
                    return $key;
                }
            }
            return false;
        }

        /**
         * Helper function to combine arrays
         *
         * @since    1.0.0
         *
         * @param $keys
         * @param $values
         * @return array
         */
        private function array_combine($keys, $values) {
            $result = array();
            foreach ($keys as $i => $k) {
                if (isset($values[$i]) && $values[$i] != '') {
                    $result[$i] = "$k" . "[$values[$i]]";
                }
            }
            return $result;
        }

        /**
         * Tester function
         */
        public function post_test() {
            global $post;

            $entry = array(
                'id'               => '982',
                'form_id'          => '4',
                'date_created'     => '2018-05-29 08:25:35',
                'is_starred'       => 0,
                'is_read'          => 0,
                'ip'               => '127.0.0.1',
                'source_url'       => 'http://propertyfox.loc/contact-us/',
                'post_id'          => NULL,
                'currency'         => 'ZAR',
                'payment_status'   => NULL,
                'payment_date'     => NULL,
                'transaction_id'   => NULL,
                'payment_amount'   => NULL,
                'payment_method'   => NULL,
                'is_fulfilled'     => NULL,
                'created_by'       => '5',
                'transaction_type' => NULL,
                'user_agent'       => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0',
                'status'           => 'active',
                6                  => 'Johan',
                7                  => 'Pretorius',
                5                  => '(321) 857-9879',
                2                  => 'Johan@propertyfox.co.za',
                3                  => 'Test message',
                '4.1'              => 'Yes please!',
            );

            $formFields = $this->setup_form_data()['fields'][$entry['form_id']];
            $linkFields = $this->setup_form_data()['link'][$entry['form_id']];

            $body = ['h_d854da9038b0c3005383102e792e19bd5a94769d' => ''];

            foreach ($formFields as $gfKey => $field) {
                $body[$field] = rgar($entry, $gfKey);
            }

            if ($linkFields['unique']) {
                $body['unique_id_field'] = $linkFields['unique'];
            }

            if ($linkFields['favorite']) {
                $body['favorite_unique_ids[]'] = get_post_meta($post->ID, $linkFields['favorite'], true);
            }
            $body = array_merge($body, $this->set_utm_data());

            /*         echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                     print_r($body);
                     echo '</pre>';*/
        }

    }