<?php

    /**
     * Fired during plugin deactivation
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     */

    /**
     * Fired during plugin deactivation.
     *
     * This class defines all code necessary to run during the plugin's deactivation.
     *
     * @since      1.0.0
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     * @author     Johan Pretorius <johan@propertyfox.co.za>
     */
    class Propertyfox_Traffic_Tracker_Deactivator {

        /**
         * Short Description. (use period)
         *
         * Long Description.
         *
         * @since    1.0.0
         */
        public static function deactivate() {

        }

    }
