<?php

    /**
     * Define the internationalization functionality
     *
     * Loads and defines the internationalization files for this plugin
     * so that it is ready for translation.
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     */

    /**
     * Define the internationalization functionality.
     *
     * Loads and defines the internationalization files for this plugin
     * so that it is ready for translation.
     *
     * @since      1.0.0
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     * @author     Johan Pretorius <johan@propertyfox.co.za>
     */
    class Propertyfox_Traffic_Tracker_i18n {

        /**
         * Load the plugin text domain for translation.
         *
         * @since    1.0.0
         */
        public function load_plugin_textdomain() {
            load_plugin_textdomain(
                'propertyfox-traffic-tracker',
                false,
                dirname(dirname(plugin_basename(__FILE__))) . '/languages/'
            );
        }
    }