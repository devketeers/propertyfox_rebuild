<?php

    /**
     * The file that defines the core plugin class
     *
     * A class definition that includes attributes and functions used across both the
     * public-facing side of the site and the admin area.
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     */

    /**
     * The core plugin class.
     *
     * This is used to define internationalization, admin-specific hooks, and
     * public-facing site hooks.
     *
     * Also maintains the unique identifier of this plugin as well as the current
     * version of the plugin.
     *
     * @since      1.0.0
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/includes
     * @author     Johan Pretorius <johan@propertyfox.co.za>
     */
    class Propertyfox_Traffic_Tracker {

        /**
         * The loader that's responsible for maintaining and registering all hooks that power
         * the plugin.
         *
         * @since    1.0.0
         * @access   protected
         * @var      Propertyfox_Traffic_Tracker_Loader $loader Maintains and registers all hooks for the plugin.
         */
        protected $loader;

        /**
         * The unique identifier of this plugin.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string $plugin_name The string used to uniquely identify this plugin.
         */
        protected $plugin_name;

        /**
         * The current version of the plugin.
         *
         * @since    1.0.0
         * @access   protected
         * @var      string $version The current version of the plugin.
         */
        protected $version;

        /**
         * The data of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      mixed $pb_traffic_tracker_options The data from the options page.
         */
        private $pb_traffic_tracker_options;

        /**
         * Define the core functionality of the plugin.
         *
         * Set the plugin name and the plugin version that can be used throughout the plugin.
         * Load the dependencies, define the locale, and set the hooks for the admin area and
         * the public-facing side of the site.
         *
         * @since    1.0.0
         */
        public function __construct() {
            if (defined('TRAFFIC_TRACKER_VERSION')) {
                $this->version = TRAFFIC_TRACKER_VERSION;
            } else {
                $this->version = '1.0.0';
            }
            $this->plugin_name = 'propertyfox-traffic-tracker';
            $this->pb_traffic_tracker_options = get_option($this->plugin_name);

            $this->load_dependencies();
            //$this->set_locale();
            if (is_admin()) {
                $this->define_admin_hooks();
            }
            if (!is_admin()) {
                $this->define_public_hooks();
            }
        }

        /**
         * Load the required dependencies for this plugin.
         *
         * Include the following files that make up the plugin:
         *
         * - Propertyfox_Traffic_Tracker_Loader. Orchestrates the hooks of the plugin.
         * - Propertyfox_Traffic_Tracker_i18n. Defines internationalization functionality.
         * - Propertyfox_Traffic_Tracker_Admin. Defines all hooks for the admin area.
         * - Propertyfox_Traffic_Tracker_Public. Defines all hooks for the public side of the site.
         *
         * Create an instance of the loader which will be used to register the hooks
         * with WordPress.
         *
         * @since    1.0.0
         * @access   private
         */
        private function load_dependencies() {

            /**
             * The class responsible for orchestrating the actions and filters of the
             * core plugin.
             */
            require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-propertyfox-traffic-tracker-loader.php';

            /**
             * The class responsible for defining internationalization functionality
             * of the plugin.
             */
            require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-propertyfox-traffic-tracker-i18n.php';

            /**
             * The class responsible for defining all actions that occur in the admin area.
             */
            require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-propertyfox-traffic-tracker-admin.php';

            /**
             * The class responsible for defining all actions that occur in the public-facing
             * side of the site.
             */
            require_once plugin_dir_path(dirname(__FILE__)) . 'public/class-propertyfox-traffic-tracker-public.php';

            $this->loader = new Propertyfox_Traffic_Tracker_Loader();

        }

        /**
         * Define the locale for this plugin for internationalization.
         *
         * Uses the Propertyfox_Traffic_Tracker_i18n class in order to set the domain and to register the hook
         * with WordPress.
         *
         * @since    1.0.0
         * @access   private
         */
        private function set_locale() {
            $plugin_i18n = new Propertyfox_Traffic_Tracker_i18n();
            $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
        }

        /**
         * Register all of the hooks related to the admin area functionality
         * of the plugin.
         *
         * @since    1.0.0
         * @access   private
         */
        private function define_admin_hooks() {
            $plugin_admin = new Propertyfox_Traffic_Tracker_Admin($this->get_plugin_name(), $this->get_version());

            if ($_GET['page'] == 'propertyfox-traffic-tracker') {
                $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
                $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
            }

            // Add menu item
            $this->loader->add_action('admin_menu', $plugin_admin, 'add_plugin_admin_menu');

            // Add Settings link to the plugin
            $plugin_basename = plugin_basename(plugin_dir_path(__DIR__) . $this->plugin_name . '.php');
            $this->loader->add_filter('plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links');

            // Save/Update our plugin options
            $this->loader->add_action('admin_init', $plugin_admin, 'options_update');

        }

        /**
         * Register all of the hooks related to the public-facing functionality
         * of the plugin.
         *
         * @since    1.0.0
         * @access   private
         */
        private function define_public_hooks() {
            $plugin_public = new Propertyfox_Traffic_Tracker_Public($this->get_plugin_name(), $this->get_version());

            /**
             * The following actions are commented out as we won't need any added style or script to our theme
             * $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
             * $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
             */
            $this->loader->add_action('wp_loaded', $plugin_public, 'setup_form_data');

            if ($plugin_public->check_external_tracker()) {
                $this->loader->add_action('init', $plugin_public, 'set_utm_cookie');
            }

            if (is_array($plugin_public->setup_form_data()['fields'])) {
                foreach ($plugin_public->setup_form_data()['fields'] as $formId => $forms) {
                    $this->loader->add_action('gform_after_submission_' . $formId, $plugin_public, 'post_to_third_party', 10, 2);
                }
            }
        }

        /**
         * Run the loader to execute all of the hooks with WordPress.
         *
         * @since    1.0.0
         */
        public function run() {
            $this->loader->run();
        }

        /**
         * The name of the plugin used to uniquely identify it within the context of
         * WordPress and to define internationalization functionality.
         *
         * @since     1.0.0
         * @return    string    The name of the plugin.
         */
        public function get_plugin_name() {
            return $this->plugin_name;
        }

        /**
         * The reference to the class that orchestrates the hooks with the plugin.
         *
         * @since     1.0.0
         * @return    Propertyfox_Traffic_Tracker_Loader    Orchestrates the hooks of the plugin.
         */
        public function get_loader() {
            return $this->loader;
        }

        /**
         * Retrieve the version number of the plugin.
         *
         * @since     1.0.0
         * @return    string    The version number of the plugin.
         */
        public function get_version() {
            return $this->version;
        }

    }
