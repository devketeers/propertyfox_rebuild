<div class="fox-traffic-tracker wrap">
    <div class="foxplug-header">
        <h1 class="wp-heading-inline"><?php echo esc_html(get_admin_page_title()); ?></h1>
        <hr class="wp-header-end">
    </div>
    <form method="post" name="setup_options" action="options.php">
        <?php submit_button('Save all changes', 'primary', 'submit', true); ?>
        <?php
            settings_fields($this->plugin_name);
            do_settings_sections($this->plugin_name);
        ?>
        <div class="tabs-js">
            <ul id="tabs-js-nav">
                <li><a href="#options">Setup Options</a></li>
                <?php foreach ($objects as $title => $object) { ?>
                    <?php if (isset($object) && !empty($object) && $object != 0) { ?>
                        <li><a href="#<?php echo $title; ?>"><?php echo ucfirst($title); ?></a></li>
                    <?php } ?>
                <?php } ?>
            </ul>
            <div id="tabs-js-content">
                <div id="options" class="tab-content">
                    <div class="foxplug-form">
                        <h4>Propertybase form details:</h4>
                        <!-- Set Form URL-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-form_url">
                                <span><?php esc_attr_e('Enter Propertybase Form URL:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Enter Propertybase Form URL:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-form_url" name="<?php echo $this->plugin_name; ?>[form_url]" value="<?php if (!empty($form_url)) echo $form_url; ?>" />
                        </fieldset>
                        <!-- Set Form ID-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-form_id">
                                <span><?php esc_attr_e('Enter form ID:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Enter form ID:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-form_id" name="<?php echo $this->plugin_name; ?>[form_id]" value="<?php if (!empty($form_id)) echo $form_id; ?>" />
                        </fieldset>
                        <h4>Propertybase Objects select:</h4>
                        <!-- Contact -->
                        <fieldset>
                            <legend class="screen-reader-text">
                                <span><?php _e('Contact Object', $this->plugin_name); ?></span>
                            </legend>
                            <label for="<?php echo $this->plugin_name; ?>-contact">
                                <input type="checkbox" id="<?php echo $this->plugin_name; ?>-contact" name="<?php echo $this->plugin_name; ?>[object][contact]" value="1" <?php checked($objects['contact'], 1); ?> />
                                <span><?php esc_attr_e('Contact Object', $this->plugin_name); ?></span>
                            </label>
                        </fieldset>
                        <!-- Company -->
                        <fieldset>
                            <legend class="screen-reader-text">
                                <span><?php _e('Company Object', $this->plugin_name); ?></span></legend>
                            <label for="<?php echo $this->plugin_name; ?>-company">
                                <input type="checkbox" id="<?php echo $this->plugin_name; ?>-company" name="<?php echo $this->plugin_name; ?>[object][company]" value="1" <?php checked($objects['company'], 1); ?> />
                                <span><?php esc_attr_e('Company Object', $this->plugin_name); ?></span>
                            </label>
                        </fieldset>
                        <!-- Property -->
                        <fieldset>
                            <legend class="screen-reader-text">
                                <span><?php _e('Property Object', $this->plugin_name); ?></span></legend>
                            <label for="<?php echo $this->plugin_name; ?>-property">
                                <input type="checkbox" id="<?php echo $this->plugin_name; ?>-property" name="<?php echo $this->plugin_name; ?>[object][property]" value="1" <?php checked($objects['property'], 1); ?> />
                                <span><?php esc_attr_e('Property Object', $this->plugin_name); ?></span>
                            </label>
                        </fieldset>
                        <!-- Listing -->
                        <fieldset>
                            <legend class="screen-reader-text">
                                <span><?php _e('Listing Object', $this->plugin_name); ?></span>
                            </legend>
                            <label for="<?php echo $this->plugin_name; ?>-listing">
                                <input type="checkbox" id="<?php echo $this->plugin_name; ?>-listing" name="<?php echo $this->plugin_name; ?>[object][listing]" value="1" <?php checked($objects['listing'], 1); ?> />
                                <span><?php esc_attr_e('Listing Object', $this->plugin_name); ?></span>
                            </label>
                        </fieldset>
                        <!-- Enquiry -->
                        <fieldset>
                            <legend class="screen-reader-text">
                                <span><?php _e('Enquiry Object', $this->plugin_name); ?> / PB Request</span>
                            </legend>
                            <label for="<?php echo $this->plugin_name; ?>-enquiry">
                                <input type="checkbox" id="<?php echo $this->plugin_name; ?>-enquiry" name="<?php echo $this->plugin_name; ?>[object][request]" value="1" <?php checked($objects['request'], 1); ?> />
                                <span><?php esc_attr_e('Enquiry Object / PB Request', $this->plugin_name); ?></span>
                            </label>
                        </fieldset>
                        <!--Tracking Fields Map-->
                        <h4>Propertybase tracking field names:</h4>
                        <p>Leave fields blank to avoid tracking the variable.</p>
                        <!--url-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_url">
                                <span><?php esc_attr_e('Url utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Url utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_url" name="<?php echo $this->plugin_name; ?>[utm][utm_url]" value="<?php echo($utm['utm_url'] ? $utm['utm_url'] : ''); ?>" />
                        </fieldset>
                        <!--source-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_source">
                                <span><?php esc_attr_e('Source utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Source utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_source" name="<?php echo $this->plugin_name; ?>[utm][utm_source]" value="<?php echo($utm['utm_source'] ? $utm['utm_source'] : ''); ?>" />
                        </fieldset>
                        <!--medium-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_medium">
                                <span><?php esc_attr_e('Medium utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Medium utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_medium" name="<?php echo $this->plugin_name; ?>[utm][utm_medium]" value="<?php echo($utm['utm_medium'] ? $utm['utm_medium'] : ''); ?>" />
                        </fieldset>
                        <!--campaign-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_campaign">
                                <span><?php esc_attr_e('Campaign utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Campaign utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_campaign" name="<?php echo $this->plugin_name; ?>[utm][utm_campaign]" value="<?php echo($utm['utm_campaign'] ? $utm['utm_campaign'] : ''); ?>" />
                        </fieldset>
                        <!--term-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_term">
                                <span><?php esc_attr_e('Term utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Term utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_term" name="<?php echo $this->plugin_name; ?>[utm][utm_term]" value="<?php echo($utm['utm_term'] ? $utm['utm_term'] : ''); ?>" />
                        </fieldset>
                        <!--content-->
                        <fieldset>
                            <label for="<?php echo $this->plugin_name; ?>-utm_content">
                                <span><?php esc_attr_e('Content utm field id:', $this->plugin_name); ?></span>
                            </label>
                            <legend class="screen-reader-text">
                                <span><?php _e('Content utm field id:', $this->plugin_name); ?></span>
                            </legend>
                            <input type="text" class="regular-text" id="<?php echo $this->plugin_name; ?>-utm_content" name="<?php echo $this->plugin_name; ?>[utm][utm_content]" value="<?php echo($utm['utm_content'] ? $utm['utm_content'] : ''); ?>" />
                        </fieldset>
                    </div>
                </div>
                <?php if (is_array($objects)) { ?>
                    <?php foreach ($objects as $title => $object) { ?>
                        <?php if (isset($object) && !empty($object) && $object != 0) { ?>
                            <div id="<?php echo $title; ?>" class="tab-content">
                                <div class="foxplug-form">
                                    <h4>Select forms to integrate with: <?php echo ucfirst($title); ?> Object</h4>
                                    <?php foreach ($forms as $id => $form) { ?>
                                        <?php
                                        $string = str_replace(' ', '_', $form->title); // Replaces all spaces with hyphens.
                                        $formTitle = strtolower(preg_replace('/[^A-Za-z0-9\_]/', '', $string)); // Removes special chars.
                                        ?>
                                        <label for="<?php echo $this->plugin_name . '-' . $form->id . '-' . $title; ?>">
                                            <input type="checkbox" id="<?php echo $this->plugin_name . '-' . $form->id . '-' . $title; ?>" name="<?php echo $this->plugin_name; ?>[forms][<?php echo $title; ?>][<?php echo $form->id; ?>]" value="1" <?php checked($selectedForms[$title][$form->id], 1); ?> />
                                            <span><?php esc_attr_e($form->title, $this->plugin_name); ?></span>
                                        </label>
                                    <?php } ?>
                                    <?php if (is_array($selectedForms)) { ?>
                                        <div class="form-fields">
                                            <?php foreach ($selectedForms as $formObject => $linkedForms) {
                                                if ($formObject == $title) {
                                                    foreach ($linkedForms as $formId => $active) {
                                                        echo '<h4>Link fields for form: ' . $this->get_the_form_title($formId) . '</h4>';
                                                        echo $this->build_fields_hooks($formId, $objects, $title);
                                                    }
                                                }
                                            } ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <?php submit_button('Save all changes', 'primary', 'submit', true); ?>
    </form>
</div>