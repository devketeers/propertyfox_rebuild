<?php

    /**
     * The admin-specific functionality of the plugin.
     *
     * @link       https://propertyfox.co.za
     * @since      1.0.0
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/admin
     */

    /**
     * The admin-specific functionality of the plugin.
     *
     * Defines the plugin name, version, and two examples hooks for how to
     * enqueue the admin-specific stylesheet and JavaScript.
     *
     * @package    Propertyfox_Traffic_Tracker
     * @subpackage Propertyfox_Traffic_Tracker/admin
     * @author     Johan Pretorius <johan@propertyfox.co.za>
     */
    class Propertyfox_Traffic_Tracker_Admin {

        /**
         * The ID of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $plugin_name The ID of this plugin.
         */
        private $plugin_name;

        /**
         * The version of this plugin.
         *
         * @since    1.0.0
         * @access   private
         * @var      string $version The current version of this plugin.
         */
        private $version;

        /**
         * Initialize the class and set its properties.
         *
         * @since    1.0.0
         * @param      string $plugin_name The name of this plugin.
         * @param      string $version The version of this plugin.
         */
        public function __construct($plugin_name, $version) {
            $this->plugin_name = $plugin_name;
            $this->version = $version;
        }

        /**
         * Register the administration menu for this plugin into the WordPress Dashboard menu.
         *
         * @since    1.0.0
         */

        public function add_plugin_admin_menu() {
            add_options_page('Propertyfox Traffic Tracker', 'Traffic Tracker', 'manage_options', $this->plugin_name, array($this, 'display_plugin_setup_page')
            );
        }

        /**
         * Add settings action link to the plugins page.
         *
         * @since    1.0.0
         */
        public function add_action_links($links) {
            $settings_link = array(
                '<a href="' . admin_url('options-general.php?page=' . $this->plugin_name) . '">' . __('Settings', $this->plugin_name) . '</a>',
            );
            return array_merge($settings_link, $links);
        }

        /**
         * Render the settings page for this plugin.
         *
         * @since    1.0.0
         */
        public function display_plugin_setup_page() {
            // Saved data
            $options = get_option($this->plugin_name);

            // GF object for setting up fields
            $forms = RGFormsModel::get_forms(NULL, 'title');

            // Form ID
            $form_url = $options['form_url'];

            // Form ID
            $form_id = $options['form_id'];

            // PB Objects
            $objects = unserialize($options['object']);

            // PB Tracking fields Map
            $utm = unserialize($options['utm']);

            // GF mapped forms
            if ($options['forms']) {
                $selectedForms = unserialize($options['forms']);
            }

            include_once('partials/propertyfox-traffic-tracker-admin-display.php');
        }

        /**
         * Register the stylesheets for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_styles() {
            wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/propertyfox-traffic-tracker-admin.css', array(), $this->version, 'all');
        }

        /**
         * Register the JavaScript for the admin area.
         *
         * @since    1.0.0
         */
        public function enqueue_scripts() {
            wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/propertyfox-traffic-tracker-admin.js', array('jquery'), $this->version, false);
        }

        /**
         * Update Options
         */
        public function options_update() {
            register_setting($this->plugin_name, $this->plugin_name, array($this, 'validate'));
        }

        /**
         * Validate admin options field
         *
         * @param $input
         * @return array
         *
         * @since    1.0.0
         */
        public function validate($input) {
            // All checkboxes inputs
            $valid = array();

            //Form URL
            $valid['form_url'] = (isset($input['form_url']) && !empty($input['form_url']) ? esc_url($input['form_url']) : '');

            //Form ID
            $valid['form_id'] = (isset($input['form_id']) && !empty($input['form_id']) ? esc_attr($input['form_id']) : '');

            //Form Objects
            $valid['object'] = (isset($input['object']) && !empty($input['object'])) ? serialize($input['object']) : '';

            //Tracking fields map
            $valid['utm'] = (isset($input['utm']) && !empty($input['utm'])) ? serialize($input['utm']) : '';

            //Forms
            $valid['forms'] = (isset($input['forms']) && !empty($input['forms'])) ? serialize($input['forms']) : '';

            //Form fields
            $valid['fields'] = (isset($input['fields']) && !empty($input['fields'])) ? serialize($input['fields']) : '';

            return $valid;
        }

        /**
         * @param $id
         * @return array
         */
        private function get_GF_fields($id) {
            global $wpdb;
            $table_name = $wpdb->prefix . "gf_form_meta";
            $exclude = ['html', 'page'];

            $metaDataForm = $wpdb->get_results("SELECT display_meta FROM $table_name WHERE form_id = $id");
            $filterFields = json_decode($metaDataForm[0]->display_meta);

            $returnArray = [];
            foreach ($filterFields->fields as $field) {
                if (!in_array($field->type, $exclude)) {
                    $returnArray[$field->id] = $field->label;
                }
            }

            return array_filter($returnArray, 'strlen');
        }

        /**
         * @param $id
         * @param $objects
         * @param $formObject
         * @return string
         */
        public function build_fields_hooks($id, $objects, $formObject) {
            //Get saved data
            $options = get_option($this->plugin_name);

            if ($options['fields']) {
                $fieldsData = unserialize($options['fields'])[$formObject][$id];
            }

            //Set base fields name
            $fieldName = $this->plugin_name . '[fields]' . '[' . $formObject . '][' . $id . ']';

            //Start html output for dynamic fields
            $html = '<table class="wp-list-table widefat fixed striped posts">';
            $html .= '<thead><tr>';
            $html .= '<th scope="col" id="title" class="manage-column column-title column-primary sortable desc"><a href="#"><span>GF Field</span></a></th>';
            $html .= '<th scope="col" id="pb-object" class="manage-column column-pb-object sortable asc"><a href="#"><span>PB Object</span></a></th>';
            $html .= '<th scope="col" id="pb-field" class="manage-column column-pb-field sortable asc"><a href="#"><span>PB Field</span></a></th>';
            $html .= '</tr></thead>';
            $html .= '<tbody id="the-list">';

            //Favorite unique ids
            $html .= '<tr id="post-unique" class="iedit author-other level-0 post-unique type-property status-publish hentry">';
            $html .= '<td class="title column-title has-row-actions column-primary page-title" data-colname="Title">Favorite unique ids</td>';
            $html .= '<td class="title column-pb-object has-row-actions page-object" data-colname="PB Object">Leave blank for none / Use meta key to pull field</td>';
            $html .= '<td class="title column-pb-field has-row-actions page-field" data-colname="PB Field">';
            $html .= '<input type="text" class="regular-text" id="' . $this->plugin_name . '-pb-field-favorite" name="' . $fieldName . '[pb_link][favorite]" value="' . $fieldsData['pb_link']['favorite'] . '" />';
            $html .= '</td>';
            $html .= '</tr>';

            //Unique id field
            $html .= '<tr id="post-unique" class="iedit author-other level-0 post-unique type-property status-publish hentry">';
            $html .= '<td class="title column-title has-row-actions column-primary page-title" data-colname="Title">Unique id field</td>';
            $html .= '<td class="title column-pb-object has-row-actions page-object" data-colname="PB Object">Leave blank for none</td>';
            $html .= '<td class="title column-pb-field has-row-actions page-field" data-colname="PB Field">';
            $html .= '<input type="text" class="regular-text" id="' . $this->plugin_name . '-pb-field-unique" name="' . $fieldName . '[pb_link][unique]" value="' . $fieldsData['pb_link']['unique'] . '" />';
            $html .= '</td>';
            $html .= '</tr>';

            //Get fields for each selected object linked to GF form
            $fields = $this->get_GF_fields($id);

            //Build fields linking for each
            foreach ($fields as $fieldId => $fieldLabel) {
                $html .= '<tr id="post-' . $fieldId . '" class="iedit author-other level-0 post-' . $fieldId . ' type-property status-publish hentry">';

                /*Title of Gravity forms field to be mapped*/
                $html .= '<td class="title column-title has-row-actions column-primary page-title" data-colname="Title">' . $fieldLabel . '</td>';

                /*Object select for Propertybase field mapping*/
                $html .= '<td class="title column-pb-object has-row-actions page-object" data-colname="PB Object">';
                $html .= '<select id="' . $this->plugin_name . '-pb-object-' . $fieldId . '" name="' . $fieldName . '[pb_object][' . $fieldId . ']">';
                foreach ($objects as $title => $object) {
                    if (isset($object) && !empty($object) && $object != 0) {
                        $html .= '<option value="' . $title . '" ' . ($title == $fieldsData['pb_object'][$fieldId] ? 'selected="selected"' : '') . '>' . $title . '</option>';
                    }
                }
                $html .= '</select>';
                $html .= '</td>';

                /*Propertybase field to map to*/
                $html .= '<td class="title column-pb-field has-row-actions page-field" data-colname="PB Field">';
                $html .= '<input type="text" class="regular-text" id="' . $this->plugin_name . '-pb-field-' . $fieldId . '" name="' . $fieldName . '[pb_field][' . $fieldId . ']" value="' . $fieldsData['pb_field'][$fieldId] . '" />';
                $html .= '</td>';

                $html .= '</tr>';
            }
            $html .= '</tbody>';
            $html .= '</table>';
            return $html;
        }

        /**
         * @param $form_id
         * @return mixed
         */
        public function get_the_form_title($form_id) {
            $forminfo = RGFormsModel::get_form($form_id);
            $form_title = $forminfo->title;
            return $form_title;
        }
    }