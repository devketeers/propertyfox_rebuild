'use strict';

/**
 * All of the code for your admin-facing JavaScript source
 * should reside in this file.
 *
 * Note: It has been assumed you will write jQuery code here, so the
 * $ function reference has been prepared for usage within the scope
 * of this function.
 *
 * This enables you to define handlers, for when the DOM is ready:
 *
 * jQuery(function() {
	 *
	 * });
 *
 * When the window is loaded:
 *
 * jQuery( window ).load(function() {
	 *
	 * });
 *
 * ...and/or other possibilities.
 *
 * Ideally, it is not considered best practise to attach more than a
 * single DOM-ready or window-load handler for a particular page.
 * Although scripts in the WordPress core, Plugins and Themes may be
 * practising this, we should strive to set a better example in our own work.
 */

jQuery(document).ready(function () {
    // Show the first tab and hide the rest
    jQuery('#tabs-js-nav li:first-child').addClass('active');
    jQuery('.tab-content').hide();
    jQuery('.tab-content:first').show();

// Click function
    jQuery('#tabs-js-nav li').click(function () {
        jQuery('#tabs-js-nav li').removeClass('active');
        jQuery(this).addClass('active');
        jQuery('.tab-content').hide();

        var activeTab = jQuery(this).find('a').attr('href');
        jQuery(activeTab).fadeIn();
        return false;
    });
});