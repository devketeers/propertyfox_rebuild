<?php

    date_default_timezone_set('Africa/Johannesburg');

    class FFDAnalytics {

        /**
         * FFDAnalytics constructor.
         */
        public function __construct() {
            wp_schedule_event(time(), 'hourly', 'SyncListings');
        }

        /**
         * Upon activation of plugin
         */
        static function activation() {
            wp_schedule_event(time(), 'hourly', 'SyncAnalytics');
            //Setup database
            self::setupdb();
        }

        /**
         * Reset activation
         */
        static function deactivation() {
            wp_clear_scheduled_hook('SyncAnalytics');
        }

        /**
         * Create db fields
         */
        private static function setupdb() {
            global $wpdb;

            $charset_collate = $wpdb->get_charset_collate();

            //Setup analytics table
            $analyticsTableName = $wpdb->prefix . "FFDAnalytics";

            $sql = "CREATE TABLE $analyticsTableName (
                id int NOT NULL AUTO_INCREMENT,
                listingId nvarchar(255) NOT NULL,
                userId int,
                createddate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                sfId nvarchar(255),
                recorded bit,
                PRIMARY KEY (id),
                INDEX listing_mlsid (listingId)
                ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            $result = dbDelta($sql);
        }

        /**
         *
         */
        public static function SyncAnalytics() {
            $settings = get_option('WebListing_Settings');

            require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegration-Functions.php');

            global $wpdb;
            $hits = array();

            $sql = "select id, listingid, createddate, sfid from {$wpdb->prefix}FFDAnalytics where recorded is null limit 200";

            $results = $wpdb->get_results($sql);
            $index = 1;
            foreach ($results as $result) {
                //Save a request
                $hit = array();

                $atts = array();
                $atts["type"] = "analytic__c";
                $atts["referenceId"] = "ref" . $index;

                $hit["attributes"] = $atts;
                $hit["Listing__c"] = $result->listingid;
                $hit["Date__c"] = date("c", strtotime($result->createddate));

                if (isset($result->sfid) && $result->sfid != "")
                    $hit["Contact__c"] = $result->sfid;

                array_push($hits, $hit);

                $sql = "update {$wpdb->prefix}FFDAnalytics set recorded=true where id=" . $result->id;
                $wpdb->query($sql);

                $index += 1;
            }

            if (sizeof($hits) > 0) {
                $data = array();
                $data["records"] = $hits;
                $reqResult = FFDPBIntegration_Functions::PostAPI($settings["OAuth"], "/services/data/v36.0/composite/tree/analytic__c", $data);
            }
        }
    }