<?php

    date_default_timezone_set('Africa/Johannesburg');

    class FFDPBIntegrationPost {
        private static $initiated = false;
        private static $isRunning = false;

        /**
         * FFDPBIntegrationPost constructor.
         */
        public function __construct() {
            wp_schedule_event(time(), 'hourly', 'SyncListings');
        }

        /**
         * Init function
         */
        public static function init() {
            if (!self::$initiated) {
                self::$initiated = true;
            }
        }

        /**
         *
         */
        public static function activation() {
            wp_schedule_event(time(), 'hourly', 'SyncListings');

            $settings = get_option('WebListing_Settings');

            if (isset($settings) && isset($settings["Status"])) {
                $settings["Status"] = "Idle";
                update_option("WebListing_Settings", $settings);
            }

            self::$isRunning = false;
        }

        /**
         *
         */
        public static function deactivation() {
            $settings = get_option('WebListing_Settings');
            wp_clear_scheduled_hook('SyncListings');
            self::$isRunning = false;
            $settings["LastRun"] = NULL;
            update_option("WebListing_Settings", $settings);
        }

        /**
         * @param $record
         * @param $property
         * @param $fields
         */
        private static function createPost($record, $property, $fields) {
            $postType = "Property";
            $isUpdate = "NO";

            $post_id = self::getPostId($record->Id);

            $province = '';
            $city = '';
            $suburb = '';
            foreach ($record as $key => $value) {
                if (strtolower($key) == "province__c" || strtolower($key) == "pba__state_pb__c") {
                    $province = $value;
                } elseif (strtolower($key) == "pba__city_pb__c") {
                    $city = $value;
                } elseif (strtolower($key) == "pba__area_pb__c") {
                    $suburb = $value;
                }
            }

            $subUrl = "";

            if ($province != "") {
                $subUrl = $province . "/";
            }

            if ($city != "") {
                $subUrl .= $city . "/";
            }

            if ($suburb != "") {
                $subUrl .= $suburb . "/";
            }

            if ($post_id == 0) {
                $post_id = wp_insert_post(
                    array(
                        'comment_status' => 'closed',
                        'ping_status'    => 'closed',
                        'post_author'    => 1,
                        'post_name'      => $record->{$fields["post_name"]},
                        'post_title'     => $record->{$fields["post_title"]},
                        'post_content'   => $record->{$fields["post_content"]},
                        'post_status'    => 'publish',
                        'post_type'      => $postType
                    )
                );
            } else {
                wp_update_post(
                    array(
                        'ID'           => $post_id,
                        'post_title'   => $record->{$fields["post_title"]},
                        'post_name'    => $record->{$fields["post_name"]},
                        'post_content' => $record->{$fields["post_content"]},
                        'post_type'    => $postType
                    )
                );

                $isUpdate = "YES";
            }
            update_post_meta($post_id, '_wp_page_template', '/Page-Templates/property_detail.php');

            foreach ($record as $key => $value) {
                //Get the field
                $pbField = array_search($key, $fields);

                if (substr($pbField, 0, 7) == "PBmeta_") {
                    update_post_meta($post_id, $fields[str_replace("PBmeta_", "meta_", $pbField)], $value);
                } elseif ($key == "Id") {
                    update_post_meta($post_id, "ListingId", $value);
                }
            }

            $images = array();
            $videos = array();
            $floorPlans = array();

            //Save images
            if (isset($property->pba__PropertyMedia__r)) {
                foreach ($property->pba__PropertyMedia__r->records as $media) {
                    if ($media->pba__Category__c == "Videos") {
                        array_push($videos, $media->pba__URL__c);
                    } elseif (NULL != $media->pba__Tags__c && strpos($media->pba__Tags__c, "Floorplan") !== false) {
                        array_push($floorPlans, $media->pba__URL__c);
                    } elseif ($media->pba__Category__c == "Images") {
                        array_push($images, $media->pba__URL__c);
                    }
                }

                update_post_meta($post_id, 'media', implode(";", $images));

                if (sizeof($videos) > 0) {
                    update_post_meta($post_id, 'video', implode(";", $videos));
                } else {
                    update_post_meta($post_id, 'video', '');
                }

                if (sizeof($floorPlans) > 0) {
                    update_post_meta($post_id, 'floorplan', implode(";", $floorPlans));
                } else {
                    update_post_meta($post_id, 'floorplan', '');
                }
            }

            //Set categories
            self::attachCategory($post_id, $province, $city, $suburb);
        }

        /**
         * @param $pbid
         */
        private static function deletePost($pbid) {
            $id = self::getPostId($pbid);
            if ($id != 0) {
                wp_delete_post($id, true);
            }
        }

        /**
         * @param $pbid
         * @return int
         */
        private static function getPostId($pbid) {
            global $wpdb;
            $meta = $wpdb->get_results("SELECT * FROM `" . $wpdb->postmeta . "` WHERE meta_key='" . esc_sql("ListingId") . "' AND meta_value='" . esc_sql($pbid) . "'");
            if (is_array($meta) && !empty($meta) && isset($meta[0])) {
                $meta = $meta[0];
            }

            if (is_object($meta)) {
                return $meta->post_id;
            }

            return 0;
        }


        /**
         * @param $postId
         * @param $province
         * @param $city
         * @param $suburb
         */
        private static function attachCategory($postId, $province, $city, $suburb) {
            $catIds = array();

            /**
             * Handle Province
             */
            $term_p = term_exists($province, 'location_category');
            if (0 !== $term_p && NULL !== $term_p) {
                array_push($catIds, $term_p['term_id']);
            }

            /**
             * Handle City
             */
            $term_c = term_exists($city, 'location_category');
            if (0 !== $term_c && NULL !== $term_c) {
                array_push($catIds, $term_c['term_id']);
            }

            /**
             * Handle Suburb
             */
            $term_s = term_exists($suburb, 'location_category');
            if (0 !== $term_s && NULL !== $term_s) {
                array_push($catIds, $term_s['term_id']);
            }

            //Set Terms
            wp_set_post_terms($postId, $catIds, 'location_category', true);

        }

        /**
         * Sync PB Listing to Wordpress
         */
        public static function mlsSync() {
            $settings = get_option('WebListing_Settings');

            if (!self::$isRunning && isset($settings) && isset($settings["EnableSync"]) && $settings["EnableSync"] == "Yes" && isset($settings["OAuth"]) && isset($settings["OAuth"]["OAuthToken"]) && (!isset($settings["Status"]) || $settings["Status"] == "Idle")) {
                try {
                    self::$isRunning = true;
                    $settings["Status"] = "Running";

                    //TODO: Enable
                    self::removeExpired();

                    global $wpdb;

                    if (sizeof($settings["SelectFields"]) > 0) {
                        $currRun = date("Y-m-d\Th:i:s");
                        $lastRun = date("Y-m-d\Th:i:s", strtotime("January 1 1900"));

                        //Always sync all listing for PF
                        //if (isset($settings["LastRun"])) {
                        //    $lastRun=$settings["LastRun"];
                        //}

                        require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegration-Functions.php');

                        $where = " where (LastModifiedDate>" . $lastRun . "Z)";

                        $query = "SELECT Id,(Select pba__ExternalLink__c,pba__tags__c,pba__category__c,pba__url__c,pba__Title__c From pba__PropertyMedia__r where pba__IsOnWebsite__c=true order by pba__SortOnWebsite__c),(select Id,Name,";

                        foreach ($settings["SelectFields"] as $key => $field) {
                            if ($field != "Id" && $field != "Name" && substr($key, 0, 5) != "meta_") {
                                $query .= $field . ",";
                            }
                        }

                        if (isset($settings["WHERE"])) {
                            if (isset($settings["WHERE"]["Condition1"])) {
                                $values = explode(";", $settings["WHERE"]["Condition1"]["Value"]);

                                $first = true;

                                foreach ($values as $value) {
                                    $condition = " or ";

                                    if ($first) {
                                        $first = false;
                                        $condition = " and(";
                                    }

                                    $where .= $condition . $settings["WHERE"]["Condition1"]["Field"] . $settings["WHERE"]["Condition1"]["Type"] . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'");
                                }

                                $where .= ") ";
                            }

                            if (isset($settings["WHERE"]["Condition2"])) {
                                $values = explode(";", $settings["WHERE"]["Condition2"]["Value"]);

                                foreach ($values as $value) {
                                    $where .= " and " . $settings["WHERE"]["Condition2"]["Field"] . $settings["WHERE"]["Condition2"]["Type"] . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'");
                                }
                            }

                            if (isset($settings["WHERE"]["Condition3"])) {
                                $values = explode(";", $settings["WHERE"]["Condition3"]["Value"]);

                                foreach ($values as $value) {
                                    $where .= " and " . $settings["WHERE"]["Condition3"]["Field"] . $settings["WHERE"]["Condition3"]["Type"] . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'");
                                }
                            }
                        }

                        $query = rtrim($query, ",");

                        $query .= " from pba__listings__r" . $where . ") from pba__property__c where id in (select pba__property__c from pba__listing__c" . $where . ")";

                        //TODO: Remove
                        //echo $query;
                        //die();

                        $done = false;
                        $methodUrl = "/services/data/v20.0/query/?q=" . urlencode($query);
                        $fields = $settings["SelectFields"];
                        $current = 0;

                        while (!$done) {
                            $results = FFDPBIntegration_Functions::GetAPI($settings["OAuth"], $methodUrl);
                            if (isset($results->totalSize) && $results->totalSize > 0) {
                                $done = $results->done;

                                if (!$done) {
                                    $methodUrl = $results->nextRecordsUrl;
                                }

                                foreach ($results->records as $property) {
                                    $current += 1;
                                    $settings["Status"] = "Syncing Property: " . $current . " of " . $results->totalSize;
                                    update_option("WebListing_Settings", $settings);
                                    if (isset($property->pba__Listings__r)) {
                                        foreach ($property->pba__Listings__r->records as $record) {
                                            //Create the post
                                            self::createPost($record, $property, $fields);
                                        }
                                    }
                                }
                            } else {
                                $done = true;
                            }
                        }

                        $settings["LastRun"] = $currRun;
                        $settings["Status"] = "Idle";
                        $settings["SkipCount"] = 0;
                        update_option("WebListing_Settings", $settings);
                        self::$isRunning = false;
                    }
                } catch (Exception $ex) {
                    //todo log error
                    $settings["Status"] = "Idle";
                    update_option("WebListing_Settings", $settings);
                    self::$isRunning = false;
                }
            } else {
                if (isset($settings["SkipCount"])) {
                    $settings["SkipCount"] = (int)$settings["SkipCount"] + 1;
                } else {
                    $settings["SkipCount"] = 1;
                }

                if ($settings["SkipCount"] > 2) {
                    $settings["SkipCount"] = 0;
                    self::$isRunning = false;
                    $settings["Status"] = "Idle";
                }

                error_log('Skipped Sync Skip Count: ' . $settings["SkipCount"]);

                update_option("WebListing_Settings", $settings);
            }
        }

        /**
         * Remove expired listing
         */
        private static function removeExpired() {
            try {
                global $wpdb;
                $settings = get_option('WebListing_Settings');

                $end = date("Y-m-d\Th:i:s", strtotime("+2 days"));
                $start = date("Y-m-d\Th:i:s", strtotime("-29 days"));
                $lastRun = date("Y-m-d\Th:i:s", strtotime("January 1 1900"));

                //if (isset($settings["LastRun"])) {
                //    $lastRun=strtotime("-2 days", strtotime($settings["LastRun"]));
                //}

                $query = "start=" . urlencode($start . "Z") . "&end=" . urlencode($end . "Z");

                $methodUrl = "/services/data/v29.0/sobjects/pba__listing__c/deleted/?" . $query;

                require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegration-Functions.php');
                $results = FFDPBIntegration_Functions::GetAPI($settings["OAuth"], $methodUrl);
                $current = 0;

                foreach ($results->deletedRecords as $del) {
                    try {
                        $current += 1;

                        $settings["Status"] = "Processing Deleted Listings " . $current . " of " . sizeof($results->deletedRecords);
                        update_option("WebListing_Settings", $settings);

                        self::deletePost($del->id);
                    } catch (Exception $ex) {
                        $err = $ex;
                    }
                }

                $current = 0;

                //Remove Delete Condition listings
                if (isset($settings["DELETE"])) {
                    $where = " where LastModifiedDate>" . $lastRun . "Z and ";
                    $first = true;

                    if (isset($settings["DELETE"]["Condition1"])) {
                        $values = explode(";", $settings["DELETE"]["Condition1"]["Value"]);

                        foreach ($values as $value) {
                            if (!$first) {
                                $where .= " and ";
                            } else {
                                $where .= "(";
                                $first = false;
                            }

                            $where .= $settings["DELETE"]["Condition1"]["Field"] . $settings["DELETE"]["Condition1"]["Type"] . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'");
                        }
                    }

                    $first = true;

                    if (isset($settings["DELETE"]["Condition2"])) {
                        $values = explode(";", $settings["DELETE"]["Condition2"]["Value"]);

                        if (sizeof($values) > 1) {
                            $where .= " or " . $settings["DELETE"]["Condition2"]["Field"];

                            if ($settings["DELETE"]["Condition2"]["Type"] == "!=") {
                                $where .= " not";
                            }

                            $where .= " in (";

                            foreach ($values as $value) {
                                $where .= ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . ",";
                            }

                            $where = rtrim($where, ",") . ")";
                        } else {
                            $where .= " or " . $settings["DELETE"]["Condition2"]["Field"] . $settings["DELETE"]["Condition2"]["Type"] . ((strtolower($values[0]) == "true" || strtolower($values[0]) == "false") ? "" : "'") . $values[0] . ((strtolower($values[0]) == "true" || strtolower($values[0]) == "false") ? "" : "'");
                        }
                    }

                    $first = true;

                    if (isset($settings["DELETE"]["Condition3"])) {
                        $values = explode(";", $settings["DELETE"]["Condition3"]["Value"]);

                        if (sizeof($values) > 1) {
                            $where .= " or " . $settings["DELETE"]["Condition3"]["Field"];

                            if ($settings["DELETE"]["Condition3"]["Type"] == "!=") {
                                $where .= " not";
                            }

                            $where .= " in (";

                            foreach ($values as $value) {
                                $where .= ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . $value . ((strtolower($value) == "true" || strtolower($value) == "false") ? "" : "'") . ",";
                            }

                            $where = rtrim($where, ",") . ")";
                        } else {
                            $where .= " or " . $settings["DELETE"]["Condition3"]["Field"] . $settings["DELETE"]["Condition3"]["Type"] . ((strtolower($values[0]) == "true" || strtolower($values[0]) == "false") ? "" : "'") . $values[0] . ((strtolower($values[0]) == "true" || strtolower($values[0]) == "false") ? "" : "'");
                        }
                    }

                    if (isset($settings["DELETE"]["Condition1"])) {
                        $where .= ")";
                    }


                    $query = "select id,pba__property__c from pba__listing__c" . $where;

                    $done = false;
                    $methodUrl = "/services/data/v20.0/query/?q=" . urlencode($query);
                    $current = 0;

                    while (!$done) {
                        $results = FFDPBIntegration_Functions::GetAPI($settings["OAuth"], $methodUrl);

                        if (isset($results->totalSize) && $results->totalSize > 0) {
                            $done = $results->done;

                            if (!$done) {
                                $methodUrl = $results->nextRecordsUrl;
                            }

                            foreach ($results->records as $listing) {
                                $current += 1;

                                $settings["Status"] = "Processing Expired Listings " . $current . " of " . $results->totalSize;
                                update_option("WebListing_Settings", $settings);

                                self::deletePost($listing->Id);
                            }
                        } else {
                            $done = true;
                        }
                    }
                }
            } catch (Exception $ex) {
                $err = $ex;
            }
        }

        /**
         * @param $data
         */
        public static function logFile($data) {
            $my_file = PBIP_PLUGIN_DIR . 'debugger.txt';
            $handle = fopen($my_file, 'w') or die('Cannot open file:  ' . $my_file);
            fwrite($handle, json_encode($data));
            fclose($handle);
        }

        public static function mylog($text, $level = 'i', $file = 'logger') {
            switch (strtolower($level)) {
                case 'e':
                case 'error':
                    $level = 'ERROR';
                    break;
                case 'i':
                case 'info':
                    $level = 'INFO';
                    break;
                case 'd':
                case 'debug':
                    $level = 'DEBUG';
                    break;
                default:
                    $level = 'INFO';
            }
            error_log(date("[Y-m-d H:i:s]") . "\t[" . $level . "]\t[" . basename(__FILE__) . "]\t" . $text . "\n", 3, $file);
        }


    }
