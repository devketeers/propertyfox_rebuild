<?php

    /**
     * Plugin Name: Propertyfox Propertybase Integration
     * Plugin URI: http://www.frozenfishdev.com
     * Description: Creates posts from Propertybase Listings
     * Author: Jarrett Fisher
     * Version: 1.0.0
     * Author URI:http://www.frozenfishdev.com
     */

    // If this file is called directly, abort.
    if (!defined('WPINC')) {
        die;
    }

    /**
     * Currently plugin version.
     * Start at version 1.0.0 and use SemVer - https://semver.org
     * Rename this for your plugin and update it as you release new versions.
     */
    define('PLUGIN_NAME_VERSION', '1.0.0');

    date_default_timezone_set('Africa/Johannesburg');

    define('PBIP_PLUGIN_DIR', plugin_dir_path(__FILE__));

    if (is_admin()) {
        require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegrationPost-Admin.php');
        add_action('init', array('FFDPBIntegrationPostAdmin', 'init'));
        //Setup the post types
        add_action('init', array('FFDPBIntegrationPost', 'init'));
    }

    require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegrationPost.php');
    register_activation_hook(__FILE__, array('FFDPBIntegrationPost', 'activation'));
    register_deactivation_hook(__FILE__, array('FFDPBIntegrationPost', 'deactivation'));
    add_action('SyncListings', array('FFDPBIntegrationPost', 'mlsSync'));

    require_once(PBIP_PLUGIN_DIR . 'class.FFDAnalytics.php');
    add_action('SyncAnalytics', array('FFDAnalytics', 'SyncAnalytics'));


    register_activation_hook(__FILE__, array('FFDAnalytics', 'activation'));