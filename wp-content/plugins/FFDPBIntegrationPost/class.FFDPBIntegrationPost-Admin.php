<?php

    class FFDPBIntegrationPostAdmin {

        private static $initiated = false;

        /**
         * FFDPBIntegrationPostAdmin constructor.
         */
        public function __construct() {
            wp_schedule_event(time(), 'hourly', 'SyncAnalytics');
        }

        /**
         * @param $settings
         * @return array
         */
        private static function getPBFields($settings) {
            require_once(PBIP_PLUGIN_DIR . 'class.FFDPBIntegration-Functions.php');

            $vals = FFDPBIntegration_Functions::GetApi($settings["OAuth"], "/services/data/v20.0/sobjects/pba__listing__c/describe");

            $items = array();

            $items[""] = "";

            foreach ($vals->fields as $field) {
                $items[$field->name] = $field->label;// . "(" . $field->name . ")";
            }

            return $items;
        }

        /**
         * Self Initiate
         */
        static function init() {
            if (!self::$initiated) {
                self::init_hooks();
                self::$initiated = true;
            }
        }

        /**
         * Initiate class hooks
         */
        public static function init_hooks() {
            add_action('admin_menu', array('FFDPBIntegrationPostAdmin', 'admin_menu'));
            add_action('wp_dashboard_setup', array('FFDPBIntegrationPostAdmin', 'settings_dashboard_widgets'));
        }

        /**
         * Create Admin menu in Wordpress
         */
        public static function admin_menu() {
            add_options_page('Propertybase Integration', 'PB Integration', 'manage_options', __FILE__, array('FFDPBIntegrationPostAdmin', 'admin_interface'));
        }

        /**
         * Register Dashboard widget
         */
        public static function settings_dashboard_widgets() {
            wp_add_dashboard_widget('custom_settings_widget', 'Web Listings Sync', array('FFDPBIntegrationPostAdmin', 'custom_dashboard_settings'));
        }

        /**
         * Create Dashboard widget
         */
        public static function custom_dashboard_settings() {
            $settings = get_option('WebListing_Settings');
            if (isset($_POST["syncNow"])) {
                require_once(PBIP_PLUGIN_DIR . "class.FFDPBIntegrationPost.php");
                FFDPBIntegrationPost::mlsSync();
            }
            ?>
            <form method="post" action="" novalidate="novalidate">
                <input type="submit" class="button button-primary" value="Sync Now" name="syncNow" />
                <!--Admin Settings-->
                <table class="form-table">
                    <tr>
                        <th scope="row" style="width: 20% !important;">Status:</th>
                        <td>
                            <?php echo isset($settings["EnableSync"]) ? ($settings["EnableSync"] == "No" ? "Disabled" : (isset($settings["Status"]) ? $settings["Status"] : "Never Run")) : "Disabled" ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Last Sync:</th>
                        <td>
                            <?php echo isset($settings["LastRun"]) ? date("Y-m-d \ h:i:s T", strtotime($settings["LastRun"])) : 'Never' ?>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Next Run</th>
                        <td><?php echo date("Y-m-d \ h:i:s T", wp_next_scheduled("SyncListings")) ?></td>
                    </tr>
                </table>
            </form>
            <?php
        }

        /**
         * Create Admin Interfaces
         */
        public static function admin_interface() {
            $doOAuth = false;
            $settings = get_option('WebListing_Settings');
            
            if (!isset($settings))
                $settings = array('Prospect' => false, 'EndPoint' => '', 'Token' => '', 'Debug' => false);

            if (isset($_POST["syncNow"])) {
                require_once(PBIP_PLUGIN_DIR . "class.FFDPBIntegrationPost.php");

                FFDPBIntegrationPost::mlsSync();
            } else if (isset($_REQUEST['code']) && !isset($settings["OAuth"]["OAuthToken"])) {

                //Get the token
                $url = "https://login.salesforce.com/services/oauth2/token";

                if ($settings["Debug"] == "Yes")
                    $url = "https://test.salesforce.com/services/oauth2/token";

                $body = "grant_type=authorization_code&client_id=" . $settings["OAuth"]["OAuthClientId"] . "&client_secret=" . $settings["OAuth"]["OAuthSecret"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "&code=" . $_REQUEST['code'];

                $ch = curl_init($url . "?" . $body);

                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                $token = curl_exec($ch);

                if (false == $token) {
                    $err = curl_error($ch);
                } else {
                    $var = json_decode($token);

                    if (isset($var->access_token)) {
                        $settings["OAuth"]["OAuthToken"] = $var->access_token;
                        $settings["OAuth"]["RefreshToken"] = $var->refresh_token;
                        $settings["OAuth"]["InstanceUrl"] = $var->instance_url;
                        update_option("WebListing_Settings", $settings);
                    }
                }

                curl_close($ch);
            } else if (isset($_POST['submit'])) {
                if (isset($_POST["ClientId"])) //Check to see if this is oauth
                {
                    $doOAuth = true;
                    $settings["OAuth"]["OAuthClientId"] = $_POST["ClientId"];
                    $settings["OAuth"]["OAuthSecret"] = $_POST["Secret"];
                    $settings["Debug"] = isset($_POST["WebListingDebug"]) ? "Yes" : "No";

                    update_option("WebListing_Settings", $settings);
                } else if (isset($_POST["ResetOAuth"])) {
                    $settings["OAuth"]["OAuthToken"] = NULL;
                    update_option("WebListing_Settings", $settings);
                } else {
                    $ok = true;
                    $message = '<p>Please Correct the Following Errors</p>';

                    if ($ok) {
                        $settings["Prospect"] = isset($_POST["EnableWebProspect"]) ? true : false;
                        $settings["Debug"] = isset($_POST["WebListingDebug"]) ? "Yes" : "No";
                        $settings["EnableSync"] = isset($_POST["EnableSync"]) ? "Yes" : "No";
                        $settings["GoogleAPIKey"] = $_POST["GoogleAPIKey"];
                        $settings["ExcludeMapsAPI"] = isset($_POST["ExcludeMapsAPI"]) ? "Yes" : "No";

                        $settings["ProspectMappings"] = array();
                        if (isset($_POST["MapFirstName"]) && $_POST["MapFirstName"] != "")
                            $settings["ProspectMappings"]["FirstName"] = $_POST["MapFirstName"];
                        if (isset($_POST["MapLastName"]) && $_POST["MapLastName"] != "")
                            $settings["ProspectMappings"]["LastName"] = $_POST["MapLastName"];
                        if (isset($_POST["MapEmail"]) && $_POST["MapEmail"] != "")
                            $settings["ProspectMappings"]["Email"] = $_POST["MapEmail"];
                        if (isset($_POST["MapSaveSearchId"]) && $_POST["MapSaveSearchId"] != "")
                            $settings["ProspectMappings"]["MapSaveSearchId"] = $_POST["MapSaveSearchId"];
                        if (isset($_POST["MapPhone"]) && $_POST["MapPhone"] != "")
                            $settings["ProspectMappings"]["Phone"] = $_POST["MapPhone"];
                        if (isset($_POST["MapCust1PB"]) && isset($_POST["MapCust1WP"]) && $_POST["MapCust1PB"] != "" && $_POST["MapCust1WP"] != "")
                            $settings["ProspectMappings"]["MapCust1"] = array($_POST["MapCust1PB"], $_POST["MapCust1WP"]);
                        if (isset($_POST["MapCust2PB"]) && isset($_POST["MapCust2WP"]) && $_POST["MapCust2PB"] != "" && $_POST["MapCust2WP"] != "")
                            $settings["ProspectMappings"]["MapCust2"] = array($_POST["MapCust2PB"], $_POST["MapCust2WP"]);
                        if (isset($_POST["MapCust3PB"]) && isset($_POST["MapCust3WP"]) && $_POST["MapCust3PB"] != "" && $_POST["MapCust3WP"] != "")
                            $settings["ProspectMappings"]["MapCust3"] = array($_POST["MapCust3PB"], $_POST["MapCust3WP"]);
                        if (isset($_POST["MapCust4PB"]) && isset($_POST["MapCust4WP"]) && $_POST["MapCust4PB"] != "" && $_POST["MapCust4WP"] != "")
                            $settings["ProspectMappings"]["MapCust4"] = array($_POST["MapCust4PB"], $_POST["MapCust4WP"]);
                        if (isset($_POST["MapCust5PB"]) && isset($_POST["MapCust5WP"]) && $_POST["MapCust5PB"] != "" && $_POST["MapCust5WP"] != "")
                            $settings["ProspectMappings"]["MapCust5"] = array($_POST["MapCust5PB"], $_POST["MapCust5WP"]);
                        $settings["SelectFields"] = array();
                        //setup post mappings
                        foreach ($_POST as $key => $value) {
                            if ((substr($key, 0, 5) == "post_" || substr($key, 0, 7) == "PBmeta_" || substr($key, 0, 5) == "meta_") && isset($value) && $value != "")
                                $settings["SelectFields"][$key] = $value;
                        }

                        //Setup where clauses
                        if (isset($_POST["WHField1"]) && $_POST["WHField1"] != "" && isset($_POST["WHValue1"]) && $_POST["WHValue1"] != "") {
                            $settings["WHERE"]["Condition1"]["Field"] = $_POST["WHField1"];
                            $settings["WHERE"]["Condition1"]["Value"] = $_POST["WHValue1"];
                            $settings["WHERE"]["Condition1"]["Type"] = $_POST["WHType1"];
                        } else {
                            $settings["WHERE"]["Condition1"] = NULL;
                            $settings["WHERE"]["Condition2"] = NULL;
                            $settings["WHERE"]["Condition3"] = NULL;
                        }

                        if (isset($_POST["WHField2"]) && $_POST["WHField2"] != "" && isset($_POST["WHValue2"]) && $_POST["WHValue2"] != "") {
                            $settings["WHERE"]["Condition2"]["Field"] = $_POST["WHField2"];
                            $settings["WHERE"]["Condition2"]["Value"] = $_POST["WHValue2"];
                            $settings["WHERE"]["Condition2"]["Type"] = $_POST["WHType2"];
                        } else {
                            $settings["WHERE"]["Condition2"] = NULL;
                            $settings["WHERE"]["Condition3"] = NULL;
                        }

                        if (isset($_POST["WHField3"]) && $_POST["WHField3"] != "" && isset($_POST["WHValue3"]) && $_POST["WHValue3"] != "") {
                            $settings["WHERE"]["Condition3"]["Field"] = $_POST["WHField3"];
                            $settings["WHERE"]["Condition3"]["Value"] = $_POST["WHValue3"];
                            $settings["WHERE"]["Condition3"]["Type"] = $_POST["WHType3"];
                        } else {
                            $settings["WHERE"]["Condition3"] = NULL;
                        }

                        //Setup delete clauses
                        if (isset($_POST["DELField1"]) && $_POST["DELField1"] != "" && isset($_POST["DELValue1"]) && $_POST["DELValue1"] != "") {
                            $settings["DELETE"]["Condition1"]["Field"] = $_POST["DELField1"];
                            $settings["DELETE"]["Condition1"]["Value"] = $_POST["DELValue1"];
                            $settings["DELETE"]["Condition1"]["Type"] = $_POST["DELType1"];
                        } else {
                            $settings["DELETE"]["Condition1"] = NULL;
                            $settings["DELETE"]["Condition2"] = NULL;
                            $settings["DELETE"]["Condition3"] = NULL;
                        }

                        if (isset($_POST["DELField2"]) && $_POST["DELField2"] != "" && isset($_POST["DELValue2"]) && $_POST["DELValue2"] != "") {
                            $settings["DELETE"]["Condition2"]["Field"] = $_POST["DELField2"];
                            $settings["DELETE"]["Condition2"]["Value"] = $_POST["DELValue2"];
                            $settings["DELETE"]["Condition2"]["Type"] = $_POST["DELType2"];
                        } else {
                            $settings["DELETE"]["Condition2"] = NULL;
                            $settings["DELETE"]["Condition3"] = NULL;
                        }

                        if (isset($_POST["DELField3"]) && $_POST["DELField3"] != "" && isset($_POST["DELValue3"]) && $_POST["DELValue3"] != "") {
                            $settings["DELETE"]["Condition3"]["Field"] = $_POST["DELField3"];
                            $settings["DELETE"]["Condition3"]["Value"] = $_POST["DELValue3"];
                            $settings["DELETE"]["Condition3"]["Type"] = $_POST["DELType3"];
                        } else {
                            $settings["DELETE"]["Condition3"] = NULL;
                        }

                        $result = update_option("WebListing_Settings", $settings);
                        echo "<p>Settings Updated</p>";
                    } else {
                        echo $message;
                    }
                }
            }
            ?>
            <div class="wrap" style="width: 50%;">
                <form method="post" action="" novalidate="novalidate">
                    <?php if (isset($settings["OAuth"]["OAuthToken"])) { ?>
                        <?php
                        $items = self::getPBFields($settings);
                        asort($items);
                        ?>
                        <h1>Web Listings Settings</h1>
                        <input type="submit" class="button button-primary" value="Sync Now" name="syncNow" />
                        <!--Admin Settings-->
                        <table class="form-table">
                            <tr>
                                <th scope="row">Status:</th>
                                <td>
                                    <?php echo isset($settings["EnableSync"]) ? ($settings["EnableSync"] == "No" ? "Disabled" : (isset($settings["Status"]) ? $settings["Status"] : "Never Run")) : "Disabled" ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Last Sync:</th>
                                <td>
                                    <?php echo isset($settings["LastRun"]) ? date("Y-m-d \ h:i:s T", strtotime($settings["LastRun"])) : 'Never' ?>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Next Run</th>
                                <td><?php echo date("Y-m-d \ h:i:s T", wp_next_scheduled("SyncListings")) ?></td>
                            </tr>
                            <tr>
                                <th scope="row">Enable Listing Sync</th>
                                <td>
                                    <input name="EnableSync" type="checkbox" <?php echo isset($settings["EnableSync"]) && $settings["EnableSync"] == 'Yes' ? 'checked="true"' : '' ?> />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Reset OAuth</th>
                                <td>
                                    <input name="ResetOAuth" type="checkbox" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Sandbox</th>
                                <td>
                                    <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"] == 'Yes' ? 'checked="true"' : '' ?> />
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" scope="row">Google Maps API Settings:</th>
                            </tr>
                            <tr>
                                <th scope="row">API Key</th>
                                <td>
                                    <input name="GoogleAPIKey" type="text" value='<?php echo isset($settings["GoogleAPIKey"]) ? $settings["GoogleAPIKey"] : "" ?>' />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Don't Include Google Maps API Code</th>
                                <td>
                                    <input name="ExcludeMapsAPI" type="checkbox" <?php echo isset($settings["ExcludeMapsAPI"]) && $settings["ExcludeMapsAPI"] == 'Yes' ? 'checked="true"' : '' ?> />
                                </td>
                            </tr>
                        </table>
                        <!--Propertybase Field Mappings-->
                        <table class="form-table">
                            <tr>
                                <th colspan="2" scope="row">Propertybase Field Mappings:</th>
                            </tr>
                            <tr>
                                <td>Post Field</td>
                                <td>Propertybase Field</td>
                            </tr>
                            <tr>
                                <td>Post Title</td>
                                <td>
                                    <select id="post_title" class="sfField postField" name="post_title">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["SelectFields"]["post_title"]) && $settings["SelectFields"]["post_title"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Post Name (Used for URL's)</td>
                                <td>
                                    <select id="post_name" class="sfField postField" name="post_name">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["SelectFields"]["post_name"]) && $settings["SelectFields"]["post_name"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Post Content</td>
                                <td>
                                    <select id="post_content" class="sfField postField" name="post_content">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["SelectFields"]["post_content"]) && $settings["SelectFields"]["post_content"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Post Excerpt</td>
                                <td>
                                    <select id="post_excerpt" class="sfField postField" name="post_excerpt">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["SelectFields"]["post_excerpt"]) && $settings["SelectFields"]["post_excerpt"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>PB Field</td>
                                <td>Meta Field</td>
                            </tr>
                            <?php for ($x = 0; $x < 50; $x++) { ?>
                                <tr>
                                    <td>
                                        <?php echo $x + 1 . '. ' ?>
                                        <select class="sfField" onChange="setMetaName('PBmeta_<?php echo $x ?>','meta_<?php echo $x ?>');" id="PBmeta_<?php echo $x ?>" name="PBmeta_<?php echo $x ?>">
                                            <?php foreach ($items as $key => $value) {
                                                if (isset($settings["SelectFields"]["PBmeta_" . $x]) && $settings["SelectFields"]["PBmeta_" . $x] == $key) {
                                                    echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                                } else {
                                                    echo '<option value="' . $key . '">' . $value . '</option>';
                                                }
                                            } ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input class="metaField" style="width:250px;" id="meta_<?php echo $x; ?>" name="meta_<?php echo $x; ?>" value="<?php echo isset($settings["SelectFields"]["meta_" . $x]) ? $settings["SelectFields"]["meta_" . $x] : "" ?>" type="text" />
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                        <!--Where Clauses-->
                        <table class="form-table">
                            <tr>
                                <th colspan="4">Where Clauses:</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <select name="WHField1">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["WHERE"]["Condition1"]["Field"]) && $settings["WHERE"]["Condition1"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="WHType1">
                                        <option value="=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["WHERE"]["Condition1"]["Type"]) && $settings["WHERE"]["Condition1"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="WHValue1" type="text" value="<?php echo isset($settings["WHERE"]["Condition1"]["Value"]) ? $settings["WHERE"]["Condition1"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    And
                                </td>
                                <td>
                                    <select name="WHField2">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["WHERE"]["Condition2"]["Field"]) && $settings["WHERE"]["Condition2"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="WHType2">
                                        <option value="=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["WHERE"]["Condition2"]["Type"]) && $settings["WHERE"]["Condition2"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="WHValue2" type="text" value="<?php echo isset($settings["WHERE"]["Condition2"]["Value"]) ? $settings["WHERE"]["Condition2"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    And
                                </td>
                                <td>
                                    <select name="WHField3">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["WHERE"]["Condition3"]["Field"]) && $settings["WHERE"]["Condition3"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="WHType3">
                                        <option value="=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["WHERE"]["Condition3"]["Type"]) && $settings["WHERE"]["Condition3"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="WHValue3" type="text" value="<?php echo isset($settings["WHERE"]["Condition3"]["Value"]) ? $settings["WHERE"]["Condition3"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                        </table>
                        <!--Delete Rules-->
                        <table class="form-table">
                            <tr>
                                <th colspan="4">Delete Rules<br />Listings Matching The Following Conditions will Be Deleted
                                </th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <select name="DELField1">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["DELETE"]["Condition1"]["Field"]) && $settings["DELETE"]["Condition1"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="DELType1">
                                        <option value="=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["DELETE"]["Condition1"]["Type"]) && $settings["DELETE"]["Condition1"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="DELValue1" type="text" value="<?php echo isset($settings["DELETE"]["Condition1"]["Value"]) ? $settings["DELETE"]["Condition1"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Or
                                </td>
                                <td>
                                    <select name="DELField2">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["DELETE"]["Condition2"]["Field"]) && $settings["DELETE"]["Condition2"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="DELType2">
                                        <option value="=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["DELETE"]["Condition2"]["Type"]) && $settings["DELETE"]["Condition2"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="DELValue2" type="text" value="<?php echo isset($settings["DELETE"]["Condition2"]["Value"]) ? $settings["DELETE"]["Condition2"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Or
                                </td>
                                <td>
                                    <select name="DELField3">
                                        <?php foreach ($items as $key => $value) {
                                            if (isset($settings["DELETE"]["Condition3"]["Field"]) && $settings["DELETE"]["Condition3"]["Field"] == $key) {
                                                echo '<option value="' . $key . '" selected="true">' . $value . '</option>';
                                            } else {
                                                echo '<option value="' . $key . '">' . $value . '</option>';
                                            }
                                        } ?>
                                    </select>
                                </td>
                                <td>
                                    <select name="DELType3">
                                        <option value="=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"] == "=") ? "selected='true'" : "" ?>>Equals</option>
                                        <option value="!=" <?php echo (isset($settings["DELETE"]["Condition3"]["Type"]) && $settings["DELETE"]["Condition3"]["Type"] == "!=") ? "selected='true'" : "" ?>>Not Equal To</option>
                                    </select>
                                </td>
                                <td>
                                    <input name="DELValue3" type="text" value="<?php echo isset($settings["DELETE"]["Condition3"]["Value"]) ? $settings["DELETE"]["Condition3"]["Value"] : "" ?>" />
                                </td>
                            </tr>
                        </table>
                        <!--  <table>
                            <tr>
                                <th scope="row">Save Contacts on Registration:</th>
                                <td>
                                    <input name="EnableWebProspect" type="checkbox" <?php /*echo (isset($settings["Prospect"]) && $settings["Prospect"]) ? 'checked="true"' : '' */ ?> />
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2" scope="row">Prospect Mappings</th>
                            </tr>
                            <tr>
                                <td>PropertyBase Field</td>
                                <td>WordPress Field</td>
                            </tr>
                            <tr>
                                <td>
                                    FirstName
                                </td>
                                <td>
                                    <input name="MapFirstName" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["FirstName"]) ? $settings["ProspectMappings"]["FirstName"] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    LastName
                                </td>
                                <td>
                                    <input name="MapLastName" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["LastName"]) ? $settings["ProspectMappings"]["LastName"] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Email
                                </td>
                                <td>
                                    <input name="MapEmail" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["Email"]) ? $settings["ProspectMappings"]["Email"] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Phone
                                </td>
                                <td>
                                    <input name="MapPhone" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["Phone"]) ? $settings["ProspectMappings"]["Phone"] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="MapCust1PB" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust1"][0]) ? $settings["ProspectMappings"]["MapCust1"][0] : "" */ ?>" />
                                </td>
                                <td>
                                    <input name="MapCust1WP" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust1"][1]) ? $settings["ProspectMappings"]["MapCust1"][1] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="MapCust2PB" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust2"][0]) ? $settings["ProspectMappings"]["MapCust2"][0] : "" */ ?>" />
                                </td>
                                <td>
                                    <input name="MapCust2WP" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust2"][1]) ? $settings["ProspectMappings"]["MapCust2"][1] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="MapCust3PB" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust3"][0]) ? $settings["ProspectMappings"]["MapCust3"][0] : "" */ ?>" />
                                </td>
                                <td>
                                    <input name="MapCust3WP" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust3"][1]) ? $settings["ProspectMappings"]["MapCust3"][1] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="MapCust4PB" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust4"][0]) ? $settings["ProspectMappings"]["MapCust4"][0] : "" */ ?>" />
                                </td>
                                <td>
                                    <input name="MapCust4WP" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust4"][1]) ? $settings["ProspectMappings"]["MapCust4"][1] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input name="MapCust5PB" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust5"][0]) ? $settings["ProspectMappings"]["MapCust5"][0] : "" */ ?>" />
                                </td>
                                <td>
                                    <input name="MapCust5WP" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapCust5"][1]) ? $settings["ProspectMappings"]["MapCust5"][1] : "" */ ?>" />
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2">
                                    Save Search Settings
                                </th>
                            </tr>
                            <tr>
                                <td>Contact Form Id</td>
                                <td>
                                    <input name="MapSaveSearchId" type="text" value="<?php /*echo isset($settings["ProspectMappings"]["MapSaveSearchId"]) ? $settings["ProspectMappings"]["MapSaveSearchId"] : "" */ ?>" />
                                </td>
                            </tr>
                            </tbody>
                        </table>-->
                        <?php submit_button(); ?>
                    <?php } else { ?>
                        <?php if ($doOAuth) { ?>
                            <?php if ($settings["Debug"] == "Yes") {
                                echo "<script>window.location='https://test.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
                            } else {
                                echo "<script>window.location='https://login.salesforce.com/services/oauth2/authorize?response_type=code&client_id=" . $_POST["ClientId"] . "&redirect_uri=" . urlencode('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME']) . "';</script>";
                            } ?>
                        <?php } ?>
                        <h1>OAuth Configuration</h1>
                        <h3>You must authorize Wordpress to be able to access your salesforce site.</h3>
                        <table class="form-table">
                            <tbody>
                            <tr>
                                <th scope="row">Sandbox</th>
                                <td>
                                    <input name="WebListingDebug" type="checkbox" <?php echo $settings["Debug"] == 'Yes' ? 'checked="true"' : '' ?> />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Client ID</th>
                                <td>
                                    <input name="ClientId" type="text" value="<?php echo isset($settings["OAuth"]['OAuthClientId']) ? $settings["OAuth"]['OAuthClientId'] : '' ?>" />
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Secret</th>
                                <td>
                                    <input name="Secret" type="text" value="<?php echo isset($settings["OAuth"]['OAuthSecret']) ? $settings["OAuth"]['OAuthSecret'] : '' ?>" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <?php submit_button(); ?>
                    <?php } ?>
                </form>
            </div>
            <script>
                function setMetaName(parentId, childId) {
                    if (jQuery("#" + childId).val() === "")
                        jQuery("#" + childId).val(jQuery("#" + parentId).val().toLowerCase());

                    disableSelectedOptions();
                }

                function disableSelectedOptions() {
                    var selected = [];

                    jQuery(".sfField").each(function () {
                        jQuery("#" + jQuery(this).attr("id") + " option").removeAttr("disabled");

                        if (jQuery(this).val() != "")
                            selected.push(jQuery(this).val());
                    });

                    jQuery.each(selected, function (index, val) {
                        var $selected = val;

                        jQuery(".sfField").each(function () {
                            if (jQuery(this).val() == $selected) return;

                            jQuery("#" + jQuery(this).attr("id") + " option[value='" + $selected + "']").attr("disabled", "disabled");
                        });
                    });
                }

                jQuery("window").ready(function () {
                    disableSelectedOptions();

                    jQuery(".postField").change(function () {
                        disableSelectedOptions();
                    })
                });
            </script>
            <?php
        }
    }