<?php get_header(); ?>
    <div id="main-content">
        <div class="topCategoryOrange">
            <ul class="sidenav-sublist">
                <li><a href="/blog/">All</a></li>
                <?php wp_list_categories([
                    'exclude'      => '1',
                    'exclude_tree' => '',
                    'order'        => 'ASC',
                    'orderby'      => 'name',
                    'title_li'     => '',
                ]); ?>
            </ul>
            <div class="blogSignupBox">
                <form action="" method="post">
                    <input type="text" id="" placeholder="Enter your Email to get our Newsletter">
                    <a href="#" class="joinButton">Join</a>
                </form>
            </div>
        </div>
        <div class="container">
            <div id="content-area" class="clearfix">
                <div id="left-area">
                    <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post();
                            $post_format = et_pb_post_format(); ?>
                            <article id="post-<?php the_ID(); ?>" <?php post_class('et_pb_post'); ?>>
                                <?php
                                    $thumb = '';
                                    $width = (int)apply_filters('et_pb_index_blog_image_width', 1080);
                                    $height = (int)apply_filters('et_pb_index_blog_image_height', 675);
                                    $classtext = 'et_pb_post_main_image';
                                    $titletext = get_the_title();
                                    $thumbnail = get_thumbnail($width, $height, $classtext, $titletext, $titletext, false, 'Blogimage');
                                    $thumb = $thumbnail["thumb"];

                                    et_divi_post_format_content();
                                ?>
                                <div class="post-image">
                                    <a href="<?php the_permalink(); ?>">
                                    <img src="/<?php echo FOX\Image\ImageResizer::imageResize(get_post_thumbnail_id(get_the_ID()), 415, 285, true, array()) ?>" alt="<?php the_title() ?>">
                                    </a>
                                </div>
                                <div class="post-content">
                                    <h2 class="entry-title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                    <?php et_divi_post_meta(); ?>
                                    <?php truncate_post(130); ?>
                                </div>
                            </article>
                        <?php endwhile; ?>
                        <?php
                        if (function_exists('wp_pagenavi')) {
                            wp_pagenavi();
                        } else {
                            get_template_part('includes/navigation', 'index');
                        } ?>
                    <?php else :
                        get_template_part('includes/no-results', 'index');
                    endif; ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>