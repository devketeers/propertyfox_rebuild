<?php
    /**
     * Template Name: Debug
     */

    ini_set('max_execution_time', 3000);
    ini_set('memory_limit', '1024M');

    require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');

    if ($_GET['process'] == 'file') {
        $files = glob('locations/*.{json}', GLOB_BRACE);
        $k = 0;
        foreach ($files as $file) {
            $cleanName = str_replace(['.json', 'locations/'], '', $file);
            $province = str_replace('-', ' ', $cleanName);

            /*     if ($province != 'Eastern Cape') {
                     continue;
                 }*/

            //Read file data
            $provinces = fopen($file, "r") or die("Unable to open file!");
            $jsonData = fread($provinces, filesize($file));
            $json = json_decode($jsonData);

            /*      echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                  print_r(json_encode($json, JSON_PRETTY_PRINT));
                  echo '</pre>';*/


            try {
                $parentProvince = Parent($province);


                foreach ($json as $City => $Towns) {

                    $parentCity = Child($parentProvince, $City);
                    foreach ($Towns as $Town) {

                        $parentTown = Child($parentCity, $Town);

                        echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                        print_r($k . ' : ' . $province . ' - ' . $City . ' - ' . $Town);
                        echo '</pre>';

                        $k++;
                    }

                }

                unset($file);
                unset($jsonData);
            } catch (\Exception $ex) {
                echo $ex->getMessage();

            }


            //print_r(json_encode($array_to_json, '128'));

        }
        fclose($provinces);
        shutdown();

    }

    if ($_GET['process'] == 'link') {
        $args = ['post_type' => 'property', 'posts_per_page' => -1, 'order' => 'ASC'];
        $posts = new \WP_Query($args);

        if ($posts->have_posts()) {
            $properties = [];
            while ($posts->have_posts()) : $posts->the_post();
                $catIds = [];
                $province = get_post_meta(get_the_ID(), 'province__c', true);
                $city = get_post_meta(get_the_ID(), 'pba__city_pb__c', true);
                $suburb = get_post_meta(get_the_ID(), 'pba__area_pb__c', true);
                $id = get_post_meta(get_the_ID(), 'pf_number__c', true);
                $properties[$province][$city][] = $suburb;

                $term_p = term_exists($province, 'location_category');
                $term_c = term_exists($city, 'location_category');
                $term_s = term_exists($suburb, 'location_category');

                echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                print_r(get_the_title());
                echo '</pre>';

                if (0 !== $term_p && NULL !== $term_p) {
                    array_push($catIds, $term_p['term_id']);
                } else {
                    echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                    print_r('Error Province: ' . $id . ' - ' . get_the_title() . ' - ' . $province);
                    echo '</pre>';
                }

                if (0 !== $term_c && NULL !== $term_c) {
                    array_push($catIds, $term_c['term_id']);
                } else {
                    echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                    print_r('Error City/Area: ' . $id . ' - ' . get_the_title() . ' - ' . $city);
                    echo '</pre>';
                }

                if (0 !== $term_s && NULL !== $term_s) {
                    array_push($catIds, $term_s['term_id']);
                } else {
                    echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                    print_r('Error Suburb: ' . $id . ' - ' . get_the_title() . ' - ' . $suburb);
                    echo '</pre>';
                }

                echo '<br><hr><br>';

                $setTerms = wp_set_post_terms(get_the_ID(), $catIds, 'location_category', true);

                echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                print_r($setTerms);
                echo '</pre>';

                echo '<br><hr><br>';

            endwhile;

        }
    }

    if ($_GET['process'] == 'import') {
        // Create array for loop
        if (($handle = fopen('https://www.property24.com/General/GetSuburbsCsv', 'r')) !== false) {
            // get the first row, which contains the column-titles (if necessary)
            $header = fgetcsv($handle);
            $array_to_json = [];
            while (($data = fgetcsv($handle)) !== false) {
                if ($data[0] != 'South Africa') {
                    continue;
                }
                try {
                    $array_to_json[$data[1]][$data[2]][] = $data[3];
                } catch (\Exception $ex) {
                    echo $ex->getMessage();
                }
                unset($data);
            }
            fclose($handle);

            //$json = serialize(json_encode($array_to_json));
            echo json_encode($array_to_json, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        // Loop properties and add to
        try {
            if (!empty($array_to_json)) {
                $parentProvince = '';
                foreach ($array_to_json as $Province => $Cities) {
                    $parentProvince = Parent($Province);
                    foreach ($Cities as $City => $Towns) {
                        $parentCity = Child($parentProvince, $City);
                        foreach ($Towns as $Town) {
                            $parentTown = Child($parentCity, $Town);
                        }
                    }
                    echo '<pre style="clear:both;position:relative;z-index:9999;background-color:lightgrey;color:red;border:1px orange solid;padding:10px;">';
                    print_r($Province);
                    echo '</pre>';
                }
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            shutdown();
        }
    }

    if ($_GET['process'] == 'delete') {
        $terms = get_terms('location_category', array('fields' => 'ids', 'hide_empty' => false));
        foreach ($terms as $value) {
            wp_delete_term($value, 'location_category');
        }
    }