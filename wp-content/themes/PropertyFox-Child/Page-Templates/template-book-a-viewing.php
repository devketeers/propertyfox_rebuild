<?php
    /*
    Template Name: Book a Viewing
    */
    get_header();
?>
<?php get_header(); ?>
    <div class="grid">
        <div class="col-1-1">
            <?php while (have_posts()) : the_post(); ?>
                <h1><?php the_title() ?></h1>
                <section class="page-content">
                    <?php if (isset($_POST['mid']) || isset($_GET['reference'])) { ?>
                        <h2 class="property-title"><?php echo($_POST['mid'] ? get_the_title($_POST['mid']) : get_the_title($_GET['reference'])) ?></h2>
                    <?php } else { ?>
                        <h2 class="property-title"><?php echo get_the_title($_COOKIE['listing_last_view']); ?></h2>
                    <?php } ?>
                    <?php the_content() ?>
                </section>
            <?php endwhile; ?>
        </div>
    </div>
<?php get_footer(); ?>