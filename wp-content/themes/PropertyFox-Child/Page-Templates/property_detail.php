<?php
    /*
    Template Name: Property Detail
    */

    wp_enqueue_style("slick", get_stylesheet_directory_uri() . "/css/slick.css", false, '1.0.0', 'all');
    wp_enqueue_style("slicktheme", get_stylesheet_directory_uri() . "/css/slick-theme.css", false, '1.0.0', 'all');
    wp_enqueue_script("slickjs", get_stylesheet_directory_uri() . "/js/slick.js", false, '1.0.0', true);
    wp_enqueue_script("listingdetailjs", get_stylesheet_directory_uri() . "/js/listing-detail.js", false, '1.0.0', true);

    get_header();

    global $post;
    $id = $post->ID;

    $helper = new \FOX\HelperClass\HelperClass();
    $media = $helper::pf_get_media($id);
    $fp = $helper::pf_get_floorplan($id);
    $video = $helper::pf_get_videos($id);
    $img = "";  //TODO: Default Image

    if (is_array($media) && sizeof($media) > 0) {
        $img = $media[0];
    }

    //Photo Gallery
    pf_get_template_part("Photo-Gallery-Part", array());
?>
    <span style="display:none;" id="galleryatts"
            data-allFP="<?php echo join(";", $fp) ?>"
            data-price="<?php echo get_post_meta($id, "listing_price__c", true) ?>"
            data-title="<?php echo $post->post_title ?>"
            data-allImg="<?php echo join(";", $media) ?>"
            data-allVid="<?php echo join(";", $video) ?>">
    </span>
    <span style="display:none;" id="mapatts"
            data-lat="<?php echo get_post_meta($id, "pba__latitude_pb__c", true) ?>"
            data-lng="<?php echo get_post_meta($id, "pba__longitude_pb__c", true) ?>"
            data-base="<?php echo get_stylesheet_directory_uri() ?>">
    </span>
    <div class="main-wrap">
        <section class="main-content-section listing-page">
            <section class="hero-section gallery" style="background-image:url(<?php echo $img ?>);">
                <div class="list-action-wrap">
                    <div class="common-wrap clear">
                        <div class="hero-link-wrap">
                            <a href="#" class="Show">
                            <span><?php echo get_post_meta($id, "pba__status__c", true); ?></span>
                            </a>
                            <a href="#" class="gallery">
                            <span>Photos</span>
                            <div class="icon-wrap clear">
                                <span class="num"> <?php echo sizeof($media); ?></span>
                                <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-photos-black.svg" alt="" class="image-counting">
                            </div>
                            </a>
                            <?php if (is_array($fp) && sizeof($fp) > 0) { ?>
                                <a href="#" class="floorplan">
                                <span>Floorplan</span>
                                <div class="icon-wrap clear">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-floorplan-black.svg" alt="floor plan">
                                </div>
                                </a>
                            <?php } ?>
                            <?php if (is_array($video) && sizeof($video) > 0) { ?>
                                <a href="#" class="video">
                                <span>Video</span>
                                <div class="icon-wrap clear">
                                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-video-black.svg" alt="property video">
                                </div>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
                    $back_to_search_url = add_query_arg(wp_parse_args($_SESSION['search'], array()), '/listing-search');
                    unset($_SESSION['search']);
                ?>
                <div class="back-page">
                    <a href="<?php echo $back_to_search_url ?>">Back</a>
                </div>
            </section>
            <div class="content-inner-wrap">
                <!--Breadcrumbs-->
                <div class="breadcum-wrap">
                    <div class="common-wrap">
                        <div class="breadcum-link">
                            <a href="<?php echo $back_to_search_url ?>">Back to Results</a>
                            <?php
                                $params = '';
                                $splitter = "?";
                            ?>
                            <a href="<?php echo get_site_url(); ?>/listing-search">Property for Sale</a>
                            <?php if (get_post_meta($id, "pba__state_pb__c", true)) {
                                $params .= $splitter . "province=" . get_post_meta($id, "pba__state_pb__c", true);
                                $splitter = "&"; ?>
                                <a href="<?php echo get_site_url(); ?>/listing-search<?php echo $params ?>">
                                <?php echo get_post_meta($id, "pba__state_pb__c", true) ?>
                                </a>
                            <?php } ?>
                            <?php if (get_post_meta($id, "pba__city_pb__c", true)) {
                                $params .= $splitter . "city=" . get_post_meta($id, "pba__city_pb__c", true);
                                $splitter = "&"; ?>
                                <a href="<?php echo get_site_url(); ?>/listing-search<?php echo $params; ?>">
                                <?php echo get_post_meta($id, "pba__city_pb__c", true) ?>
                                </a>
                            <?php } ?>
                            <?php if (get_post_meta($id, "pba__area_pb__c", true)) {
                                $params .= $splitter . "suburb=" . get_post_meta($id, "pba__area_pb__c", true);
                                $splitter = "&"; ?>
                                <a href="<?php echo get_site_url(); ?>/listing-search<?php echo $params ?>">
                                <?php echo get_post_meta($id, "pba__area_pb__c", true) ?>
                                </a>
                            <?php } ?>
                            <a href="#" class="active">
                            <?php echo get_post_meta($id, "pf_number__c", true) ?>
                            </a>
                        </div>
                    </div>
                </div>
                <!--Inner Content-->
                <section class="listing-des">
                    <div class="common-wrap clear">
                        <!--Property meta-->
                        <div class="listing-des-wrap">
                            <div class="listing-content-wrap">
                                <div class="price-info">
                                    <h3><?php echo(get_post_meta($post->ID, "listing_price__c", true) == 999999999 ? 'POA' : 'R ' . str_replace(",", " ", number_format(get_post_meta($post->ID, "listing_price__c", true)))); ?></h3>
                                    <h6> <?php echo $post->post_title ?> </h6>
                                    <a href="/bond-calculator/" class="mobi">Bond Costs</a>
                                </div>
                                <div class="content-list">
                                    <div class="listing-house-item">
                                        <div class="property-icon-wrap">
                                            <div class="property-icon-item">
                                                <?php if (get_post_meta($id, "bedrooms__c", true)) { ?>
                                                    <div class="property-single-icon">
                                                        <figure>
                                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-beds-grey.svg" alt="property beds">
                                                        </figure>
                                                        <span><?php echo get_post_meta($id, "bedrooms__c", true) ?></span>
                                                    </div>
                                                <?php } ?>
                                                <?php if (get_post_meta($id, "bathrooms__c", true)) { ?>
                                                    <div class="property-single-icon">
                                                        <figure>
                                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-baths-grey.svg" alt="property baths">
                                                        </figure>
                                                        <span><?php echo get_post_meta($id, "bathrooms__c", true) ?></span>
                                                    </div>
                                                <?php } ?>
                                                <?php if (get_post_meta($id, "garages__c", true)) { ?>
                                                    <div class="property-single-icon">
                                                        <figure>
                                                            <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-garage-grey.svg" alt="property garage">
                                                        </figure>
                                                        <span><?php echo get_post_meta($id, "garages__c", true) ?></span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php if (get_post_meta($id, "land_size__c", true)) { ?>
                                                <?php switch (get_post_meta($id, "measurement_type__c", true)) {
                                                    case "Hectares":
                                                        $measurement = "ha";
                                                        break;
                                                    case "Metres Squared":
                                                        $measurement = 'm²';
                                                        break;
                                                } ?>
                                                <p>Erf size:
                                                    <span><?php echo get_post_meta($id, "land_size__c", true) . $measurement; ?></span>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="bond-cost-link">
                                        <a href="/bond-calculator/" target="_blank" class="desktop">Calculate Bond Costs</a>
                                    </div>
                                </div>
                                <div class="listing-title-wrap">
                                    <div class="listing-title-content">
                                        <?php echo $post->post_content ?>
                                    </div>
                                    <div class="expand-btn">
                                        <a href="#"><span>Read more</span> <span>Read less</span></a>
                                    </div>
                                </div>
                                <!--Property Details-->
                                <div class="property-details">
                                    <p>Property Details</p>
                                    <div class="property-coll">
                                        <?php if (get_post_meta($id, "pf_number__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Web Reference</span> <dfn>
                                                    <?php echo get_post_meta($id, "pf_number__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "pba__propertytype__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Type of Property</span> <dfn>
                                                    <?php echo get_post_meta($id, "pba__propertytype__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "floor_size__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Floor Size</span> <dfn>
                                                    <?php echo get_post_meta($id, "floor_size__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "land_size__c", true)) { ?>
                                            <?php switch (get_post_meta($id, "measurement_type__c", true)) {
                                                case "Hectares":
                                                    $measurement = "ha";
                                                    break;
                                                case "Metres Squared":
                                                    $measurement = 'm²';
                                                    break;
                                            } ?>
                                            <div class="property-details-item">
                                                <span>Erf Size</span> <dfn>
                                                    <?php echo get_post_meta($id, "land_size__c", true) . $measurement ?></dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "monthly_rates__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Rates and Taxes</span>
                                                <dfn>R <?php echo get_post_meta($id, "monthly_rates__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--Rooms-->
                                <div class="property-details">
                                    <p>Rooms</p>
                                    <div class="property-coll">
                                        <?php if (get_post_meta($id, "bedrooms__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Bedrooms</span> <dfn>
                                                    <?php echo get_post_meta($id, "bedrooms__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "bathrooms__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Bathrooms</span> <dfn>
                                                    <?php echo get_post_meta($id, "bathrooms__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php if (get_post_meta($id, "kitchen__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Kitchens</span> <dfn>
                                                    <?php echo get_post_meta($id, "kitchen__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                        <?php
                                            $rooms = 0;

                                            if (get_post_meta($id, "lounge__c", true)) {
                                                $rooms += intval(get_post_meta($id, "lounge__c", true));
                                            }

                                            if (get_post_meta($id, "dining_rooms__c", true)) {
                                                $rooms += intval(get_post_meta($id, "dining_rooms__c", true));
                                            }
                                        ?>
                                        <?php if ($rooms) { ?>
                                            <div class="property-details-item">
                                                <span>Reception Rooms</span> <dfn>
                                                    <?php echo $rooms; ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>

                                        <?php if (get_post_meta($id, "study__c", true)) { ?>
                                            <div class="property-details-item">
                                                <span>Studies</span> <dfn>
                                                    <?php echo get_post_meta($id, "study__c", true) ?>
                                                </dfn>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!--External Features-->
                                <?php $externalFeatures = [
                                    'Garages'                => get_post_meta($id, "garages__c", true),
                                    'Flatlet'                => get_post_meta($id, "flatlet__c", true),
                                    'Pool'                   => get_post_meta($id, "pool__c", true),
                                    'Security'               => str_replace(';', ', ', get_post_meta($id, "security__c", true)),
                                    'Domestic Accommodation' => get_post_meta($id, "domestic_accommodation__c", true)
                                ]; ?>
                                <?php if ($cleanFeatures = array_filter($externalFeatures, 'strlen')) { ?>
                                    <div class="property-details">
                                        <p>External Features</p>
                                        <div class="property-coll">
                                            <?php foreach ($cleanFeatures as $title => $value) { ?>
                                                <div class="property-details-item">
                                                    <span><?php echo $title; ?></span> <dfn>
                                                        <?php echo $value; ?>
                                                    </dfn>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php if (get_post_meta($id, "extras__c", true)) { ?>
                                    <div class="property-details">
                                        <p>Extras</p>
                                        <div class="property-coll">
                                            <div class="property-details-item">
                                                <span><?php echo get_post_meta($id, "extras__c", true) ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="listing-sidebar">
                                <form action="/book-a-viewing?reference=<?php echo get_post_meta($id, "pf_number__c", true) ?>" method="post">
                                    <input type="hidden" name="mid" value="<?php echo get_the_ID() ?>">
                                    <input type="submit" class="bookButton desktop" value="Book a Viewing">
                                </form>
                                <div class="contact-info-wrap">
                                    <div class="contact-alert-wrap">
                                        <div class="contact-social">
                                            <?php echo \FOX\CustomShare\CustomShare::socialMediaButtons(true, true, false, true, false, true); ?>
                                        </div>
                                    </div>
                                    <div class="btn-wrap">
                                        <div class="btn btn-coll">Contact PropertyFox</div>
                                    </div>
                                    <div class="contact-form-wrap">
                                        <div class="contact-form-info">
                                            <p>Available 7 days a week, 8am-8pm</p>
                                            <div class="contact-info">
                                                <a href="tel:0876250369">087 625 0369</a>
                                            </div>
                                            <?php if (is_active_sidebar('fox_sidebar')) : ?>
                                                <?php dynamic_sidebar('fox_sidebar'); ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--POI Map-->
                        <div class="listing-tab-nav">
                            <ul>
                                <li id="togglepoi" class="active">
                                    <a href="#" title="Points of Interest" class="changeMap">Points of Interest</a>
                                </li>
                                <li id="togglestreet">
                                    <a href="#" title="Street Map" class="streetMap">Street Map</a>
                                </li>
                                <li id="togglestreetview">
                                    <a href="#" title="Street View" class="streetView">Street View</a>
                                </li>
                            </ul>
                        </div>
                        <div class="listing-service-content">
                            <div class="listing-service-item-wrap">
                                <div id="divMap" class="listing-map"></div>
                                <div class="listing-service-wrap">
                                    <div class="listing-service-item poi-education">
                                        <h6>Education</h6>
                                        <div class="listing-item-wraper">
                                        </div>
                                    </div>
                                    <div class="listing-service-item poi-worship">
                                        <h6>Worship</h6>
                                        <div class="listing-item-wraper">
                                        </div>
                                    </div>
                                    <div class="listing-service-item poi-shopping">
                                        <h6>Shopping</h6>
                                        <div class="listing-item-wraper">
                                        </div>
                                    </div>
                                    <div class="listing-service-item poi-other">
                                        <h6>Other Amenities</h6>
                                        <div class="listing-item-wraper">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="link-btn">
                                <a href="#" id="showPOI">Show All</a>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Homes near this listing-->
                <?php
                    //TODO: Pull based on Area

                    $data = array();

                    if (get_post_meta($id, "pba__state_pb__c", true)) {
                        $data["province"] = get_post_meta($id, "pba__state_pb__c", true);
                    }

                    if (get_post_meta($id, "pba__city_pb__c", true)) {
                        $data["city"] = get_post_meta($id, "pba__city_pb__c", true);
                    }

                    if (get_post_meta($id, "pba__area_pb__c", true)) {
                        $data["suburb"] = get_post_meta($id, "pba__area_pb__c", true);
                    }

                    $data["id"] = get_post_meta($id, "ListingId", true);

                    $result = $helper::pf_query_listings($data);
                ?>
                <?php if (count($result->posts) > 0) { ?>
                    <div class="property-item-wrap">
                        <div class="title-wrap">
                            <h4>Homes near this listing</h4>
                        </div>
                        <div class="common-wrap clear">
                            <div class="customer-slider-info" id="listing-slider">
                                <div class="customer-slider">
                                    <?php foreach ($result->posts as $listing) {
                                        pf_get_template_part("Listing-Slider-Part", array("listing" => $listing));
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <!--Call to action-->
            <div class="footer-call-to-action">
                <div class="get-started-wrap" id="get-started-wrap">
                    <h4>Save with the smart way to sell your home</h4>
                    <a href="/get-started/">Get Started</a>
                </div>
                <div class="bottom-fixed-btn">
                    <a href="tel:+27876250369" class="call-us">Call Us</a>
                    <a href="/book-a-viewing/?mid=<?php echo get_the_ID() ?>" class="booking">Book Viewing</a>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>