<?php
    /*
    Template Name: Property Search
    */

    if (isset($_POST["search"])) {
        $data = $_POST;

        if (isset($_POST["keywords"]) && $_POST["keywords"] == "Enter city, suburb or Web Ref")
            unset($data["keywords"]);

        unset($data["search"]);

        header("Location: " . get_site_url() . "/listing-search/?" . http_build_query($data));
        wp_die();
    }

    get_header();
?>
    <!-- Begin main-content section
                ================================ -->
    <section class="main-content-section listing-search-content">
        <!-- Begin hero section
        ======================= -->
        <section class="hero-section" style="background-image:url(<?php echo get_stylesheet_directory_uri() ?>/images/home-hero-banner.jpg);">
            <div class="hero-content-info clear">
                <h2 class="desktop">Search Properties</h2> <h2 class="mobi">Property Search</h2>
                <div class="hero-input-wrap">
                    <section class="list-menu-wrap">
                        <form action="#" method="post">
                            <div class="search-wrap">
                                <input type="text" name="keywords" value="Enter city, suburb or Web Ref">
                            </div>
                            <div class="search-feature-wrap">
                                <ul>
                                    <li>
                                        <span>Property Type</span>
                                        <div class="input-field-wrap property-type">
                                            <ul class="seacrh-feature input-field">
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="House" id="check-1">
                                                    <label for="check-1">House</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Apartment / Flat" id="check-2">
                                                    <label for="check-2">Apartment / Flat</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Townhouse" id="check-3">
                                                    <label for="check-3">Townhouse</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Vacant Land / Plot" id="check-4">
                                                    <label for="check-4">Vacant Land / Plot</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Farm" id="check-5">
                                                    <label for="check-5">Farm</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Commercial" id="check-6">
                                                    <label for="check-6">Commercial</label>
                                                    <div class="check-box"></div>
                                                </li>
                                                <li class="check-item">
                                                    <input type="checkbox" name="proptype[]" value="Industrial" id="check-7">
                                                    <label for="check-7">Industrial</label>
                                                    <div class="check-box"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <select name="minprice" class="styled-select" title="Min Price">
                                            <option value="">Min Price</option>
                                            <option value="100000">R 100 000</option>
                                            <option value="150000">R 150 000</option>
                                            <option value="200000">R 200 000</option>
                                            <option value="250000">R 250 000</option>
                                        </select>
                                    </li>
                                    <li>
                                        <select name="maxprice" class="styled-select" title="Max Price">
                                            <option value="">Max Price</option>
                                            <option value="100000">R 100 000</option>
                                            <option value="150000">R 150 000</option>
                                            <option value="200000">R 200 000</option>
                                            <option value="250000">R 250 000</option>
                                        </select>
                                    </li>
                                    <li>
                                        <!--<span>Beds</span>-->
                                        <select name="beds" class="styled-select">
                                            <option value="">Beds</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </li>
                                    <li class="check-item desktop">
                                        <input type="hidden" name="status[]" value="Active" />
                                        <input type="checkbox" name="status[]" value="Sold" id="value-1">
                                        <label for="value-1">Include Sold</label>
                                        <div class="check-box"></div>
                                    </li>
                                </ul>
                            </div>
                            <div class="sold-out-wrap mobi">
                                <div class="sold-out">
                                    <dfn>Include Sold</dfn>
                                    <label class="switch">
                                        <input type="checkbox"> <span class="slider"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="search-wrap search-submit">
                                <input type="submit" name="search" value="Search">
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </section>
        <!-- End hero section -->
        <div class="get-started-wrap" id="get-started-wrap">
            <h4>Save with the smart way to sell your home</h4>
            <a href="/get-started/">Get Started</a>
        </div>
    </section>
<?php get_footer() ?>