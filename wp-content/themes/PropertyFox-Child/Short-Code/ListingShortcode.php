<?php

    class ListingShortcode {
        /**
         * @param $atts
         */
        public static function do_shortCode($atts) {
            wp_enqueue_style("slick", get_stylesheet_directory_uri() . "/css/slick.css", false, '1.0.0', 'all');
            wp_enqueue_style("slicktheme", get_stylesheet_directory_uri() . "/css/slick-theme.css", false, '1.0.0', 'all');
            wp_enqueue_style("jqueryui", get_stylesheet_directory_uri() . "/css/jquery-ui.css", false, '1.0.0', 'all');

            wp_enqueue_script("slickjs", get_stylesheet_directory_uri() . "/js/slick.js", false, '1.0.0', true);

            $vars = shortcode_atts(array(
                'province' => '',
                'city'     => '',
                'suburb'   => '',
                'title'    => '',
                'items'    => '5'
            ), $atts);


            $data = array();

            if (isset($vars["province"])) {
                $data["province"] = $vars["province"];
            }

            if (isset($vars["city"])) {
                $data["city"] = $vars["city"];
            }

            if (isset($vars["suburb"])) {
                $data["suburb"] = $vars["suburb"];
            }

            if (isset($vars["items"])) {
                $data["itemsperpage"] = $vars["items"];
            }

            $data["id"] = get_post_meta($id, "ListingId", true);

            $helper = new \FOX\HelperClass\HelperClass();
            $result = $helper::pf_query_listings($data);

            //Photo Gallery
            pf_get_template_part("Photo-Gallery-Part", array()); ?>
            <div>
                <div class="title-wrap">
                    <h4><?php echo $vars["title"] ?></h4>
                </div>
                <div class="common-wrap clear">
                    <div class="customer-slider-info" id="listing-slider">
                        <div class="customer-slider">
                            <?php
                                foreach ($result->posts as $listing) {
                                    pf_get_template_part("Listing-Slider-Part", array("listing" => $listing));
                                } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }