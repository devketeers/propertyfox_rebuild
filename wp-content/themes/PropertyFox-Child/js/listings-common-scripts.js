/* Enabling support for new HTML5 tags for IE6, IE7 and IE8 */
if (navigator.appName === 'Microsoft Internet Explorer') {
    if ((navigator.userAgent.indexOf('MSIE 6.0') >= 0) || (navigator.userAgent.indexOf('MSIE 7.0') >= 0) || (navigator.userAgent.indexOf('MSIE 8.0') >= 0)) {
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('aside');
        document.createElement('footer');
        document.createElement('article');
        document.createElement('hgroup');
        document.createElement('figure');
        document.createElement('figcaption')
    }
}

(function ($) {
    jQuery(function () {
        // Begin input common focus and blur for value.
        var inputText = jQuery('input:text,input:password,textarea');
        inputText.focus(function () {
            if (this.value === this.defaultValue) {
                this.value = ''
            }
        });
        inputText.blur(function () {
            if (!this.value) {
                this.value = this.defaultValue;
            }
        });
        // Ends input common focus and blur for value.


        if (jQuery(".include-content, .listing-page, .listing-list-content").length) {
            jQuery("body").addClass("black-header");
        }

        if (jQuery(window).width() < 768) {
            jQuery('div.property-details p').click(function () {
                jQuery(this).parent().toggleClass('show');
                jQuery(this).parent().find('div.property-coll').slideToggle();
            })
        }

        if (jQuery(window).width() < 768) {
            jQuery(".listing-service-item h6").click(function () {
                jQuery(this).parent().toggleClass('show');
                jQuery(this).parent().find(".listing-item-wraper").slideToggle()
            })
        }

        //this function for scroll down
        jQuery("div.scroll-down").bind('click', function () {
            if (jQuery(window).width() < 767) {
                jQuery('body, html').stop(true, true).animate({
                    scrollTop: jQuery("#get-started-wrap").offset().top - 560
                }, 1500, "easeInOutCubic")
            }
            else {
                jQuery('body, html').stop(true, true).animate({
                    scrollTop: jQuery("#get-started-wrap").offset().top - 560
                }, 1500, "easeInOutCubic")
            }
        });

        if (jQuery("#customer-slider").length) {
            jQuery('.customer-slider').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '10px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 766,
                        settings: {
                            dots: true,
                            arrows: false,
                            centerMode: true,
                            centerPadding: '20px',
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }

        if (jQuery("#listing-slider").length) {
            jQuery('.customer-slider').slick({
                centerMode: true,
                centerPadding: '0px',
                slidesToShow: 3,
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '10px',
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 766,
                        settings: {
                            dots: true,
                            arrows: true,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }

                ]
            });
        }


        if (jQuery(window).width() < 768) {
            if (jQuery("#floating-brand-item-wrap").length) {
                jQuery('#floating-brand-item-wrap').slick({
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3,
                    responsive: [
                        {
                            breakpoint: 766,
                            settings: {
                                dots: true,
                                arrows: true,
                                //centerMode: true,
                                centerPadding: '0px',
                                slidesToShow: 3
                            }
                        }
                    ]
                });
            }
        }

        if (jQuery("div.property-price-info").length) {
            // Traditional Agent Commission slider function
            jQuery(function () {
                var progresSliderElement = jQuery("#progressRange");
                var progressRangehandle = jQuery("#progressRangehandle");
                progresSliderElement.slider({
                    range: "min",
                    min: 0,
                    max: 10,
                    value: 7,
                    create: function () {
                        progressRangehandle.text(progresSliderElement.slider("value") + "%");

                    },
                    slide: function (event, ui) {
                        progressRangehandle.text(ui.value + "%");
                    },
                    change: function () {
                        var propertyPrice = jQuery("#pricingRange").slider("value");
                        var tacPrcnt = jQuery("#progressRange").slider("value") / 100;
                        var taCommission = tacPrcnt * propertyPrice;
                        var pfcPrcnt = 1.5 / 100;
                        var pfCommision = pfcPrcnt * propertyPrice;
                        var savePrice = Math.floor(taCommission - pfCommision);
                        netPrice.text("R" + savePrice)
                    }
                });

                // Property price slider
                var sliderElement = jQuery("#pricingRange");
                var handle = jQuery("#custom-handle");
                var netPrice = jQuery("#netPrice");

                sliderElement.slider({
                    range: "max",
                    min: 2500000,
                    max: 10000000,
                    value: 1000000,
                    create: function () {
                        handle.text("R" + sliderElement.slider("value"));
                    },
                    slide: function (event, ui) {
                        jQuery("#minRange").text("R" + ui.value);
                        handle.text("R" + ui.value);
                    },
                    change: function () {
                        var propertyPrice = jQuery("#pricingRange").slider("value");
                        var tacPrcnt = jQuery("#progressRange").slider("value") / 100;
                        var taCommission = tacPrcnt * propertyPrice;
                        var pfcPrcnt = 1.5 / 100;
                        var pfCommision = pfcPrcnt * propertyPrice;
                        var savePrice = Math.floor(taCommission - pfCommision);
                        netPrice.text("R" + savePrice)
                    }
                });
                jQuery("#minRange").text("R" + sliderElement.slider("value"));

                var propertyPrice = jQuery("#pricingRange").slider("value");
                var tacPrcnt = jQuery("#progressRange").slider("value") / 100;
                var taCommission = tacPrcnt * propertyPrice;
                var pfcPrcnt = 1.5 / 100;
                var pfCommision = pfcPrcnt * propertyPrice;
                var savePrice = Math.floor(taCommission - pfCommision);
                netPrice.text("R" + savePrice)
            });
        }

        if (jQuery('.listing-list-content').length) {
            jQuery('body').addClass('listing-list-content-body');
        }

        //this function for selectric
        if (jQuery("select.styled-select").length) {
            jQuery('select.styled-select').selectric();
        }

        //this function for custom scroll bar
        if (jQuery('.input-field-wrap').length) {
            jQuery(".input-field-wrap").getNiceScroll().resize();
            jQuery(".input-field").niceScroll({
                grabcursorenabled: false,
            });

            if (jQuery(window).width() < 992) {
                jQuery(".property-type-item-wrap").niceScroll({
                    grabcursorenabled: false,
                });
            }
        }

        if (jQuery(window).width() < 768) {
            jQuery(".main-wrap").getNiceScroll().resize();
            jQuery(".navigation-wrap, .refine-search-wrap").niceScroll({
                grabcursorenabled: false,
            });
        }

        //this function for add class to input
        jQuery('.input-row input[type="text"], .input-row textarea').focus(function () {
            jQuery(this).parent().addClass('active');
        });

        jQuery('.input-row input[type="text"], .input-row textarea').blur(function () {
            jQuery(this).parent().removeClass('active');
        });

        jQuery('.input-row input').keyup(function () {
            jQuery(this).parent().addClass('value-adding');
        });

        jQuery('div.input-row input, .input-row textarea').blur(function () {
            if (!this.value) {
                this.value = this.defaultValue;
            }
            if (this.value === this.defaultValue) {
                jQuery(this).parent().removeClass('value-adding');
            }
        });

        //this function for tab
        jQuery('#tab ul li').eq(0).addClass('active');
        jQuery('.listing-service-item-wrap').hide();
        jQuery('.listing-service-item-wrap').eq(0).show();

        jQuery('#tab ul li').click(function (e) {
            e.preventDefault();
        });
        jQuery('#tab ul li').each(function (i) {
            jQuery(this).click(function () {
                if (jQuery(this).hasClass('active')) return false;
                else {
                    jQuery('#tab ul li').removeClass('active');
                    jQuery(this).addClass('active');
                    jQuery('.listing-service-item-wrap').hide();
                    jQuery('.listing-service-item-wrap').eq(i).show();
                }
            });
        });

        if (jQuery('section.home-content').length) {
            jQuery('body').addClass('home-content-body')
        }

        jQuery(window).on("scroll", function () {
            if (jQuery(window).width() > 767) {
                if (jQuery(window).scrollTop() > 10) {
                    jQuery("div.header-wrap").addClass("fixedTop")
                } else {
                    jQuery("div.header-wrap").removeClass("fixedTop")
                }
            }
        });

        if (jQuery(window).width() > 767) {
            jQuery("#nav-icon-wrap").click(function (e) {
                e.stopPropagation();
                jQuery("body").addClass("navOpened");
                jQuery(this).hide()
            });

            jQuery(".navigation-wrap .close-btn, body").click(function (e) {
                jQuery("body").removeClass("navOpened");
                jQuery("#nav-icon-wrap").show()
            });

            jQuery(".main-nav, .login-link-wrap").click(function (e) {
                e.stopPropagation()
            })
        }

        if (jQuery(window).width() < 768) {
            jQuery("#nav-icon-wrap .nav-icon.orange").click(function () {
                jQuery(".navigation-wrap").slideDown();
                jQuery("body").addClass("phoneNavopened")
            });

            jQuery("#nav-icon-wrap .close-btn.brown").click(function () {
                jQuery(".navigation-wrap").slideUp();
                jQuery("body").removeClass("phoneNavopened")
            })
        }

        if (jQuery(window).width() < 768) {
            jQuery(".footer-widget").find("ul.dropdown-menu").parent().addClass("subnav");
            jQuery(".footer-widget.subnav span").bind('click', 'touchend', function (e) {
                jQuery(".footer-widget.subnav span").parent().find("ul:visible").slideUp();
                jQuery(".footer-widget.subnav span").removeClass("active");
                if (jQuery(this).parent().find("ul:visible").length) {
                    jQuery(this).removeClass("active");
                    jQuery(this).parent().find("ul:visible").slideUp()
                } else {
                    jQuery(this).addClass("active");
                    jQuery(this).parent().find("ul").slideDown()
                }
            })
        }

        if (jQuery(".listing-page").length) {
            jQuery('body').addClass("listing-page-body")
        }


        if (jQuery(window).width() > 767) {
            jQuery(".map-wrap").height(jQuery(".property-wrap.list-property").outerHeight())
        }

        jQuery(".selectric-dropdown-style .label").click(function () {
            jQuery(this).parents(".form-inner .input-row").toggleClass("selectrick-opened")
        });

        jQuery(".selectric-dropdown-style .selectric-items li").click(function () {
            jQuery(".form-inner .input-row").removeClass("selectrick-opened")
        });

        /* property listing detail - read more/less */
        if (jQuery('.listing-title-wrap .listing-title-content').height() >= 200) {
            jQuery(".listing-title-wrap .expand-btn").click(function (e) {
                e.preventDefault();
                jQuery(this).parent().toggleClass("paragraph-shown");
            })
        } else {
            jQuery('.listing-title-wrap').addClass('paragraph-shown');
            jQuery('.listing-title-wrap .expand-btn').hide();
        }

        jQuery(".refine-search").click(function (e) {
            e.preventDefault();
            jQuery(".map-wrap").hide();
            jQuery("#refine-search-wrap").slideDown()
        });

        jQuery("#refine-search-wrap div.close-btn").click(function () {
            jQuery("#refine-search-wrap").slideUp()
        });

        // if selectric dropdown opens, close property type dropdown (desktop)
        jQuery('select.styled-select').on('selectric-open', function () {
            jQuery('.search-feature-wrap > ul > li:first-child').removeClass('active');
        });

        // if property type dropdown opens, close other open selectric dropdowns
        jQuery(".search-feature-wrap > ul > li:first-child").click(function (e) {
            e.stopPropagation();
            jQuery('select.styled-select').selectric('close');
        });

        if (jQuery(window).width() < 768) {
            jQuery(".listing-search-content .search-feature-wrap > ul > li:first-child").click(function (e) {
                e.stopPropagation();
                jQuery(this).find("ul").parent().slideToggle();
            });

            // if selectric dropdown opens, close property type dropdown (mobile)
            jQuery('select.styled-select').on('selectric-open', function () {
                jQuery('.search-feature-wrap .input-field-wrap').slideUp();
            });

            jQuery(".search-feature-wrap > ul > li:first-child .input-field-wrap").click(function (e) {
                e.stopPropagation()
            });

            jQuery("body").click(function () {
                jQuery(".listing-search-content .search-feature-wrap > ul > li > span").parent().find("ul").parent().hide()
            });
        }

        jQuery(".map-triger").click(function (e) {
            e.preventDefault();
            jQuery(".listing-list-content .list-menu-wrap").hide();
            jQuery(".map-wrap").slideToggle();
            jQuery(this).parent().toggleClass("mapviewed");
            initializeMap();
        });

        var $howWeSlide = jQuery('div.how-we-slider');

        $howWeSlide.each(function () {
            var itemTotal = jQuery(this).find('ul.slides > li').length;
            jQuery("div.how-we-slider-wrap").find('div.total-slide').html(itemTotal);
            jQuery("div.how-we-slider-wrap").find('div.active-slide-no').html(1);
        });

        var $modalWeSlide = jQuery('div.modal-slider-wrap');

        $modalWeSlide.each(function () {
            var itemTotal = jQuery(this).find('ul.slides > li').length;
            jQuery("div.modal-wrap").find('div.total-slide').html(itemTotal);
            jQuery("div.modal-wrap").find('div.active-slide-no').html(1);
        });

        jQuery(".carousel-item-inner").find("div.read-btn").parents(".carousel-item-inner").addClass("large-content");

        if (jQuery(".tol-tip-triger").length) {
            jQuery(document).tooltip({
                position: {
                    my: "center bottom-20",
                    at: "center top",
                    using: function (position, feedback) {
                        jQuery(this).css(position);
                        jQuery("<div>").addClass("arrow").addClass(feedback.vertical).addClass(feedback.horizontal).appendTo(this);
                    }
                }
            });
        }

        jQuery(".tool-tip-triger").each(function () {
            var self = jQuery(this);
            self.mouseenter(function () {
                self.find(".tooltip-info").stop(true, true).fadeIn()
            });

            self.mouseleave(function () {
                self.find(".tooltip-info").stop(true, true).delay(200).fadeOut()
            })
        });

        jQuery(".hero-link-wrap a.heart-icon, .heart-icon").click(function (e) {
            e.preventDefault();
            jQuery(this).toggleClass("active")
        });

        jQuery(".search-feature-wrap > ul > li").click(function (e) {
            e.stopPropagation();
            if (jQuery(this).hasClass("active")) return true;
            else {
                jQuery(".search-feature-wrap > ul > li").removeClass("active");
                jQuery(this).addClass("active");
            }
        });

        jQuery("body").click(function () {
            jQuery(".search-feature-wrap > ul > li").removeClass("active")
        });

        jQuery(".bottom-fixed-btn a").find("span").parent().addClass("withIcon");

        if (jQuery(window).width() < 768) {
            jQuery(".property-type-wrap ul li").click(function () {
                jQuery(this).find(".input-field-wrap").slideToggle()
            })
        }

        if (jQuery(window).width() < 992) {
            jQuery(".property-triger").click(function () {
                jQuery(this).parent().find(".input-field-wrap").slideToggle()
            })
        }


    });// End ready function.

    jQuery(window).load(function () {

        if (jQuery('div.how-we-slider').length === 0) return false;

        var $howSlide = jQuery('div.how-we-slider');

        $howSlide.flexslider({
            animation: "slide",
            slideshow: false,
            directionNav: false,
            controlNav: true,
            slideshowSpeed: 5000,
            animationSpeed: 600,
            manualControls: "#sliderControler ol li",
            useCSS: false,
            animationLoop: true,
            pauseOnAction: false,
            after: function (slider) {
                jQuery('div.how-we-slider').each(function () {
                    jQuery("div.how-we-slider-wrap").find('div.active-slide-no').html(jQuery(this).find('ul.slides > li.flex-active-slide div.slide-no').html());
                })
            }
        });

        jQuery('#Next, a.next-slider-triger').click(function (e) {
            e.preventDefault();
            $howSlide.flexslider("next");
        });

        jQuery('#Previous').click(function (e) {
            e.preventDefault();
            $howSlide.flexslider("prev");
        });
    });

    jQuery(window).load(function () {
        if (jQuery('div.modal-slider-wrap').length === 0) {
            return false;
        }
        // why can't we use 'on' ...can't figure out why it doesn't work?
        // Answer: Because on load will only register one instance of the click. See below this how its supposed to be done.
        jQuery(".gallery").click(function (e) {
            doGallery(this, "images");
            e.preventDefault();
        });
    });
})(jQuery);

jQuery(document).ready(function () {
    jQuery(document).on("click", ".gallery", function (e) {
        doGallery(this, "images");
        e.preventDefault();
    });

    jQuery(document).on('click', '.galleryClose', function (e) {
        jQuery("#main-header").show();
        jQuery("body").css({overflow: 'inherit'});
        jQuery.unblockUI();
        e.preventDefault();
    });

    jQuery(document).on('click', '.floorplan', function (e) {
        doGallery(this, "floorplans");
        e.preventDefault();
    });

    jQuery(document).on('click', '.video', function (e) {
        doGallery(this, "videos");
        e.preventDefault();
    });

    jQuery(document).on('click', '.showOnMap', function (e) {
        var data = jQuery(this).data();
        showInfo(data.id, data.base);
        e.preventDefault();
    });

    jQuery(document).on('click', '.changeMap', function (e) {
        changeMap('poi');
        e.preventDefault();
    });

    jQuery(document).on('click', '.streetMap', function (e) {
        changeMap('street');
        e.preventDefault();
    });

    jQuery(document).on('click', '.streetView', function (e) {
        changeMap('streetview');
        e.preventDefault();
    });

    /*@todo - trigger close when clicked
        jQuery(document).on('click', '.blackoverlay', function (e) {
        });
    */

});

function doGallery(cnt, type) {
    jQuery("#main-header").hide();

    jQuery("body").css({overflow: 'hidden'});

    var $th = jQuery(jQuery(cnt).closest(".property-figure-wrap"));

    if ($th.length === 0) {
        $th = jQuery("#galleryatts");
    }

    var price = $th.attr('data-price'),
        title = $th.attr('data-title'),
        media = "";

    if (type === "images") {
        media = $th.attr('data-allimg').split(";");
    } else if (type === "floorplans") {
        media = $th.attr('data-allFP').split(";");
    } else if (type === "videos") {
        media = $th.attr('data-allVid').split(";");
    }

    price = (formatMoney(price, 0, ".", " ") === 'POA' ? 'POA' : 'R ' + formatMoney(price, 0, ".", " "));
    jQuery("#modTitle h5").html(price);
    jQuery("#modTitle p").html(title);
    jQuery(".galleryslide").remove();

    jQuery(media).each(function (index) {
        if (type === "videos") {
            var mContent = '<iframe width="1120" height="630" src="' + this + '?rel=0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';
        } else {
            mContent = '<img src="' + this + '" alt="">';
        }
        jQuery(".galleryslides").append('<li class="slide galleryslide"><div class="slide-thumb">' + mContent + '</div><div class="slide-no">' + (index + 1) + '</div></li>');
    });

    jQuery("#modSlides").html(media.length);
    jQuery("#modCurrentSlide").html("1");

    jQuery('div.modal-slider-wrap').removeData("flexslider");

    jQuery('div.modal-slider-wrap').flexslider({
        //animation:"slide",
        slideshow: false,
        directionNav: true,
        controlNav: false,
        slideshowSpeed: 5000,  //Integer: Set the speed of the slideshow cycling, in milliseconds
        animationSpeed: 600,
        useCSS: false,
        animationLoop: true,
        pauseOnAction: false, // default setting
        after: function (slider) {
            jQuery('div.modal-slider-wrap').each(function () {
                jQuery("div.modal-wrap").find('div.active-slide-no').html(jQuery(this).find('ul.slides > li.flex-active-slide div.slide-no').html())
            })
        }
    });

    jQuery.blockUI({
        message: jQuery('#photoGallery'),
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: 1,
            color: '#fff',
            top: '0px',
            width: '100%',
            left: '0px',
            cursor: 'default'
        }
    });
}

function formatMoney(n, c, d, t) {
    if (n === '999999999') {
        return "POA";
    }

    var c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d === undefined ? "." : d,
        t = t === undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

jQuery(document).ready(function () {
    jQuery('input[id="autocomplete"]').autocomplete({
        minLength: 2,
        source: function (request, response) {
            var data = jQuery(this).data();
            data.action = 'fox_ajax';
            data.task = 'search';
            data.search = request.term;
            jQuery.ajax({
                url: csi_globals.ajax_url,
                data: data,
                dataType: "json",
                type: "POST",
                success: function (data) {
                    response(jQuery.map(data, function (item) {
                        return {
                            label: item.title,
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            jQuery(this).val(ui.item.title);
            setTimeout(function (event) {
                update_search_results(event)
            }, 500);
        }
    });

    jQuery('.autocomplete').autocomplete({
        minLength: 2,
        source: function (request, response) {
            var data = jQuery(this).data();
            data.action = 'fox_ajax';
            data.task = 'search';
            data.search = request.term;
            jQuery.ajax({
                url: csi_globals.ajax_url,
                data: data,
                dataType: "json",
                type: "POST",
                success: function (data) {
                    response(jQuery.map(data, function (item) {
                        return {
                            label: item.title,
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            jQuery(this).val(ui.item.title);
            setTimeout(function (event) {
                update_search_results(event)
            }, 500);
        }
    });


    /**
     * Auto suggest
     */
    if (jQuery("#input_1_8 option:selected").val()) {
        jQuery("#input_1_7").prop('disabled', false).attr("placeholder", "Please enter your Suburb");
    } else {
        jQuery("#input_1_7").prop('disabled', true).attr("placeholder", "Please select Province first");
    }

    jQuery(document).on('change', '#input_1_8', function (e) {
        var OptValue = e.target.value;
        jQuery("#input_1_7").val('');
        if (OptValue || jQuery("#input_1_8 option:selected").val()) {
            jQuery("#input_1_7").prop('disabled', false).attr("placeholder", "Please enter your Suburb");
        } else {
            jQuery("#input_1_7").prop('disabled', true).attr("placeholder", "Please select Province first");
        }
    });

    //Get started ajax area select
    jQuery('input[id="input_1_7"]').autocomplete({
        minLength: 2,
        source: function (request, response) {
            var data = jQuery(this).data();
            data.action = 'fox_ajax';
            data.task = 'suburbs';
            data.province = jQuery("#input_1_8 option:selected").val();
            data.search = request.term;
            jQuery.ajax({
                url: csi_globals.ajax_url,
                data: data,
                dataType: "json",
                type: "POST",
                success: function (data) {
                    response(jQuery.map(data, function (item) {
                        return {
                            label: item.title,
                        }
                    }))
                }
            });
        },
        select: function (event, ui) {
            jQuery("#input_1_7").val(ui.item.title);
        }
    });
});