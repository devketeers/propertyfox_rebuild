var gMap,
    infowindows = {},
    property_markers = {},
    last_info_window = false,
    results_loading = false,
    load_more = false,
    search_request = null;

var qs = (function (a) {
    if (a === "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length === 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

function get_property_details(search_query, url) {
    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: search_query + '&action=ajax_property_details',
        success: function (response) {
            //console.log(response);
            window.location.href = url;
        },
        error: function (errorThrown) {
            alert(errorThrown);
        }
    });
    return false;
}

function initMap($map_obj, map_args) {

    map_args = map_args || {};
    map_args = jQuery.extend(true, {
        scrollwheel: false,
        mapTypeControl: false,
        zoom: 10,
        maxZoom: 16,
        gestureHandling: "greedy"
    }, map_args);

    return new google.maps.Map($map_obj.get(0), map_args);
}

function init_markers() {
    var marker_ids = [],
        markers_to_delete,
        bounds = new google.maps.LatLngBounds();

    if (null === gMap) {
        return;
    }

    jQuery('.property-figure-wrap').each(function () {
        var $th = jQuery(this),
            id = $th.attr('data-id'),
            lat = $th.attr('data-lat'),
            lng = $th.attr('data-lng'),
            lnk = $th.attr('data-url'),
            price = $th.attr('data-price'),
            img = $th.attr('data-thumbnail'),
            base = $th.attr('data-basedir'),
            title = $th.attr('data-title'),
            beds = $th.attr('data-beds'),
            baths = $th.attr('data-baths'),
            park = $th.attr('data-parking'),
            erf = $th.attr('data-erf'),
            cnt = $th.attr('data-imgCount'),
            imgs = $th.attr('data-allImg'),
            status = $th.attr('data-status');

        infowindows[id] = new google.maps.InfoWindow({
            content: '<div class="property-figure-wrap" ' + 'data-price="' + price + '" ' + 'data-title="' + title + '" ' + 'data-allImg="' + imgs + '" ' + '>' +
                '<figure>' +
                '<a href="' + lnk + '"><img src="' + img + '" alt=" + title + "></a>' +
                '</figure>' +
                '<div class="property-figure-icon">' +
                '<div class="new">' + status + '</div>' +
                '</div>' +
                '<div class="propety-figure-bottom">' +
                '<div class="propety-figure-bottom-inner">' +
                '<a href="#photoGallery" class="propety-bottom-icon gallery">' +
                '<img src="' + base + '/svgs/svg-property-photos-white.svg" alt="Photo Gallery">' +
                '<span class="num">' + cnt + '</span>' +
                '</a>' +
                '</div>' +
                '</div>' +
                '</div>' +

                '<div class="property-content-wrap">' +
                '<h5 class="desktop">' + (formatMoney(price, 0, ".", " ") === 'POA' ? 'POA' : 'R ' + formatMoney(price, 0, ".", " ")) + '</h5><h5 class="mobi">' + (formatMoney(price, 0, ".", " ") === 'POA' ? 'POA' : 'R ' + formatMoney(price, 0, ".", " ")) + '</h5>' +
                '<dfn class="desktop">' + title + '</dfn><dfn class="mobi">' + title + '</dfn>' +
                '<div class="property-icon-wrap">' +
                '<div class="property-icon-item">' +
                '<div class="property-single-icon">' +
                '<figure>' +
                '<img src="' + base + '/svgs/svg-property-beds-grey.svg" alt="Beds">' +
                '</figure>' +
                '<span>' + beds + '</span>' +
                '</div>' +
                '<div class="property-single-icon">' +
                '<figure>' +
                '<img src="' + base + '/svgs/svg-property-baths-grey.svg" alt="Baths">' +
                '</figure>' +
                '<span>' + baths + '</span>' +
                '</div>' +
                '<div class="property-single-icon">' +
                '<figure>' +
                '<img src="' + base + '/svgs/svg-property-garage-grey.svg" alt="Garage">' +
                '</figure>' +
                '<span>' + park + '</span>' +
                '</div>' +
                '</div>' +
                '<p>Erf size: <span>' + erf + 'm²</span></p>' +
                '</div>' +
                '</div>',
            maxWidth: 263, // 315 - 51 = 264
            pixelOffset: new google.maps.Size(-17, 0)
        });

        var $corresponding_item = $th;

        if (undefined !== lat && undefined !== lng) {
            lat = parseFloat(lat);
            lng = parseFloat(lng);

            if (!isNaN(lat) && !isNaN(lng)) {
                if (undefined === property_markers[id]) {
                    var p = nFormatter(price.replace(/,/g, ""), 1);

                    property_markers[id] = new google.maps.Marker({
                        map: gMap,
                        position: {
                            lat: lat,
                            lng: lng
                        },
                        icon: {
                            url: base + "/svgs/svg-property-listing-default.png",
                            size: new google.maps.Size(50, 75),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(25, 62),
                            scaledSize: new google.maps.Size(25, 37.5)
                        }
                    });

                    google.maps.event.addListener(infowindows[id], 'closeclick', function () {
                        resetMarkers(base);
                    });

                    google.maps.event.addListener(infowindows[id], 'domready', function () {
                        var iwOuter = jQuery('.gm-style-iw');
                        var iwBackground = iwOuter.prev();

                        // add custom class for targeted styling
                        iwOuter.addClass('custom-iw');

                        // arrow
                        iwBackground.children(':nth-child(3)').addClass('custom-iw-arrow');

                        // border radius
                        iwBackground.children(':nth-child(4)').addClass('custom-iw-bg');

                    });

                    google.maps.event.addListener(property_markers[id], 'click', function () {
                        if (last_info_window) {
                            last_info_window.close();
                        }

                        resetMarkers(base);

                        var icon = {
                            url: base + "/svgs/svg-property-listing-active.png",
                            size: new google.maps.Size(68, 96),
                            origin: new google.maps.Point(0, 0),
                            anchor: new google.maps.Point(30, 72),
                            scaledSize: new google.maps.Size(34, 48)
                        };

                        property_markers[id].setIcon(icon);

                        jQuery('#searchresults > li .marker-hovered').removeClass('marker-hovered');
                        last_info_window = infowindows[id];
                        infowindows[id].open(gMap, property_markers[id]);
                        $corresponding_item.addClass('marker-hovered');
                        jQuery(".property-seacrh-wrap").scrollTo($corresponding_item, 300);
                    });

                    google.maps.event.addListener(property_markers[id], 'mouseover', function () {
                        if (false === last_info_window || (last_info_window && !last_info_window.getMap())) {
                            jQuery('#property-item-wrap > div .marker-hovered').removeClass('marker-hovered');
                            $corresponding_item.addClass('marker-hovered');
                        }
                    });

                    google.maps.event.addListener(property_markers[id], 'mouseout', function () {
                        if (last_info_window && !last_info_window.getMap()) {
                            jQuery('.marker-hovered').removeClass('marker-hovered');
                        }
                    });
                }

                marker_ids.push(id);
                bounds.extend(property_markers[id].getPosition());
            }
        }
    });

    markers_to_delete = _.difference(Object.keys(property_markers), marker_ids);
    for (var i = 0; i < markers_to_delete.length; i++) {
        property_markers[markers_to_delete[i]].setMap(null);
        delete property_markers[markers_to_delete[i]];
    }

    if (marker_ids.length) {
        gMap.setCenter(bounds.getCenter());
        gMap.fitBounds(bounds);
    }
}

function resetMarkers(base) {
    jQuery.each(property_markers, function (index, marker) {
        var icon = {
            url: base + "/svgs/svg-property-listing-default.png",
            size: new google.maps.Size(50, 75),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(25, 62),
            scaledSize: new google.maps.Size(25, 37.5)
        };

        marker.setIcon(icon);
    });
}

function update_search_results(e) {
    if (e != null && (e === "paged" || jQuery(e.target).is('.pager'))) {
        var more_results_to_load = true;
    } else {
        jQuery('#currentpage').val('1');
        jQuery(".scroll-pane").scrollTop(0);
        more_results_to_load = true;
        jQuery('.results-title').html('');
    }

    if (!more_results_to_load) {
        return;
    }

    var $search_form = jQuery("#frmSearch");

    jQuery.each(infowindows, function (index, iw) {
        iw.close();
    });

    var data = $search_form.find(":input[value]").serialize() + '&action=listing_search',
        page = parseInt(jQuery('#currentpage').val(), 10),
        load_more = false;

    //console.log(data);

    data = stripUrlParams("?" + data).substr(1);

    //console.log(data);

    if (null !== search_request) {
        search_request.abort();
    }

    $search_form.data('data', data);

    results_loading = true;
    jQuery(".property-item-wrap").addClass('busy');

    search_request = jQuery.get(ajaxurl, data, function (res) {
        jQuery(".property-item-wrap").removeClass('busy');
        results_loading = false;
        if (res.success) {

            jQuery('.listing-counts').html("Showing " + res.data.showingmin + " - " + res.data.showing + " <em>of " + res.data.total_listings + " Results</em>");
            jQuery('.listing-counts-mobile').html(res.data.total_listings + " Results");
            setupPager(page, parseInt(res.data.total_listings));

            if (load_more) {
                jQuery('.property-item-wrap').append(res.data.html);
            } else {
                jQuery('.property-item-wrap').html(res.data.html);
            }

            //Update the query string
            if (history.pushState) {
                data = stripUrlParams("?" + data, "action,ttlitems");
                window.history.pushState('', '', '/listing-search' + data);
            }

            more_results_to_load = !!res.data.has_more;
        }

        init_markers();
    }, 'json');
}

function stripUrlParams(url, parameter) {
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var stuff = urlparts[1];
        var ignore = [];

        if (parameter)
            ignore = parameter.split(",");

        pars = stuff.split("&");
        var comps = {};
        var append = "";
        for (i = pars.length - 1; i >= 0; i--) {
            spl = pars[i].split("=");

            if (spl[0].endsWith("%5B%5D") || spl[0].endsWith("[]") && comps.indexOf[spl[0]] !== -1) {
                if (append.indexOf(spl[0] + "=" + spl[1]) === -1)
                    append += "&" + spl[0] + "=" + spl[1];
            }
            else {
                comps[spl[0]] = spl[1];
            }
        }
        pars = [];

        for (var a in comps) {
            if (ignore.indexOf(a) === -1 && comps[a] !== "" && comps[a] !== "undefined" && comps[a] !== "Enter+City%2C+Suburb+or+Web+Ref")
                pars.push(a + "=" + comps[a]);
        }
        url = urlparts[0] + '?' + pars.join('&');
        return url + append;
    } else {
        return url;
    }
}

function showInfo(id, base) {
    if (last_info_window) {
        last_info_window.close();
    }

    resetMarkers(base);

    var icon = {
        url: base + "/svgs/svg-property-listing-active.png",
        size: new google.maps.Size(68, 96),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(30, 72),
        scaledSize: new google.maps.Size(34, 48)
    };

    property_markers[id].setIcon(icon);

    last_info_window = infowindows[id];
    infowindows[id].open(gMap, property_markers[id]);
}

function nFormatter(num, digits) {
    var si = [
        {value: 1E18, symbol: "E"},
        {value: 1E15, symbol: "P"},
        {value: 1E12, symbol: "T"},
        {value: 1E9, symbol: "G"},
        {value: 1E6, symbol: "M"},
        {value: 1E3, symbol: "k"}
    ], rx = /\.0+$|(\.[0-9]*[1-9])0+$/, i;
    for (i = 0; i < si.length; i++) {
        if (num >= si[i].value) {
            return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
        }
    }

    if (num === "") {
        return num;
    }

    return num.toFixed(digits).replace(rx, "$1");
}

function changePage(event, pg) {
    jQuery("#currentpage").val(pg);
    jQuery('html, body').animate({scrollTop: 0}, 'slow');
    update_search_results(event);
}

function setupPager(page = false, ttl = false) {
    var start = 1,
        end = 1,
        tooMany = false;

    if (ttl === false)
        ttl = parseInt(jQuery("#ttlitems").val());

    jQuery("#pager").children().slice(1).remove();

    if (!page) {
        page = parseInt(jQuery("#currentpage").val());
    }

    jQuery("#pagerPrev").unbind("click");
    jQuery("#pagerNext").unbind("click");

    if (page === 1)
        jQuery("#pagerPrev").addClass("disabled");
    else {
        jQuery("#pagerPrev").removeClass("disabled");
        jQuery("#pagerPrev").click(function (event) {
            changePage("paged", page - 1);
        });
    }

    var numPages = Math.ceil(ttl / 10);

    if (numPages > 5) {
        start = page;
        end = page + 5;
        tooMany = true;
    }
    else {
        end = numPages;
    }

    if (end > numPages) {
        end = numPages;
        start = end - 6;
        tooMany = false;
    }

    for (var i = start; i <= end; i++) {
        var cls = "";

        if (i === page) {
            cls = " class='active'";
        }

        jQuery("#pager").append("<a href='javascript:changePage(\"paged\"," + i + ");'" + cls + ">" + i + "</a>");
    }

    if (tooMany) {
        jQuery("#pager").append('<a href="javascript:void(0);">...</a>');
        jQuery("#pager").append("<a href='javascript:changePage(\"paged\"," + numPages + ");'>" + numPages + "</a>");
    }

    var disabled = "onclick='changePage(\"paged\"," + (page + 1) + ");'";

    if (page === numPages) {
        disabled = " class='disabled'";
    }

    if (ttl > 0) {
        jQuery("#pager").append('<span id="pagerNext"' + disabled + '>Next</span>');
    }
}

jQuery.fn.scrollTo = function (elem, speed) {
    jQuery("html,body").animate({
        scrollTop: jQuery(this).scrollTop() - jQuery(this).offset().top + elem.offset().top
    }, speed === undefined ? 1000 : speed);
    return this;
};

function initializeMap() {
    gMap = initMap(jQuery("#divMap"));

    infowindows = {};
    property_markers = {};

    init_markers();
}

jQuery(window).ready(function () {
    if (jQuery("#divMap").length) {
        initializeMap();
    }

    jQuery(document).on("click", ".mapGallery", function () {
        doGallery(this);
    });

    jQuery(".search-filter").change(function (event) {
        update_search_results(event);
    });

    jQuery(document).on('click', '.searchButton', function (event) {
        event.preventDefault();
        update_search_results(event);
        jQuery("#refine-search-wrap").slideUp();
    });

    jQuery("#orderby").change(function (event) {
        update_search_results(event);
    });

    jQuery(".parking-filter li").click(function (event) {
        jQuery(".parking-filter li").removeClass("active");
        jQuery(this).addClass("active");
        jQuery("#parking").val(jQuery(this).attr("data-val"));
        update_search_results(event);
    });

    jQuery(".maxp-filter li").click(function (event) {
        jQuery(".maxp-filter li").removeClass("active");
        jQuery(this).addClass("active");
        jQuery("#maxprice").val(jQuery(this).attr("data-val"));
        update_search_results(event);
    });

    jQuery(".minp-filter li").click(function (event) {
        jQuery(".minp-filter li").removeClass("active");
        jQuery(this).addClass("active");
        jQuery("#minprice").val(jQuery(this).attr("data-val"));
        update_search_results(event);
    });

    jQuery(".beds-filter li").click(function (event) {
        jQuery(".beds-filter li").removeClass("active");
        jQuery(this).addClass("active");
        jQuery("#beds").val(jQuery(this).attr("data-val"));
        update_search_results(event);
    });

    jQuery(".baths-filter li").click(function (event) {
        jQuery(".baths-filter li").removeClass("active");
        jQuery(this).addClass("active");
        jQuery("#baths").val(jQuery(this).attr("data-val"));
        update_search_results(event);
    });

    jQuery(".mobile-beds").click(function (event) {
        jQuery(".mobile-beds").removeAttr("checked");
        jQuery(this).find(":input").attr("checked", "checked");
    });

    jQuery(".mobile-baths").click(function (event) {
        jQuery(".mobile-baths").removeAttr("checked");
        jQuery(this).find(":input").attr("checked", "checked");
    });

    jQuery(".mobile-parking").click(function (event) {
        jQuery(".mobile-parking").removeAttr("checked");
        jQuery(this).find(":input").attr("checked", "checked");
    });

    jQuery("#removeTriger").click(function (event) {
        jQuery(".mobileKeywords").val("Enter City, Suburb or Web Ref");
        jQuery(".mobileSearchDD").val("").selectric("refresh");
        jQuery(".mobileSearchCK").removeAttr("checked");
        jQuery(".mobile-beds").removeAttr("checked");
        jQuery(".mobile-parking").removeAttr("checked");
        jQuery(".mobile-baths").removeAttr("checked");
        jQuery(".mobile-sold").removeAttr("checked");
        jQuery(jQuery(".mobile-beds")[0]).find(":input").attr("checked", "checked");
        jQuery(jQuery(".mobile-baths")[0]).find(":input").attr("checked", "checked");
        jQuery(jQuery(".mobile-parking")[0]).find(":input").attr("checked", "checked");
        update_search_results(event);
    });

    jQuery("#include").click(function (event) {
        update_search_results(event);
    });

    setupPager();
});
