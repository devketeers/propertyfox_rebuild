var gMap,
    panorama,
    infoWindows={},
    last_info_window = false,
    schools="",
    shopping="",
    worship="",
    base,
    sourceLocation,
    other="";

function initMap( $map_obj, map_args ) {
    var map;

    map_args = map_args || {};
    map_args = jQuery.extend(true, {
        scrollwheel: false,
        mapTypeControl: false,
        zoom: 17,
        maxZoom: 50,
        mapTypeId: "roadmap"
    }, map_args);
   
    var map = new google.maps.Map( $map_obj.get(0), map_args);

    return map;
}

function init_marker() {
    var bounds = new google.maps.LatLngBounds();

    if ( null === gMap ) {
        return;
    }

        var $th = jQuery("#mapatts"),
        lat = $th.attr('data-lat'),
        lng = $th.attr('data-lng');
        base = $th.attr('data-base');
        
        if ( undefined !== lat && undefined !== lng ) {
            lat = parseFloat( lat );
            lng = parseFloat( lng );

            if ( ! isNaN( lat ) && ! isNaN( lng ) ) {
                sourceLocation=new google.maps.LatLng(lat,lng);
                var prop={
                    lat: lat,
                    lng: lng
                };

                var marker = new google.maps.Marker({
                    icon: {
                        url: base + "/svgs/svg-property-listing-active.png",
                        size: new google.maps.Size(50,75),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(25, 62),
                        scaledSize: new google.maps.Size(25,37.5)
                    },
                    map: gMap,
                    position: prop,
                });

                bounds.extend( marker.getPosition() );   

                panorama = gMap.getStreetView();
                panorama.setPosition(prop);
                panorama.setPov(/** @type {google.maps.StreetViewPov} */({
                  heading: 265,
                  pitch: 0
                }));
            }
        }
 
    gMap.setCenter( bounds.getCenter() );
    
    var service = new google.maps.places.PlacesService(gMap);
    service.nearbySearch({
      location: prop,
      //bounds: bounds,
      rankBy:google.maps.places.RankBy.DISTANCE,
      type: ["school","university","grocery_or_supermarket","shopping_mall","church","hindu_temple","mosque","place_of_worship","synagogue","airport","city_hall","courthouse","hospital","library","park","police","post_office","stadium"]
    }, renderPlaces);

    //gMap.fitBounds( bounds );
}
function renderPlaces(results,status){
    if (status === google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
          createMarker(results[i],i);
        }

        if(schools.length>0)
            jQuery(".poi-education > .listing-item-wraper").append(schools);
        else
            jQuery(".poi-education").remove();

        if(worship.length>0)
            jQuery(".poi-worship > .listing-item-wraper").append(worship);
        else
            jQuery(".poi-worship").remove();

        if(shopping.length>0)
            jQuery(".poi-shopping > .listing-item-wraper").append(shopping);
        else
            jQuery(".poi-shopping").remove();

        if(other.length>0)
            jQuery(".poi-other > .listing-item-wraper").append(other);
        else
            jQuery(".poi-other").remove();


         showHidePOIs();

      }
      
}


// show all points of interest
function showHidePOIs(){
   if (jQuery(window).width() >= 768) {
      
      var $togglePOI = jQuery('#showPOI'),
          $listingServiceWrap = jQuery('.listing-service-wrap');

      // hide > 4 POI's on load
      $listingServiceWrap.find('.listing-service-item').each(function () {
         var $listPOI = jQuery(this);
         $listPOI.find('.listing-item-wraper > .service-single-item:gt(3)').hide();
      });      

      // on click, show all and hide btn
      $togglePOI.click(function(e){
         e.preventDefault();
         $listingServiceWrap
            .addClass('listing-service-wrap--expanded')
            .find('.listing-service-item .service-single-item').slideDown();
         $togglePOI.parent('.link-btn').addClass('link-btn--hidden');
      });

   }
}

function createMarker(place,id) {
    var placeLoc = place.geometry.location;
    var placeLatLng=new google.maps.LatLng(placeLoc.lat(),placeLoc.lng());
    var icon="";
    var container;

    if(jQuery.inArray("school",place.types)>0 || jQuery.inArray("university",place.types)>0){
        icon="/svgs/svg-interests-school.svg";
    }
    else if(jQuery.inArray("mosque",place.types)>0){
        icon="/svgs/svg-interests-mosk.svg";
    }
    else if(jQuery.inArray("church",place.types)>0 || jQuery.inArray("synagogue",place.types)>0 || 
            jQuery.inArray("hindu_temple",place.types)>0 || jQuery.inArray("place_of_worship",place.types)>0){
        icon="/svgs/svg-interests-church.svg";
    }
    else if(jQuery.inArray("grocery_or_supermarket",place.types)>0 || jQuery.inArray("shopping_mall",place.types)>0){
        icon="/svgs/svg-interests-shop.svg";
    }
    else{
        icon="/svgs/svg-interests-shop.svg";
    }

    var html='<div class="service-single-item">' +
    '<div class="service-icon">' +
    '<img src="' + base + icon + '" alt="">' +
    '</div><p><span>' + place.name + '</span><dfn>' + calcDistance(sourceLocation,placeLatLng) + 'km</dfn></p></div>';
    
    if(jQuery.inArray("school",place.types)>0 || jQuery.inArray("university",place.types)>0){
        schools+=html;
    }
    else if(jQuery.inArray("mosque",place.types)>0 || jQuery.inArray("church",place.types)>0 || jQuery.inArray("synagogue",place.types)>0 || 
            jQuery.inArray("hindu_temple",place.types)>0 || jQuery.inArray("place_of_worship",place.types)>0){
        worship += html;
    }
    else if(jQuery.inArray("grocery_or_supermarket",place.types)>0 || jQuery.inArray("shopping_mall",place.types)>0){
        shopping+=html;
    }
    else{
        other+=html;
    }

    var marker = new google.maps.Marker({
      map: gMap,
      position: place.geometry.location,
      icon: base + icon
    });

        infoWindows[id] = new google.maps.InfoWindow({
        content:place.name
    });

    google.maps.event.addListener(marker, 'click', function() {
        if (last_info_window) {
            last_info_window.close();
        }
        last_info_window = infoWindows[id];
        infoWindows[id].open( gMap, marker );
    });

    google.maps.event.addListener(infoWindows[id], 'domready', function () {
        var iwOuter = jQuery('.gm-style-iw');
        var iwBackground = iwOuter.prev();

        // add custom class for targeted styling
        iwOuter.addClass('poi-iw');

        // arrow
        iwBackground.children(':nth-child(3)').addClass('poi-iw-arrow');
       
        // border radius
        iwBackground.children(':nth-child(2)').addClass('poi-iw-bg-shadow');                  
        iwBackground.children(':nth-child(4)').addClass('poi-iw-bg');

   });
  }
function calcDistance(p1, p2) {
    return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
}
function changeMap(t){
    jQuery("li.active").removeClass("active");
    
    jQuery("#toggle" + t).addClass("active");

    panorama.setVisible(false);

    if(t==='streetview')
    {
        gMap.setMapTypeId("satellite");
        panorama.setVisible(true);
    }
    else if(t=='street')
        gMap.setMapTypeId("satellite");
    else if(t==='poi')
    {
        gMap.setMapTypeId("roadmap");
    }
}
jQuery(window).ready(function(){
    if(jQuery("#divMap").length)
    {
        gMap=initMap(jQuery("#divMap"));

        init_marker();
    }
});