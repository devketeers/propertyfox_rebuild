<?php
    function csi_scripts() {
        /**
         * CSS
         */
        wp_register_style("fonts", "https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700", false, '1.0.9', 'all');
        wp_enqueue_style('fonts');

        wp_register_style('fox-style-css', get_stylesheet_directory_uri() . '/css/style.css', false, '1.0.9', 'all');
        wp_enqueue_style('fox-style-css');

        wp_register_style("selectric", get_stylesheet_directory_uri() . "/css/selectric.css", false, '1.0.9', 'all');
        wp_enqueue_style('selectric');

        /**
         * Scripts
         */
        wp_deregister_script('jquery');
        wp_register_script('jquery', includes_url('/js/jquery/jquery.js'), false, NULL, true);
        wp_enqueue_script('jquery');

        wp_register_script('fox-default-js', get_stylesheet_directory_uri() . '/js/listings-common-scripts.js', array('jquery-ui-autocomplete', 'jquery'), '1.0.9', true);
        wp_enqueue_script('fox-default-js');
        wp_localize_script('fox-default-js', 'csi_globals', array('ajax_url' => admin_url('admin-ajax.php')));

        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-autocomplete');
        wp_enqueue_script('jquery-ui-slider');

        wp_register_script("selectric-js", get_stylesheet_directory_uri() . "/js/jquery.selectric.js", false, '1.0.9', true);
        wp_enqueue_script('selectric-js');

        wp_register_script("nice-scroll-js", get_stylesheet_directory_uri() . "/js/jquery.nicescroll.min.js", false, '1.0.9', true);
        wp_enqueue_script('nice-scroll-js');

        wp_register_script("block-ui-js", get_stylesheet_directory_uri() . "/js/jquery.blockui.js", false, '1.0.9', true);
        wp_enqueue_script('block-ui-js');

        wp_register_script("underscore-js", get_stylesheet_directory_uri() . "/js/underscore-min.js", false, '1.0.9', true);
        wp_enqueue_script('underscore-js');

        wp_register_script("touch-punch-js", get_stylesheet_directory_uri() . "/js/jquery.ui.touch-punch.min.js", false, '1.0.9', true);
        wp_enqueue_script('touch-punch-js');

        wp_register_script("gmap-js", "https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyBiMpt6RwQSW836mbqQ4QX23UH2NdbJz4o", false, '1.0.9', true);
        wp_enqueue_script('gmap-js');
    }

    add_action('wp_enqueue_scripts', 'csi_scripts');