<?php
    /**
     * Allow SVGs
     *
     * @param $mimes
     * @return mixed
     */
    function cc_mime_types($mimes) {
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }

    add_filter('upload_mimes', 'cc_mime_types');

    //Remove anchor
    add_filter('gform_confirmation_anchor', '__return_false');

    /**
     * Add the Divi Builder to Custom Post Types
     *
     * @param $post_types
     * @return array
     */
    function my_et_builder_post_types($post_types) {
        $post_types[] = 'neighbourhood';
        $post_types[] = 'ANOTHER_CPT_HERE';

        return $post_types;
    }

    add_filter('et_builder_post_types', 'my_et_builder_post_types');

    /**
     * Display child pages
     *
     * @return string
     */
    function wpb_list_child_pages() {
        global $post;
        if (is_page() && $post->post_parent) {
            $args = array(
                'depth'       => 1,
                'child_of'    => $post->post_parent,
                'echo'        => 0,
                'sort_column' => 'menu_order',
            );
            $childpages = wp_dropdown_pages($args);
        }
        if ($childpages) {
            $string = '<ul>' . $childpages . '</ul>';
        }
        return $string;
    }

    add_shortcode('wpb_childpages', 'wpb_list_child_pages');

    /**
     * Display grand child pages
     *
     * @return string
     */
    function wpb_list_grandchild_pages() {
        global $post;
        if (is_page() && $post->post_parent) {
            $childpages = get_pages('sort_column=menu_order&depth=1&title_li=&child_of=' . $post->ID . '&echo=0');
        }
        if ($childpages) {
            $string = '<select onchange="loadPage(this.value)"><option selected="selected">Neighbourhood</option>';
            foreach ($childpages as $gc) {
                $string .= '<option value="' . $gc->guid . '">' . $gc->post_title . '</option>';
            }
            $string .= '</select>';
        }
        return $string;
    }

    add_shortcode('wpb_grandchildpages', 'wpb_list_grandchild_pages');

    /**
     * Ajax url
     */
    function set_variables() { ?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
        </script>
    <?php }

    add_action('wp_head', 'set_variables');

    /**
     * @param $part
     * @param array $variables
     */
    function pf_get_template_part($part, $variables = array()) {
        $path = locate_template(array('Template-Parts/' . $part . '.php'));
        if ($path) {
            extract($variables, EXTR_SKIP);
            include $path;
        }
    }

    /**
     * Ajax search results
     */
    function pf_search_results_ajax() {
        $helper = new \FOX\HelperClass\HelperClass();

        $listings = $helper::pf_query_listings();
        $search_params = $helper::get_search_params();

        $ttlRecords = $listings->found_posts;

        $result = array();

        if ($listings && !empty($listings)) {
            ob_start();

            foreach ($listings->posts as $listing) {
                pf_get_template_part(
                    'Listing-Card-Part',
                    array(
                        'listing'       => $listing,
                        'search_params' => $search_params
                    )
                );
            }

            $pg = intval($listings->query["paged"]);
            $pg = $pg == 0 ? 1 : $pg;

            $showing = ($pg * 10);
            $showing = $showing >= $ttlRecords ? $ttlRecords : $showing;
            $showingmin = (intval($listings->query["paged"] - 1) * 10) + 1;
            $showingmin = $showingmin <= 1 ? 1 : $showingmin;

            wp_send_json_success(array(
                'html'           => ob_get_clean(),
                'showing'        => $showing,
                'showingmin'     => $showingmin,
                'total_listings' => $ttlRecords,
                'has_more'       => $showing < $ttlRecords,
            ));
        } else {
            wp_send_json_error(array('message' => 'There are no matching properties'));
        }

        wp_send_json_success(array('listings' => $html));
    }

    add_action('wp_ajax_listing_search', 'pf_search_results_ajax', 10);
    add_action('wp_ajax_nopriv_listing_search', 'pf_search_results_ajax', 10);

    /**
     * Search Request
     */
    function ajax_property_details() {
        if (!session_id()) {
            session_start();
        }
        $_SESSION['search'] = $_REQUEST['search'];
        wp_die();
    }

    add_action('wp_ajax_ajax_property_details', 'ajax_property_details');
    add_action('wp_ajax_nopriv_ajax_property_details', 'ajax_property_details');

    //Listings Short Code

    add_shortcode("toplistings", array('ListingShortcode', 'do_shortCode'));

    require('fox-scripts.php'); //Scripts

    add_filter('gform_phone_formats', 'sa_phone_format');
    function sa_phone_format($phone_formats) {
        $phone_formats['sa'] = array(
            'label'       => 'SA',
            'mask'        => '999 999 9999',
            'regex'       => '/^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$/',
            'instruction' => false,
        );

        return $phone_formats;
    }