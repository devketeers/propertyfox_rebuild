<?php
    /*
    Template Name: Advanced Search Page
    */

    get_header();

    wp_enqueue_script("advance-search-js", get_stylesheet_directory_uri() . "/js/advanced-search.js", false, '1.0.0', true);

    //Photo Gallery
    pf_get_template_part("Photo-Gallery-Part", array());

    $helper = new \FOX\HelperClass\HelperClass();

    $result = $helper::pf_query_listings();
    $maxPrice = $helper::pf_get_max("listing_price__c");
    $maxErf = $helper::pf_get_max("land_size__c");
    $search_params = $helper::get_search_params();

    $current = isset($_GET['currentpage']) ? $_GET['currentpage'] : 1;
    if ($current > 1) {
        $current = (($current - 1) * 10) + 1;
    }
?>
    <form id="frmSearch" action="#" method="post">
        <section class="main-content-section listing-list-content">
            <section class="list-menu-wrap desktop">
                <input id="currentpage" name="currentpage" value="<?php echo isset($_GET['currentpage']) ? $_GET['currentpage'] : 1 ?>" type="hidden" />
                <input id="parking" name="parking" value="<?php echo isset($_GET['parking']) ? $_GET['parking'] : '' ?>" type="hidden" />
                <input id="maxprice" name="maxprice" value="<?php echo isset($_GET['maxprice']) ? $_GET['maxprice'] : '' ?>" type="hidden" />
                <input id="minprice" name="minprice" value="<?php echo isset($_GET['minprice']) ? $_GET['minprice'] : '' ?>" type="hidden" />
                <input id="beds" name="beds" value="<?php echo isset($_GET['beds']) ? $_GET['beds'] : '' ?>" type="hidden" />
                <input id="baths" name="baths" value="<?php echo isset($_GET['baths']) ? $_GET['baths'] : '' ?>" type="hidden" />
                <input id="city" name="city" value="<?php echo isset($_GET['city']) ? $_GET['city'] : '' ?>" type="hidden" />
                <input id="suburb" name="suburb" value="<?php echo isset($_GET['suburb']) ? $_GET['suburb'] : '' ?>" type="hidden" />
                <input id="province" name="province" value="<?php echo isset($_GET['province']) ? $_GET['province'] : '' ?>" type="hidden" />
                <input id="ttlitems" name="ttlitems" value="<?php echo $result->found_posts ?>" type="hidden" />
                <div class="search-wrap">
                    <input type="text" name="keywords" id="autocomplete" title="Search" value="<?php echo isset($_GET["keywords"]) ? $_GET["keywords"] : "Enter City, Suburb or Web Ref" ?>">
                    <input id="btnSearch" type="button" class="searchButton" value="submit">
                </div>
                <div class="search-feature-wrap">
                    <ul>
                        <li>
                            <span>Property Type</span>
                            <div class="input-field-wrap">
                                <ul class="seacrh-feature input-field">
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("House", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="House" id="check-1">
                                        <label for="check-1">House</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Apartment / Flat", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Apartment / Flat" id="check-2">
                                        <label for="check-2">Apartment / Flat</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Townhouse", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Townhouse" id="check-3">
                                        <label for="check-3">Townhouse</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Vacant Land / Plot", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Vacant Land / Plot" id="check-4">
                                        <label for="check-4">Vacant Land / Plot</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Farm", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Farm" id="check-5">
                                        <label for="check-5">Farm</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Commercial", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Commercial" id="check-6">
                                        <label for="check-6">Commercial</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter" <?php echo isset($_GET["proptype"]) && in_array("Industrial", $_GET["proptype"]) ? "checked='checked'" : "" ?> name="proptype[]" value="Industrial" id="check-7">
                                        <label for="check-7">Industrial</label>
                                        <div class="check-box"></div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span>Min Price</span>
                            <div class="input-field-wrap">
                                <ul class="input-field pirce-filter minp-filter">
                                    <li data-val="">Any</li>
                                    <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                        <li data-val="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></li>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span>Max Price</span>
                            <div class="input-field-wrap">
                                <ul class="input-field pirce-filter maxp-filter">
                                    <li data-val="">Any</li>
                                    <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                        <li data-val="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></li>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span>Beds</span>
                            <div class="input-field-wrap">
                                <ul class="input-field pirce-filter beds-filter">
                                    <li data-val="">Any</li>
                                    <li data-val="1">1+</li>
                                    <li data-val="2">2+</li>
                                    <li data-val="3">3+</li>
                                    <li data-val="4">4+</li>
                                    <li data-val="5">5+</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span>Baths</span>
                            <div class="input-field-wrap">
                                <ul class="input-field pirce-filter baths-filter">
                                    <li data-val="">Any</li>
                                    <li data-val="1">1+</li>
                                    <li data-val="2">2+</li>
                                    <li data-val="3">3+</li>
                                    <li data-val="4">4+</li>
                                    <li data-val="5">5+</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <span>More</span>
                            <div class="input-field-wrap">
                                <div class="property-type-item-wrap">
                                    <ul class="mero-info input-field">
                                        <li class="more-row">
                                            <span>Parking</span>
                                            <ul class="horizontal-bar parking-filter">
                                                <li data-val="" class="active">Any</li>
                                                <li data-val="1">1+</li>
                                                <li data-val="2">2+</li>
                                                <li data-val="3">3+</li>
                                                <li data-val="4">4+</li>
                                                <li data-val="5">5+</li>
                                            </ul>
                                        </li>
                                        <li class="more-row">
                                            <span>Unit Size</span> <select name="minsq" class="styled-select">
                                                <option value="">Any</option>
                                                <option value="50">50+ m²</option>
                                                <option value="60">60+ m²</option>
                                                <option value="70">70+ m²</option>
                                                <option value="80">80+ m²</option>
                                                <option value="90">90+ m²</option>
                                            </select>
                                        </li>
                                        <li class="more-row">
                                            <span>Erf Size</span> <select name="erf" class="styled-select">
                                                <option value="">Any</option>
                                                <?php for ($i = 100; $i < $maxErf; $i += 100): ?>
                                                    <?php if (isset($_GET["erf"]) && $_GET["erf"] != "" && $_GET["erf"] == $i): ?>
                                                        <option selected value="<?php echo $i ?>"><?php echo $i ?>m²</option>
                                                    <?php else: ?>
                                                        <option value="<?php echo $i ?>"><?php echo $i ?>m² +</option>
                                                    <?php endif; ?>
                                                <?php endfor; ?>
                                            </select>
                                        </li>
                                        <li class="more-row ipad small property-type">
                                            <span>Property Type</span>
                                            <div class="property-triger">Property Type</div>
                                            <div class="input-field-wrap">
                                                <ul class="seacrh-feature input-field">
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="House" id="check-a1" <?php echo isset($_GET["proptype-ipad"]) && in_array("House", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a1">House</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Apartment / Flat" id="check-a2" <?php echo isset($_GET["proptype-ipad"]) && in_array("Apartment / Flat", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a2">Apartment / Flat</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Townhouse" id="check-a3" <?php echo isset($_GET["proptype-ipad"]) && in_array("Townhouse", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a3">Townhouse</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Vacant Land / Plot" id="check-a4" <?php echo isset($_GET["proptype-ipad"]) && in_array("Vacant Land / Plot", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a4">Vacant Land / Plot</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Farm" id="check-a5" <?php echo isset($_GET["proptype-ipad"]) && in_array("Farm", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a5">Farm</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Commercial" id="check-a6" <?php echo isset($_GET["proptype-ipad"]) && in_array("Commercial", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a6">Commercial</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                    <li class="check-item">
                                                        <input type="checkbox" name="proptype-ipad[]" value="Industrial" id="check-a7" <?php echo isset($_GET["proptype-ipad"]) && in_array("Industrial", $_GET["proptype-ipad"]) ? "checked='checked'" : "" ?>>
                                                        <label for="check-a7">Industrial</label>
                                                        <div class="check-box"></div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="more-row  ipad small">
                                            <span>Min Price</span> <select name="minprice-ipad" class="styled-select">
                                                <option value="">Any</option>
                                                <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                                    <option value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </li>
                                        <li class="more-row  ipad small">
                                            <span>Max Price</span> <select name="maxprice-ipad" class="styled-select">
                                                <option value="">Any</option>
                                                <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                                    <option value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </li>
                                        <li class="more-row ipad large">
                                            <span>Beds</span> <select name="beds-ipad" class="styled-select">
                                                <option value="">Any</option>
                                                <option value="1">1+</option>
                                                <option value="2">2+</option>
                                                <option value="3">3+</option>
                                                <option value="4">4+</option>
                                                <option value="5">5+</option>
                                            </select>
                                        </li>
                                        <li class="more-row ipad large">
                                            <span>Baths</span> <select name="baths-ipad" class="styled-select">
                                                <option value="">Any</option>
                                                <option value="1">1+</option>
                                                <option value="2">2+</option>
                                                <option value="3">3+</option>
                                                <option value="4">4+</option>
                                                <option value="5">5+</option>
                                            </select>
                                        </li>
                                        <li class="more-row">
                                            <ul class="more-check">
                                                <li class="check-item">
                                                    <input type="hidden" name="status[]" value="Active" />
                                                    <input type="checkbox" name="status[]" value="Sold" <?php echo isset($_GET["status"]) && in_array("Sold", $_GET["status"]) ? "checked='checked'" : "" ?> id="include">
                                                    <label for="include">Include Sold</label>
                                                    <div class="check-box"></div>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
            <div class="content-inner-wrap">
                <div class="property-wrap list-property">
                    <div class="breadcum-link">
                        <?php
                            $root = get_site_url();
                            $params = '';
                            $splitter = "?";
                            $searchArea = '';
                        ?>
                        <a href="<?php echo $root; ?>/listing-search">Property for Sale</a>
                        <?php if (isset($_GET["province"])) {
                            $params .= $splitter . "province=" . $_GET["province"];
                            $searchArea = $_GET["province"];
                            $splitter = "&"; ?>
                            <a href="<?php echo $root ?>/listing-search<?php echo $params; ?>"><?php echo $_GET["province"]; ?></a>
                        <?php } ?>
                        <?php if (isset($_GET["city"])) {
                            $params .= $splitter . "city=" . $_GET["city"];
                            $searchArea = $_GET["city"];
                            $splitter = "&"; ?>
                            <a href="<?php echo $root ?>/listing-search<?php echo $params ?>"><?php echo $_GET["city"]; ?></a>
                        <?php } ?>
                        <?php if (isset($_GET["suburb"])):
                            $searchArea = $_GET["suburb"];
                            ?>
                            <a href="#" class="active"><?php echo $_GET["suburb"]; ?></a>
                        <?php endif; ?>
                    </div>
                    <h3>
                    <?php if (!$searchArea) { ?>
                        Properties for Sale
                    <?php } else { ?>
                        Property for Sale in <?php echo $searchArea; ?>
                    <?php } ?>
                    </h3>
                    <div class="property-seacrh-wrap">
                        <div class="property-search">
                            <?php
                                $showing = $current + 9;
                                $showing = $showing >= $result->found_posts ? $result->found_posts : $showing;
                            ?>
                            <span class="desktop listing-counts">Showing <?php echo $current . " - " . $showing ?> <em><div class="total-listings">of <?php echo $result->found_posts ?> Results</div></em></span>
                            <span class="mobi"><em class="listing-counts-mobile"><?php echo $result->found_posts ?> Results</em></span>
                            <div class="proprety-selection-wrap">
                                <select id="orderby" name="orderby" class="styled-select">
                                    <option value="createddate:desc">Default Order</option>
                                    <option value="listing_price__c:asc">Price - Low to High</option>
                                    <option value="listing_price__c:desc">Price - High to Low</option>
                                    <option value="createddate:desc">Most Recent</option>
                                    <option value="pba__propertytype__c:desc">Property Type</option>
                                    <option value="land_size__c:desc">Erf Size</option>
                                </select>
                            </div>
                        </div>
                        <div class="property-item-wrap">
                            <?php foreach ($result->posts as $listing) {
                                pf_get_template_part("Listing-Card-Part", array(
                                    "listing"       => $listing,
                                    'search_params' => $search_params
                                ));
                            } ?>
                        </div>
                        <div class="pagination-wrap">
                            <ul id="pager">
                                <span id="pagerPrev" class="disabled">Previous</span>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="map-wrap">
                    <div style="width:100%;" id="divMap"></div>
                </div>
            </div>
            <div class="get-started-wrap" id="get-started-wrap">
                <h4>Save with the smart way to sell your home</h4>
                <a href="#">Get Started</a>
            </div>
            <div class="bottom-fixed-btn">
                <a href="#" class="refine-search"><span>Refine Search</span></a>
                <a href="#" class="booking map-triger"><span>Map View</span><span>List View</span></a>
            </div>
        </section>
        <div class="refine-search-wrap mobi " id="refine-search-wrap">
            <div class="reifne-title">
                <span>Refine your Search</span>
                <div class="close-btn">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-navigation-menu-close.svg" alt="">
                </div>
            </div>
            <div class="rfine-search">
                <span>Search Location</span>
                <div class="search-wrap">
                    <input name="keywords" class="mobileKeyWords" type="text" value="<?php echo isset($_GET["keywords"]) ? $_GET["keywords"] : "Enter City, Suburb or Web Ref" ?>">
                </div>
                <div class="search-feature-wrap">
                    <span>Price Range</span>
                    <ul>
                        <li>
                            <select name="minprice-mobile" class="styled-select mobileSearchDD">
                                <option value="">Min Price</option>
                                <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                    <?php if (isset($_GET["minprice-mobile"]) && $_GET["minprice-mobile"] != "" && $_GET["minprice-mobile"] == $i): ?>
                                        <option selected value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </select>
                        </li>
                        <li>
                            <select name="maxprice-mobile" class="styled-select mobileSearchDD">
                                <option value="">Max Price</option>
                                <?php for ($i = 100000; $i < $maxPrice; $i += 50000): ?>
                                    <?php if (isset($_GET["maxprice-mobile"]) && $_GET["maxprice-mobile"] != "" && $_GET["maxprice-mobile"] == $i): ?>
                                        <option selected value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                    <?php else: ?>
                                        <option value="<?php echo $i ?>">R <?php echo str_replace(",", " ", number_format($i)) ?></option>
                                    <?php endif; ?>
                                <?php endfor; ?>
                            </select>
                        </li>
                    </ul>
                </div>
                <div class="search-feature-wrap property-type-wrap">
                    <span>Property Type</span>
                    <ul>
                        <li>
                            <span>Property Type</span>
                            <div class="input-field-wrap">
                                <ul class="seacrh-feature input-field">
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="House" id="fcheck-1" <?php echo isset($_GET["proptype-mobile"]) && in_array("House", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-1">House</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Apartment / Flat" id="fcheck-2" <?php echo isset($_GET["proptype-mobile"]) && in_array("Apartment / Flat", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-2">Apartment / Flat</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Townhouse" id="fcheck-3" <?php echo isset($_GET["proptype-mobile"]) && in_array("Townhouse", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-3">Townhouse</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Vacant Land / Plot" id="fcheck-4" <?php echo isset($_GET["proptype-mobile"]) && in_array("Vacant Land / Plot", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-4">Vacant Land / Plot</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Farm" id="fcheck-5" <?php echo isset($_GET["proptype-mobile"]) && in_array("Farm", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-5">Farm</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Commercial" id="fcheck-6" <?php echo isset($_GET["proptype-mobile"]) && in_array("Commercial", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-6">Commercial</label>
                                        <div class="check-box"></div>
                                    </li>
                                    <li class="check-item">
                                        <input type="checkbox" class="search-filter mobileSearchCK" name="proptype-mobile[]" value="Industrial" id="fcheck-7" <?php echo isset($_GET["proptype-mobile"]) && in_array("Industrial", $_GET["proptype-mobile"]) ? "checked='checked'" : "" ?>>
                                        <label for="fcheck-7">Industrial</label>
                                        <div class="check-box"></div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="radio-row-info">
                    <span>Bedrooms</span>
                    <div class="radio-content">
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="" value="" <?php echo !isset($_GET["beds-mobile"]) || $_GET["beds-mobile"] == "" ? "checked" : "" ?>>
                            <label for="item-1">Any</label>
                        </div>
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="1" value="1" <?php echo isset($_GET["beds-mobile"]) && $_GET["beds-mobile"] == 1 ? "checked" : "" ?>>
                            <label for="item-2">1+</label>
                        </div>
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="2" value="2" <?php echo isset($_GET["beds-mobile"]) && $_GET["beds-mobile"] == 2 ? "checked" : "" ?>>
                            <label for="item-3">2+</label>
                        </div>
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="3" value="3" <?php echo isset($_GET["beds-mobile"]) && $_GET["beds-mobile"] == 3 ? "checked" : "" ?>>
                            <label for="item-4">3+</label>
                        </div>
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="4" value="4" <?php echo isset($_GET["beds-mobile"]) && $_GET["beds-mobile"] == 4 ? "checked" : "" ?>>
                            <label for="item-5">4+</label>
                        </div>
                        <div class="radio-item mobile-beds">
                            <input type="radio" name="beds-mobile" id="5" value="5" <?php echo isset($_GET["beds-mobile"]) && $_GET["beds-mobile"] == 5 ? "checked" : "" ?>>
                            <label for="item-6">5+</label>
                        </div>
                    </div>
                </div>
                <div class="radio-row-info">
                    <span>Bathrooms</span>
                    <div class="radio-content">
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="" value="" <?php echo !isset($_GET["baths-mobile"]) || $_GET["baths-mobile"] == "" ? "checked" : "" ?>>
                            <label for="call-1">Any</label>
                            <div class="check"></div>
                        </div>
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="1" value="1" <?php echo isset($_GET["baths-mobile"]) && $_GET["baths-mobile"] == 1 ? "checked" : "" ?>>
                            <label for="call-2">1+</label>
                        </div>
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="2" value="2" <?php echo isset($_GET["baths-mobile"]) && $_GET["baths-mobile"] == 2 ? "checked" : "" ?>>
                            <label for="call-3">2+</label>
                        </div>
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="3" value="3" <?php echo isset($_GET["baths-mobile"]) && $_GET["baths-mobile"] == 3 ? "checked" : "" ?>>
                            <label for="call-4">3+</label>
                        </div>
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="4" value="4" <?php echo isset($_GET["baths-mobile"]) && $_GET["baths-mobile"] == 4 ? "checked" : "" ?>>
                            <label for="call-5">4+</label>
                        </div>
                        <div class="radio-item mobile-baths">
                            <input type="radio" name="baths-mobile" id="5" value="5" <?php echo isset($_GET["baths-mobile"]) && $_GET["baths-mobile"] == 5 ? "checked" : "" ?>>
                            <label for="call-6">5+</label>
                        </div>
                    </div>
                </div>
                <div class="radio-row-info">
                    <span>Parking</span>
                    <div class="radio-content">
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-1" value="" <?php echo !isset($_GET["parking-mobile"]) || $_GET["parking-mobile"] == "" ? "checked" : "" ?>>
                            <label for="items-1">Any</label>
                            <div class="check"></div>
                        </div>
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-2" value="1" <?php echo isset($_GET["parking-mobile"]) && $_GET["parking-mobile"] == 1 ? "checked" : "" ?>>
                            <label for="items-2">1+</label>
                        </div>
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-3" value="2" <?php echo isset($_GET["parking-mobile"]) && $_GET["parking-mobile"] == 2 ? "checked" : "" ?>>
                            <label for="items-3">2+</label>
                        </div>
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-4" value="3" <?php echo isset($_GET["parking-mobile"]) && $_GET["parking-mobile"] == 3 ? "checked" : "" ?>>
                            <label for="items-4">3+</label>
                        </div>
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-5" value="4" <?php echo isset($_GET["parking-mobile"]) && $_GET["parking-mobile"] == 4 ? "checked" : "" ?>>
                            <label for="items-5">4+</label>
                        </div>
                        <div class="radio-item mobile-parking">
                            <input type="radio" name="parking-mobile" id="items-6" value="5" <?php echo isset($_GET["parking-mobile"]) && $_GET["parking-mobile"] == 5 ? "checked" : "" ?>>
                            <label for="items-6">5+</label>
                        </div>
                    </div>
                </div>
                <div class="select-item-row unit-size">
                    <span>Unit Size</span> <select name="minsq-mobile" class="styled-select mobileSearchDD">
                        <option <?php echo !isset($_GET["minsq-mobile"]) || $_GET["minsq-mobile"] == "" ? "selected" : "" ?> value="">Any</option>
                        <option <?php echo isset($_GET["minsq-mobile"]) && $_GET["minsq-mobile"] == 50 ? "selected" : "" ?> value="50">50+ m²</option>
                        <option <?php echo isset($_GET["minsq-mobile"]) && $_GET["minsq-mobile"] == 60 ? "selected" : "" ?> value="60">60+ m²</option>
                        <option <?php echo isset($_GET["minsq-mobile"]) && $_GET["minsq-mobile"] == 70 ? "selected" : "" ?> value="70">70+ m²</option>
                        <option <?php echo isset($_GET["minsq-mobile"]) && $_GET["minsq-mobile"] == 80 ? "selected" : "" ?> value="80">80+ m²</option>
                        <option <?php echo isset($_GET["minsq-mobile"]) && $_GET["minsq-mobile"] == 90 ? "selected" : "" ?> value="90">90+ m²</option>
                    </select>
                </div>
                <div class="select-item-row erf-size">
                    <span>Erf Size</span> <select name="erf-mobile" class="styled-select mobileSearchDD">
                        <option <?php echo !isset($_GET["erf-mobile"]) || $_GET["erf-mobile"] == "" ? "selected" : "" ?> value="">Any</option>
                        <?php for ($i = 100; $i < $maxErf; $i += 100): ?>
                            <?php if (isset($_GET["erf-mobile"]) && $_GET["erf-mobile"] != "" && $_GET["erf-mobile"] == $i): ?>
                                <option selected value="<?php echo $i ?>"><?php echo $i ?>m² +</option>
                            <?php else: ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?>m²</option>
                            <?php endif; ?>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="sold-out-wrap">
                    <div class="sold-out">
                        <dfn>Include Sold</dfn>
                        <label class="switch">
                            <input name="sold-mobile" class="mobile-sold" value="sold" type="checkbox">
                            <span class="slider"></span>
                        </label>
                    </div>
                </div>
                <div class="removeTriger" id="removeTriger">Remove Filters</div>
                <div class="apply-filter-btn">
                    <input type="button" class="searchButton" value="Apply Filters">
                </div>
            </div>
        </div>
    </form>
<?php get_footer(); ?>