<div class="slide">
    <div class="carousel-item">
        <?php pf_get_template_part("Listing-Base-Part",
            array(
                "listing"    => $listing,
                "showMapPin" => false,
                "fullurl"    => true,
            )
        ); ?>
    </div>
</div>