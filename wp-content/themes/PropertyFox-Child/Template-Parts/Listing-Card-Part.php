<div class="property-item">
    <?php pf_get_template_part("Listing-Base-Part",
        array(
            "listing"       => $listing,
            "showMapPin"    => true,
            "search_params" => $search_params
        )
    ); ?>
</div>