<?php
    wp_enqueue_style("flex-slider", get_stylesheet_directory_uri() . "/css/flexslider.css", false, '1.0.0', 'all');
    wp_enqueue_script("flex-slider-js", get_stylesheet_directory_uri() . "/js/jquery.flexslider-min.js", false, '1.0.0', true);
?>
<!-- Gallery Modal -->
<div style="display:none;" id="photoGallery">
    <div class="main-wrap">
        <section class="main-content-section modal-content-section">
            <section class="modal-section">
                <div class="modal-inner">
                    <div id="modTitle" class="modal-title">
                        <h5></h5>
                        <p></p>
                    </div>
                    <div class="close-btn"><a class="galleryClose" href="javascript:void(0)">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-gallery-close-desktop.svg" alt="">
                        </a></div>
                    <div class="modal-slider-wrapper">
                        <div class="modal-wrap clear">
                            <div class="modal-slider-wrap flexslider">
                                <ul class="slides galleryslides">
                                </ul>
                            </div>
                            <div class="pagination-item">
                                <div id="modCurrentSlide" class="active-slide-no"></div>
                                <span> / </span>
                                <div id="modSlides" class="total-slide"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </div>
</div>