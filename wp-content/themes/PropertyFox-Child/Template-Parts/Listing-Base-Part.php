<?php
    $helper = new \FOX\HelperClass\HelperClass();
    $media = $helper::pf_get_media($listing->ID);
    $fp = $helper::pf_get_floorplan($listing->ID);
    $video = $helper::pf_get_videos($listing->ID);
    $img = "";  //TODO: Default Image


    if (is_array($media) && sizeof($media) > 0) {
        $media = array_reverse($media);
        $img = $media[0];
    }
    $images = '';
    foreach ($media as $img) {
        $images .= '"' . $img . '",';
    }
?>
<div class="property-figure-wrap"
        data-lat="<?php echo get_post_meta($listing->ID, "pba__latitude_pb__c", true); ?>"
        data-lng="<?php echo get_post_meta($listing->ID, "pba__longitude_pb__c", true); ?>"
        data-basedir="<?php echo get_stylesheet_directory_uri() ?>"
        data-parking="<?php echo get_post_meta($listing->ID, "garages__c", true); ?>"
        data-price="<?php echo get_post_meta($listing->ID, "listing_price__c", true); ?>"
        data-beds="<?php echo get_post_meta($listing->ID, "bedrooms__c", true); ?>"
        data-baths="<?php echo get_post_meta($listing->ID, "bathrooms__c", true); ?>"
        data-park="<?php echo get_post_meta($listing->ID, "bathrooms__c", true); ?>"
        data-erf="<?php echo get_post_meta($listing->ID, "land_size__c", true); ?>"
        data-title="<?php echo $listing->post_title; ?>"
        data-allImg="<?php echo join(";", $media); ?>"
        data-allFP="<?php echo join(";", $fp); ?>"
        data-allVid="<?php echo join(";", $video); ?>"
        data-imgCount="<?php echo sizeof($media); ?>"
        data-thumbnail="<?php echo $img; ?>"
        data-id="<?php echo $listing->ID; ?>"
        data-url="<?php echo get_permalink($listing->ID); ?>"
        data-status="<?php echo get_post_meta($listing->ID, "pba__status__c", true); ?>">
    <figure>
        <a href="<?php echo get_permalink($listing->ID) ?>" class="images-rotation" data-images='[<?php echo rtrim($images, ','); ?>]'>
        <img src="<?php echo $img ?>" alt="<?php echo $listing->post_title ?>"></a>
    </figure>
    <div class="property-figure-icon">
        <div class="new">
            <?php echo get_post_meta($listing->ID, "pba__status__c", true); ?>
        </div>
    </div>
    <div class="propety-figure-bottom">
        <div class="propety-figure-bottom-inner">
            <?php if ($showMapPin) : ?>
                <a href="#" title="Show on map" class="propety-bottom-icon showOnMap" data-id="<?php echo $listing->ID ?>" data-base="<?php echo get_stylesheet_directory_uri(); ?>">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-mapped-white.svg" alt="Show on map">
                </a>
            <?php endif; ?>
            <?php if (is_array($fp) && sizeof($fp) > 0): ?>
                <a href="javascript:void(0);" title="View floor plan" class="propety-bottom-icon floorplan">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-floorplan-white.svg" alt="floorplan">
                </a>
            <?php endif; ?>
            <?php if (is_array($video) && sizeof($video) > 0): ?>
                <a href="javascript:void(0);" title="View video" class="propety-bottom-icon video">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-video-white.svg" alt="video">
                </a>
            <?php endif; ?>
            <a href="#photoGallery" title="View image gallery" class="propety-bottom-icon gallery">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-photos-white.svg" alt="gallery">
            <span class="num"> <?php echo sizeof($media); ?></span>
            </a>
        </div>
    </div>
</div>
<div class="property-content-wrap">
    <h5 class="desktop">
    <a href="<?php echo get_permalink($listing->ID) ?>">
    <?php if (get_post_meta($listing->ID, "listing_price__c", true) == 999999999) {
        echo "POA";
    } else { ?>
        R <?php echo str_replace(",", " ", number_format(get_post_meta($listing->ID, "listing_price__c", true))) ?>
    <?php } ?>
    </a>
    </h5> <h5 class="mobi">
    <?php if (get_post_meta($listing->ID, "listing_price__c", true) == 999999999) {
        echo "POA";
    } else { ?>
        R  <?php echo str_replace(",", " ", number_format(get_post_meta($listing->ID, "listing_price__c", true))) ?>
    <?php } ?>
    </h5> <dfn class="desktop">
        <a href="<?php echo get_permalink($listing->ID) ?>">
        <?php echo $listing->post_title ?>
        </a>
    </dfn> <dfn class="mobi">
        <a href="<?php echo get_permalink($listing->ID) ?>">
        <?php echo $listing->post_title ?>
        </a>
    </dfn>
    <div class="property-icon-wrap">
        <div class="property-icon-item">
            <div class="property-single-icon">
                <figure>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-beds-grey.svg" alt="">
                </figure>
                <span> <?php echo get_post_meta($listing->ID, "bedrooms__c", true) ?>  </span>
            </div>
            <div class="property-single-icon">
                <figure>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-baths-grey.svg" alt="">
                </figure>
                <span> <?php echo get_post_meta($listing->ID, "bathrooms__c", true) ?> </span>
            </div>
            <div class="property-single-icon">
                <figure>
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/svgs/svg-property-garage-grey.svg" alt="">
                </figure>
                <span><?php echo get_post_meta($listing->ID, "garages__c", true) ?></span>
            </div>
        </div>
        <?php if (get_post_meta($listing->ID, "land_size__c", true)) { ?>
            <?php switch (get_post_meta($listing->ID, "measurement_type__c", true)) {
                case "Hectares":
                    $measurement = "ha";
                    break;
                case "Metres Squared":
                    $measurement = 'm²';
                    break;
            } ?>
            <p>Erf size: <span><?php echo get_post_meta($listing->ID, "land_size__c", true) . $measurement ?></span></p>
        <?php } ?>
    </div>
</div>