<?php if ('on' == et_get_option('divi_back_to_top', 'false')) : ?>
    <span class="et_pb_scroll_top et-pb-icon"></span>
<?php endif; ?>
<?php if (!is_page_template('page-template-blank.php')) : ?>
    <footer id="main-footer">
        <?php get_sidebar('footer'); ?>
        <?php if (has_nav_menu('footer-menu')) : ?>
            <div id="et-footer-nav">
                <div class="container">
                    <?php wp_nav_menu([
                        'theme_location' => 'footer-menu',
                        'depth'          => '1',
                        'menu_class'     => 'bottom-nav',
                        'container'      => '',
                        'fallback_cb'    => '',
                    ]); ?>
                </div>
            </div> <!-- #et-footer-nav -->
        <?php endif; ?>
        <div id="footer-bottom">
            <div class="container clearfix">
                <?php if (false !== et_get_option('show_footer_social_icons', true)) {
                    get_template_part('includes/social_icons', 'footer');
                }
                    echo et_get_footer_credits(); ?>
            </div>
        </div>
    </footer>
    </div>
<?php endif; ?>
</div>
<script type="text/javascript">
    jQuery(window).load(function () {
        jQuery("#hidden-during-page-load").show();
    });

    jQuery(document).ready(function () {
        jQuery('ul.sidenav-sublist li').eq(0).addClass('active');

        //tab 1
        jQuery('#open-tab-dance a.nextTabButton').on('click', function (event) {
            event.preventDefault();
            jQuery('#my-tabs .et_pb_tab_1 a').click();
        });

        //tab 2
        jQuery('#open-tab-dance2 a.nextTabButton').on('click', function (event) {
            event.preventDefault();
            jQuery('#my-tabs .et_pb_tab_2 a').click();
        });

        //tab 3
        jQuery('#open-tab-dance3 a.nextTabButton').on('click', function (event) {
            event.preventDefault();
            jQuery('#my-tabs .et_pb_tab_3 a').click();
        });

        //tab 4
        jQuery('#open-tab-dance4 a.nextTabButton').on('click', function (event) {
            event.preventDefault();
            jQuery('#my-tabs .et_pb_tab_4 a').click();
        });

        //tab 5
        jQuery('#open-tab-dance5 a.nextTabButton').on('click', function (event) {
            event.preventDefault();
            jQuery('#my-tabs .et_pb_tab_5 a').click();
        });

    });
</script>
<?php wp_footer(); ?>
</body>
</html>