<?php

    namespace FOX\HelperClass;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    class HelperClass {
        /**
         * @param $id
         * @return array
         */
        public static function pf_get_media($id) {
            if (NULL === get_post_meta($id, "media", true) || get_post_meta($id, "media", true) === "") {
                return array();
            }

            $media = explode(";", get_post_meta($id, "media", true));
            return $media;
        }

        /**
         * @param $id
         * @return array
         */
        public static function pf_get_floorplan($id) {
            if (NULL === get_post_meta($id, "floorplan", true) || get_post_meta($id, "floorplan", true) === "") {
                return array();
            }

            $media = explode(";", get_post_meta($id, "floorplan", true));
            return $media;
        }

        /**
         * @param $id
         * @return array
         */
        public static function pf_get_videos($id) {
            if (NULL === get_post_meta($id, "video", true) || get_post_meta($id, "video", true) === "") {
                return array();
            }

            $media = explode(";", get_post_meta($id, "video", true));
            return $media;
        }

        /**
         * @param $field
         * @return null|string
         */
        public static function pf_get_max($field) {
            global $wpdb;
            $query = $wpdb->prepare("SELECT MAX(cast(meta_value as UNSIGNED)) FROM {$wpdb->postmeta} WHERE meta_value!='999999999' and meta_key='%s'", $field);
            $max = $wpdb->get_var($query);
            return $max;
        }

        /**
         * @param bool $data
         * @return \WP_Query
         */
        public static function pf_query_listings($data = false) {
            $params = self::get_search_params($data);
            $meta_query = array();
            $orderby = array();
            $pageSize = 10;
            $page = 1;

            if (isset($params["currentpage"]) && $params["currentpage"] != "") {
                $page = intval($params["currentpage"]);
            }

            if (isset($params["itemsperpage"]) && $params["itemsperpage"] != "") {
                $pageSize = intval($params["itemsperpage"]);
            }

            if (isset($params["proptype"]) && is_array($params["proptype"]) && sizeof($params["proptype"]) > 0) {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'pba__propertytype__c',
                        'value'   => $params["proptype"],
                        'compare' => 'IN'
                    )
                );
            }

            if (isset($params["beds"]) && $params["beds"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'bedrooms__c',
                        'value'   => intval($params["beds"]),
                        'compare' => '>=',
                        'type'    => 'numeric'
                    )
                );
            }

            if (isset($params["baths"]) && $params["baths"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'bathrooms__c',
                        'value'   => intval($params["baths"]),
                        'compare' => '>=',
                        'type'    => 'numeric'
                    )
                );
            }

            if (isset($params["maxprice"]) && $params["maxprice"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'listing_price__c',
                        'value'   => intval($params["maxprice"]),
                        'compare' => '<=',
                        'type'    => 'numeric'
                    )
                );
            }

            if (isset($params["id"]) && $params["id"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'ListingId',
                        'value'   => $params["id"],
                        'compare' => '!='
                    )
                );
            }

            if (isset($params["minprice"]) && $params["minprice"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'listing_price__c',
                        'value'   => intval($params["minprice"]),
                        'compare' => '>=',
                        'type'    => 'numeric'
                    )
                );
            }

            if (isset($params["minsq"]) && $params["minsq"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'land_size__c',
                        'value'   => intval($params["minsq"]),
                        'compare' => '>=',
                        'type'    => 'numeric'
                    )
                );
            }


            if (isset($params["parking"]) && $params["parking"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'garages__c',
                        'value'   => $params["parking"],
                        'compare' => '>='
                    )
                );
            }

            if (isset($params["erf"]) && $params["erf"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'land_size__c',
                        'value'   => $params["erf"],
                        'compare' => '>='
                    )
                );
            }

            if (isset($params["province"]) && $params["province"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'pba__state_pb__c',
                        'value'   => $params["province"],
                        'compare' => '='
                    )
                );
            }

            if (isset($params["city"]) && $params["city"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'pba__city_pb__c',
                        'value'   => $params["city"],
                        'compare' => '='
                    )
                );
            }

            if (isset($params["suburb"]) && $params["suburb"] != "") {
                $meta_query[] = array(
                    'relation' => 'AND',
                    array(
                        'key'     => 'pba__area_pb__c',
                        'value'   => $params["suburb"],
                        'compare' => '='
                    )
                );
            }

            if (substr($params["keywords"], 0, 2) === "pf") {
                $meta_query[] = array(
                    'key'     => 'pf_number__c',
                    'value'   => $params["keywords"],
                    'compare' => 'LIKE',
                );
            } else {
                if (isset($params["orderby"]) && $params["orderby"] != "") {
                    $parts = explode(":", $params["orderby"]);
                    $orderby["meta_key"] = $parts[0];

                    if ($parts[0] === "pba__propertytype__c") {
                        $orderby["orderby"] = "meta_value";
                    } elseif ($parts[0] === "createddate") {
                        $meta_query[] = array(
                            'createddate' => array(
                                'key'     => 'createddate',
                                'type'    => 'DATE',
                                'compare' => 'EXISTS'
                            ));
                        $orderby["orderby"] = "createddate";
                    } else {
                        $orderby["orderby"] = "meta_value_num";
                    }
                    $orderby["order"] = $parts[1];
                }

                /* @todo - this needs to be looked at again
                 * if (isset($params["status"]) && $params["status"] != "") {
                 * $meta_query[] = array(
                 * 'relation' => 'AND',
                 * array(
                 * 'key'     => 'pba__status__c',
                 * 'value'   => $params["status"],
                 * 'compare' => 'IN'
                 * )
                 * );
                 * }*/

                if (isset($params["keywords"]) && $params["keywords"] != "" && $params["keywords"] != "Enter City, Suburb or Web Ref") {
                    $meta_query[] = array(
                        'relation' => 'OR',
                        array(
                            'key'   => 'pba__status__c',
                            'value' => $params["status"],
                        ),
                        array(
                            'key'     => 'pba__city_pb__c',
                            'value'   => $params["keywords"],
                            'compare' => 'LIKE',
                        ),
                        array(
                            'key'     => 'pba__state_pb__c',
                            'value'   => $params["keywords"],
                            'compare' => 'LIKE',
                        ),
                        array(
                            'key'     => 'pba__area_pb__c',
                            'value'   => $params["keywords"],
                            'compare' => 'LIKE',
                        ),
                    );
                }
            }

            $args = array(
                'post_type'      => 'property',
                'posts_per_page' => $pageSize,
                'post_status'    => 'publish',
                'paged'          => $page,
                'meta_query'     => $meta_query,

            );


            $args = array_merge($args, $orderby);

            if (isset($params["keywords"]) && substr($params["keywords"], 0, 2) !== "pf") {
                $args['s'] = $params["keywords"];
            }

            $query = new \WP_Query($args);



            return $query;
        }

        /**
         * @param bool $data
         * @return array
         */
        public static function get_search_params($data = false) {
            if (!is_array($data)) {
                $data = $_GET;
                // Don't allow the itemsperpage to be overriden through $_GET
                unset($data['itemsperpage']);
            }

            $defaults = array(
                'currentpage'  => 1,
                'itemsperpage' => 10,
                'status'       => array('Active', 'Reserved'),
                'orderby'      => 'createddate:DESC',
                'parking'      => '',
                'type'         => '',
                'beds'         => '',
                'baths'        => '',
                'proptype'     => array(),
                'province'     => '',
                'suburb'       => '',
                'city'         => '',
                'price'        => '',
                'minprice'     => '',
                'maxprice'     => '',
                'minsq'        => '',
                'maxsq'        => '',
                'minacr'       => '',
                'maxacr'       => '',
                'sq'           => '',
                'daysm'        => '',
                'changed'      => '',
                'keywords'     => '',
                'erf'          => '',
            );

            if (isset($data["maxprice-ipad"]) && $data["maxprice-ipad"] != "") {
                $data["maxprice"] = $data["maxprice-ipad"];
            } elseif (isset($data["maxprice-mobile"]) && $data["maxprice-mobile"] != "") {
                $data["maxprice"] = $data["maxprice-mobile"];
            }

            if (isset($data["minprice-ipad"]) && $data["minprice-ipad"] != "") {
                $data["minprice"] = $data["minprice-ipad"];
            } elseif (isset($data["minprice-mobile"]) && $data["minprice-mobile"] != "") {
                $data["minprice"] = $data["minprice-mobile"];
            }

            if (isset($data["beds-ipad"]) && $data["beds-ipad"] != "") {
                $data["beds"] = $data["beds-ipad"];
            } elseif (isset($data["beds-mobile"]) && $data["beds-mobile"] != "") {
                $data["beds"] = $data["beds-mobile"];
            }

            if (isset($data["erf-ipad"]) && $data["erf-ipad"] != "") {
                $data["erf"] = $data["erf-ipad"];
            } elseif (isset($data["erf-mobile"]) && $data["erf-mobile"] != "") {
                $data["erf"] = $data["erf-mobile"];
            }

            if (isset($data["baths-ipad"]) && $data["baths-ipad"] != "") {
                $data["baths"] = $data["baths-ipad"];
            } elseif (isset($data["baths-mobile"]) && $data["baths-mobile"] != "") {
                $data["baths"] = $data["baths-mobile"];
            }

            if (isset($data["proptype-ipad"]) && $data["proptype-ipad"] != "") {
                $data["proptype"] = $data["proptype-ipad"];
            } elseif (isset($data["proptype-mobile"]) && $data["proptype-mobile"] != "") {
                $data["proptype"] = $data["proptype-mobile"];
            }

            if (isset($data["keywords-mobile"]) && $data["keywords-mobile"] != "Enter City, Suburb or Web Ref") {
                $data["keywords"] = $data["keywords-mobile"];
            }

            if (isset($data["parking-mobile"]) && $data["parking-mobile"] != "") {
                $data["parking"] = $data["parking-mobile"];
            }

            if (isset($data["minsq-mobile"]) && $data["minsq-mobile"] != "") {
                $data["minsq"] = $data["minsq-mobile"];
            }

            if (isset($data["sold-mobile"]) && $data["sold-mobile"] != "") {
                array_push($data["status"], $data["sold-mobile"]);
            }

            unset($data["action"]);

            $args = array_filter(wp_parse_args($data, $defaults));

            return $args;
        }
    }
