<?php

    /**
     * Custom Admin Css
     */
    function fox_admin_scripts() {
        wp_enqueue_style('fox-admin-theme', get_template_directory_uri() . '/FOX/Assets/Css/admin.css');
    }

    add_action('admin_enqueue_scripts', 'fox_admin_scripts');

    /**
     * Admin Login Scripts
     */
    function fox_admin_login_scripts() {
        wp_enqueue_style('fox-admin-theme', get_template_directory_uri() . '/FOX/Assets/Css/admin.css');
        wp_enqueue_style('fox-login-theme', get_template_directory_uri() . '/FOX/Assets/Css/login.css');
    }

    add_action('login_enqueue_scripts', 'fox_admin_login_scripts');

    /**
     * Change logo url for login screen
     *
     * @return string
     */
    function loginpage_custom_link() {
        return 'http://propertyfox.co.za';
    }

    add_filter('login_headerurl', 'loginpage_custom_link');

    /**
     * @return string
     */
    function change_title_on_logo() {
        return 'Click here to find out more about Propertyfox';
    }

    add_filter('login_headertitle', 'change_title_on_logo');

    /**
     * Filters for branding admin area
     *
     * @param $text
     * @return string
     */
    function left_admin_footer_text_output($text) {
        $text = '&copy; ' . date('Y') . ' - ' . get_bloginfo('name', 'display');
        return $text;
    }

    add_filter('admin_footer_text', 'left_admin_footer_text_output'); //left side

    /**
     * Admin footer update
     *
     * @param $text
     * @return string
     */
    function right_admin_footer_text_output($text) {
        $text = 'Theme by <a href="http://www.propertyfox.co.za">Property Fox</a>';
        return $text;
    }

    add_filter('update_footer', 'right_admin_footer_text_output', 11); //right side

    /**
     * Start everything
     */
    if (is_admin()) {
        \FOX\SPAdmin\SPAdmin::spadmin();
        \FOX\SPAdmin\SPDashboard::spdashboard();
    }