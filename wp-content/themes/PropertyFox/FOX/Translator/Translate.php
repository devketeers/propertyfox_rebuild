<?php

    namespace FOX\Translator;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class Translate
     * @package FOX\Translate
     */
    class Translate {
        protected static $translator = NULL;
        public $translated = array();

        /**
         * @return Translate|null
         */
        static function t() {
            if (self::$translator === NULL) {
                self::$translator = new Translate();
            }
            return self::$translator;
        }

        /**
         * @param $language
         * @return string
         */
        public function translate($language) {
            $_SESSION['translate'] = true;
            if ($_SESSION['translate'] == true) {
                if (!count($this->translated) > 0) {
                    $posts_array = get_posts(array('post_type' => 'translations'));
                    foreach ($posts_array as $key => $val) {
                        $english = get_post_meta($val->ID, 'fox_translatefrench', true);
                        $this->translated[strtolower($val->post_title)] = $english;
                    }
                }
                if (array_key_exists(strtolower($language), $this->translated)) {
                    return ucfirst($this->translated[strtolower($language)]);
                } else {
                    return $language;
                }
            }
        }
    }