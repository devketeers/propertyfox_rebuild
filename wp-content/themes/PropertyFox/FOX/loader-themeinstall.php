<?php
// Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Actions for theme setup
     */
    function fox_theme_setup() {
        add_option('foxplug version', SKELETON_VER);
        add_action('admin_notices', 'fox_successful_setup');
    }

    /**
     *Actions for theme setup
     */
    function fox_successful_setup() { ?>
        <div class="updated">
            <h3>FoxPlug Installed</h3>
            <p>FoxPlug has been activated successfully.</p>
        </div>
    <?php }

    add_action('after_switch_theme', 'fox_theme_setup');

