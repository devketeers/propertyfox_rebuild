<?php

    namespace FOX\Sidebars;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class Sidebars
     * @package FOX\Sidebars
     */
    class Sidebars {
        private $sidebars = array();

        /**
         * sideBars constructor.
         */
        function __construct() {
            add_action('widgets_init', array($this, 'initSidebar'));
        }

        /**
         * Register Sidebar
         */
        function initSidebar() {
            foreach ($this->sidebars as $sidebar) {
                register_sidebar($sidebar);
            }
        }

        /**
         * @param array $sidebar
         * @throws \Exception
         */
        function addSideBar($sidebar) {
            if (!$sidebar['name'] || !$sidebar['id']) {
                throw new \Exception('No Sidebar name or ID provided');
            }
            $this->sidebars[] = $sidebar;
        }
    }