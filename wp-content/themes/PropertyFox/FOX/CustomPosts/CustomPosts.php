<?php

    namespace FOX\CustomPosts;

    use FOX\CustomFields\CustomFields;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class CustomPosts
     *
     * @package FOX\CustomPosts
     */
    class CustomPosts extends CustomFields {

        private $postTypeID, $multipleName, $singularName, $addNewLabel, $description, $supports, $taxonomies, $icon, $url;

        /**
         * CustomPosts constructor.
         * @param string $postType Post type identifier
         * @param string $postTypeName Post type pretty name
         * @param array $taxonomies Taxonomy display and name
         * @param array $supports Wordpress CPT supports array
         * @param array $extra Wordpress CPT extra settings
         */
        public function __construct($postType, $postTypeName, $taxonomies = array(false, 'Category'), $supports = array('title', 'editor'), $extra = array('description' => 'A new post type', 'icon' => 'dashicons-menu', 'archive-edit' => false)) {
            $this->postTypeID = $postType;
            $this->url = strtolower(str_replace(" ", "-", $postType));
            $this->multipleName = $postTypeName;
            $this->singularName = $postTypeName;
            $this->addNewLabel = 'Add New ' . __($postTypeName, 'project_fox_domain');
            $this->supports = $supports;
            $this->taxonomies = $taxonomies;
            $this->icon = $extra['icon'];
            $this->description = $extra['description'];

            if ($taxonomies[0]) {
                add_action('init', array($this, 'foxRegisterTaxonomy'));
                add_rewrite_rule('^' . $this->url . '/?$', 'index.php?post_type=' . $this->postTypeID, 'top');
                add_rewrite_rule('^' . $this->url . '/([^/]*)?$', 'index.php?' . $this->postTypeID . 'category_term=$matches[1]', 'top');
                add_rewrite_rule('^' . $this->url . '/([^/]*)/([^/]*)?$', 'index.php?' . $this->postTypeID . 'post_term=$matches[2]', 'top');
            }
            add_action('init', array($this, 'foxRegisterPostType'));

            if ($extra['archive-edit']) {
                add_action('admin_menu', array($this, 'fox_admin_menu'));
            }

            parent::__construct($this->postTypeID);
        }

        /**
         * Add archive menu item to
         */
        public function fox_admin_menu() {
            add_submenu_page('edit.php?post_type=' . $this->postTypeID, '', $this->singularName . ' Archive', 'manage_options', 'archive-' . $this->postTypeID, array($this, 'fox_custom_content'));
        }

        /**
         * Custom Archive Templates
         */
        public function fox_custom_content() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            foreach ($_POST['content'] as $key => $val) {
                update_option($key, $val);
            }

            if (file_exists(__DIR__ . '/Templates/archive-' . $this->postTypeID . '.php')) {
                include('Templates/archive-' . $this->postTypeID . '.php');
            } else {
                include('Templates/archive.php');
            }
        }

        /**
         * Register Taxonomies for Posts
         */
        public function foxRegisterTaxonomy() {
            register_taxonomy(
                str_replace(' ', '_', strtolower($this->taxonomies[1])) . '_category',
                $this->postTypeID,
                array(
                    'hierarchical' => true,
                    'labels'       => array(
                        'name'          => _x($this->taxonomies[1] . 's', 'taxonomy general name'),
                        'singular_name' => _x($this->taxonomies[1], 'taxonomy singular name')
                    ),
                    'query_var'    => true,
                    'rewrite'      => array(
                        'slug'       => $this->postTypeID, // This controls the base slug that will display before each term
                        'with_front' => false // Don't display the category base before
                    )
                )
            );
        }

        /**
         * Register Post Type
         */
        public function foxRegisterPostType() {
            // Set UI labels for Custom Post Type
            $labels = array(
                'name'               => __($this->multipleName),
                'singular_name'      => __($this->singularName),
                'menu_name'          => __($this->singularName),
                'all_items'          => __('All ' . $this->multipleName),
                'view_item'          => __('View ' . $this->singularName),
                'add_new_item'       => __('Add New ' . $this->singularName),
                'add_new'            => __('Add New'),
                'edit_item'          => __('Edit ' . $this->singularName),
                'update_item'        => __('Update ' . $this->singularName),
                'search_items'       => __('Search ' . $this->singularName),
                'not_found'          => __('Not Found'),
                'not_found_in_trash' => __('Not found in Trash'),
            );

            // Set other options for Custom Post Type
            $args = array(
                'label'               => __($this->singularName),
                'description'         => __($this->multipleName),
                'labels'              => $labels,
                'public'              => true,
                // Features this CPT supports in Post Editor
                'supports'            => $this->supports,

                /* A hierarchical CPT is like Pages and can have
                   * Parent and child items. A non-hierarchical CPT
                   * is like Posts.
                   */
                'hierarchical'        => false,
                //'public'              => true,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'show_in_nav_menus'   => true,
                'show_in_admin_bar'   => true,
                'menu_position'       => 5,
                'can_export'          => true,
                'has_archive'         => true,
                'exclude_from_search' => false,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
                'menu_icon'           => $this->icon,
                //'rewrite'             =>  array('slug' => 'property/%subUrl%/%subUrl2%/%subUrl3%')
            );
            if ($this->taxonomies[0]) {
                $args['taxonomies'] = array(str_replace(' ', '_', strtolower($this->taxonomies[1])) . '_category', 'features');
            }

            // Registering your Custom Post Type
            register_post_type($this->postTypeID, $args);
        }
    }