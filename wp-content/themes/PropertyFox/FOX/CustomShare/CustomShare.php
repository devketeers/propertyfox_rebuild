<?php

    namespace FOX\CustomShare;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class CustomShare
     * @package FOX\CustomShare
     */
    class CustomShare {

        public $globalLink;

        /**
         * CustomShare constructor.
         */
        public function __construct() {
            add_action('wp_footer', array($this, 'customScripts'));
            add_shortcode('foxSharing', array($this, 'socialMediaButtons'));
        }

        /**
         * Social Media Buttons
         *
         * @param bool $mail
         * @param bool $facebook
         * @param bool $linkedin
         * @param bool $twitter
         * @param bool $google
         * @param bool $whatsapp
         * @return string
         */
        static function socialMediaButtons($mail = false, $facebook = false, $linkedin = false, $twitter = false, $google = false, $whatsapp = false) {
            global $post;
            (new self);

            $globalLink = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            $directory = get_template_directory_uri() . '/FOX/CustomShare/img';

            $content = '<section class="fox_sharing">';
            $content .= '<ul>';
            $content .= '<li><p>Share:</p></li>';
            $content .= ($mail ? '<li><img data-title="' . $post->post_title . '" class="foxMail" data-href="' . $globalLink . '" src="' . $directory . '/mail.png" /></li>' : '');
            $content .= ($facebook ? '<li><img data-title="' . $post->post_title . '" class="foxFacebook" data-href="' . $globalLink . '" src="' . $directory . '/facebook.png" /><span class="facebookShares foxShareCount"></span></li>' : '');
            $content .= ($linkedin ? '<li><img data-title="' . $post->post_title . '" class="foxLinkedin" data-href="' . $globalLink . '" src="' . $directory . '/linkedin.png" /><span class="linkedInShares foxShareCount"></span></li>' : '');
            $content .= ($twitter ? '<li><img data-title="' . $post->post_title . '" class="foxTwitter" data-href="' . $globalLink . '" src="' . $directory . '/twitter.png" /><span class="twitterShares foxShareCount"></span></li>' : '');
            $content .= ($google ? '<li><img data-title="' . $post->post_title . '" class="foxGoogle" data-href="' . $globalLink . '" src="' . $directory . '/google.png" /></li>' : '');
            $content .= ($whatsapp ? '<li><img data-title="' . $post->post_title . '" class="foxWhatsapp" data-href="' . $globalLink . '" src="' . $directory . '/whatsapp.png" /></li>' : '');

            $content .= '</ul>';
            $content .= '</section>';

            return $content;
        }

        /**
         * Custom Scripts
         */
        public function customScripts() {
            wp_enqueue_style('fox-share-buttons-css', get_template_directory_uri() . '/FOX/CustomShare/css/customShare.css');
            wp_enqueue_script('custom-share', get_template_directory_uri() . '/FOX/CustomShare/js/customShare.js', array(), '1.0.0', true);
        }
    }