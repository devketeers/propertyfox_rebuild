<?php

    /**
     * use FOX\ImageDownload\ImageDownload;
     * $image = new ImageDownload();
     * $image->updateImages($id);
     */

    namespace FOX\ImageDownload;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class ImageDownload
     * @package FOX\ImageDownload
     */
    class ImageDownload {

        /**
         * ImageDownload constructor.
         */
        public function __construct() { }

        /**
         * @param $url
         */
        public function grabImage($url, $id) {
            $uploadDir = wp_upload_dir();
            $saveTo = $uploadDir['path'] . '/' . str_replace(' ', '-', utf8_decode(urldecode(basename($url))));
            if (!file_exists($saveTo)) {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                $raw = curl_exec($ch);
                curl_close($ch);

                $fp = fopen($saveTo, 'x');
                fwrite($fp, $raw);
                fclose($fp);

                if ($fp) {
                    $attachment = $this->attachmentSave($saveTo, $id);
                    update_post_meta($id, 'fox_property_images_all_images', $attachment);
                }
            }
        }

        /**
         * @param $id
         */
        public function updateImages($id) {
            $uploadDir = wp_upload_dir();
            $media = explode(";", get_post_meta($id, "media", true));
            $galleryImages = [];
            foreach ($media as $image) {
                $saveTo = $uploadDir['path'] . '/' . str_replace(' ', '-', utf8_decode(urldecode(basename($image))));;
                if (!file_exists($saveTo)) {
                    $ch = curl_init($image);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    $raw = curl_exec($ch);
                    curl_close($ch);

                    $fp = fopen($saveTo, 'x');
                    fwrite($fp, $raw);
                    fclose($fp);

                    if ($fp) {
                        $attachment = $this->attachmentSave($saveTo, $id);
                        $galleryImages[] = $attachment;
                    }
                }
            }
            update_post_meta($id, 'fox_property_images_all_images', implode(',', $galleryImages));
        }

        /**
         * @param $fileName
         * @param $postId
         * @return int|\WP_Error
         */
        public function attachmentSave($fileName, $postId) {
            // $filename should be the path to a file in the upload directory.
            $filename = $fileName;

            // The ID of the post this attachment is for.
            $parent_post_id = $postId;

            // Check the type of file. We'll use this as the 'post_mime_type'.
            $filetype = wp_check_filetype(basename($filename), NULL);

            // Get the path to the upload directory.
            $wp_upload_dir = wp_upload_dir();

            // Prepare an array of post data for the attachment.
            $attachment = array(
                'guid'           => $wp_upload_dir['url'] . '/' . basename($filename),
                'post_mime_type' => $filetype['type'],
                'post_title'     => preg_replace('/\.[^.]+$/', '', basename($filename)),
                'post_content'   => '',
                'post_status'    => 'inherit'
            );

            // Insert the attachment.
            $attach_id = wp_insert_attachment($attachment, $filename, $parent_post_id);

            // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            // Generate the metadata for the attachment, and update the database record.
            $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
            wp_update_attachment_metadata($attach_id, $attach_data);

            // If post doesn't have a featured image
            if (!has_post_thumbnail($parent_post_id)) {
                set_post_thumbnail($parent_post_id, $attach_id);
            }

            return $attach_id;
        }
    }