<?php

    namespace Sections;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class Sections
     * @package FOX\Sections
     */
    class Sections {
        public $title, $unique;

        /**
         * Frontend output
         *
         * @param $data
         * @param $count
         */
        public function frontEnd($data, $count) { }

        /**
         * Backend edit screen
         */
        public function editScreen() { }
    }