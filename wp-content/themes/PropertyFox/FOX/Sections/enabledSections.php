<?php

    namespace Sections;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class enabledSections
     * @package FOX\enabledSections
     */
    class enabledSections {
        public $sections = array();

        /**
         * enabledSections constructor.
         */
        public function __construct() {
            add_action('wp_ajax_fox_ajax_section', array($this, 'fox_ajax_section'));
            add_action('wp_ajax_nopriv_section_frontend', array($this, 'fox_frontendAjax'));
            add_action('wp_ajax_section_frontend', array($this, 'fox_frontendAjax'));
        }

        /**
         * @param $field
         * @return array
         */
        public function enabledSections($field) {
            foreach ($field['enabledSections'] as $key => $val) {
                $class = '\Sections\\' . $val . '\\' . $val;
                if (class_exists($class)) {
                    $this->sections[] = new $class;
                }
            }
            return $this->sections;
        }

        /**
         * @param $data
         */
        public function display_sections($data) {
            foreach ($data as $key => $val) {
                $done = false;
                foreach ($this->sections as $keySections => $valSections) {
                    if ($valSections->unique == $val['type']) {
                        $valSections->frontend($val, count($data));
                        $done = true;
                    }
                }
                if (!$done) {
                    $class = '\Sections\\' . $val['type'] . '\\' . $val['type'];
                    $x = new $class();
                    $x->frontend($val, count($data));
                    $this->sections[] = $x;
                }
            }
        }

        /**
         * Backend AJAX sections load
         */
        function fox_ajax_section() {
            foreach ($_POST['data'] as $key => $val) {
                $_POST['data'][$key] = htmlentities(stripslashes($val));
            }
            $class = '\Sections\\' . $_POST['data']['type'] . '\\' . $_POST['data']['type'];
            $x = new $class();
            $x->editscreen();
            exit;
        }

        /**
         * Frontend AJAX for edit
         */
        function fox_frontendAjax() {
            $done = false;
            foreach ($this->sections as $key => $val) {
                if ($_POST['type'] == $val['type']) {
                    $val->$_POST['function']($_POST);
                    $done = true;
                }

            }
            if (!$done) {
                $class = '\Sections\\' . $_POST['type'] . '\\' . $_POST['type'];
                $x = new $class();

                $funcName = $_POST['function'];
                $x->$funcName();
                $this->sections[] = $x;
            }
            exit;
        }
    }