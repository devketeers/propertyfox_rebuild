<?php

    namespace Sections\ExampleSection;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    use Sections\Sections;

    /**
     * Class ExampleSection
     * @package FOX\ExampleSection
     */
    class ExampleSection extends Sections {
        public $title, $unique;

        /**
         * ExampleSection constructor.
         */
        public function __construct() {
            $this->title = 'Example';
            $this->unique = 'Example';
            add_action('wp_footer', array($this, 'loadScripts'));
            add_action('wp_footer', array($this, 'backendScripts'));
        }

        /**
         * Frontend output
         *
         * @param $data
         * @param $count
         */
        public function frontEnd($data, $count) {
            if ($data['news'] == 'yes') {
                $args = array(
                    'posts_per_page'   => 3,
                    'cat'              => $data['category'],
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                    'suppress_filters' => true
                );
                $news = new \WP_Query($args);
            }
            require(__DIR__ . '/template/frontend.php');
        }

        /**
         * Backend edit screen
         */
        public function editScreen() {
            $attachment = get_post($_POST['data']['image']);
            echo '<table width="100%">';
            echo '<tr>';
            echo '<th>Banner Title</th>';
            echo '<td><textarea name="' . $_POST['data']['fieldid'] . '[' . $_POST['data']['order'] . '][title]" rows="4" cols="50">' . $_POST['data']['title'] . '</textarea></td>';
            echo '</table>';
        }

        /**
         * Ajax return response
         */
        public function ajaxReturn() { }

        /**
         * @return mixed|void
         */
        static function frontendScripts() {
            if (is_admin()) {
                wp_enqueue_media();
                wp_register_script('gallery-item-section', str_replace('\\', '/', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))) . '/scripts/scripts.backend', array('jquery'));
                wp_enqueue_script('gallery-item-section');
            }
        }

        /**
         * @return mixed|void
         */
        static function backendScripts() {
            if (is_admin()) {
                wp_enqueue_media();
                wp_register_script('gallery-item-section', str_replace('\\', '/', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))) . '/scripts/scripts.backend', array('jquery'));
                wp_enqueue_script('gallery-item-section');
            }
        }
    }