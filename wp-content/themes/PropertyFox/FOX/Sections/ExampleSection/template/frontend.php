<section class="section-banner">
    <div class="slider-container">
        <div class="banner-slide">
            <?php $banner_images = explode(',', $data['image']); ?>
            <?php foreach ($banner_images as $image) { ?>
                <img src="<?php echo '/' . CSI\Image\ImageResizer::imageResize($image, 1900, 1200, true, array()) ?>">
            <?php } ?>
        </div>
        <div class="cf"></div>
    </div>
    <div class="grid container">
        <?php if ($data['title']) { ?>
            <div class="feature-message">
                <div class="filler"></div>
                <div class="sm-1">
                    <h1><?php echo nl2br($data['title']) ?></h1>
                </div>
            </div>
        <?php } ?>
        <?php
            if ($data['news'] == 'yes') {
                if ($news->have_posts()) {
                    require 'news.php';
                }
            }
        ?>
    </div>
    <?php if ($data['feature'] == 'yes') {
        require 'blocks.php';
    } ?>
</section>