<?php
    $status = preg_split("/\r\n|\n|\r/", shell_exec("git status 2>&1"));
    $history = preg_split("/\r\n|\n|\r/", shell_exec('git log --pretty=format:"Name: %an <br> Email: %ae <br>Commit: %s"'));
    if ($_GET['git-pull']) {
        //shell_exec("git pull");
    }
?>

<div class="foxplug-header">
    <h1>Git Status and Pull</h1>
</div>
<div id="poststuff" class="wrap fox-foxplug">
    <div class="foxplug-form postbox">
        <h3 class="handle">Git Status: <?php echo $status[0]; ?></h3>
        <p><?php echo $status[1]; ?></p>
        <?php if (strpos($status[4], 'not staged') !== false) { ?>
            <p>There are unstaged changes.</p>
        <?php } ?>
        <h4>Last Commit Details:</h4>
        <p><?php echo $history[0]; ?></p>
    </div>
    <?php //@Todo git pull - issue here is you need a password and username. Or set it up with global ssh key on the server which than causes new security issues. ?>
    <div class="foxplug-form postbox">
        <h3 class="handle">Git Pull: Offline</h3>
        <form method="get" action="<?php echo $_SERVER['HTTP_REFERER']; ?>&this=1">
            <input type="hidden" name="page" value="fox-foxplug-gitoptions" />
            <input type="hidden" name="git-pull" value="true" />
            <div class="options">
                <h4>Please take note!</h4>
                <p>Please check with the developer before doing a GIT PULL.</p>
                <p>Developers details can be found in the Git Status block.</p>
            </div>
            <?php submit_button('Git Pull'); ?>
        </form>
        <!--<?php /*add_thickbox(); */ ?>
        <a href="#TB_inline?width=600&height=550&inlineId=modal-window-id" class="thickbox">Modal Me</a>
        <div id="modal-window-id" style="display:none;">
            <p>Lorem Ipsum sit dolla amet.</p>
        </div>-->
    </div>
    <div class="clear"></div>
</div>