<div class="foxplug-header">
    <h1>FoxPlug Site Options</h1>
</div>
<ul class="tabs">
    <li>
        <input type="radio" name="tabs" class="radio-tab" id="tab1" checked />
        <label for="tab1">Fox Plug</label>
        <div id="tab-content1" class="tab-content">
            <div id="poststuff" class="wrap fox-foxplug">
                <div class="foxplug-form postbox">
                    Content
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </li>
    <li>
        <input type="radio" class="radio-tab" name="tabs" id="tab2" />
        <label for="tab2">Site Options</label>
        <div id="tab-content2" class="tab-content">
            <?php include('view/sp_site_options.php'); ?>
        </div>
    </li>
</ul>
