<?php

    namespace FOX\SPAdmin;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class SPDashboard
     * @package FOX\SPDashboard
     */
    class SPDashboard {

        protected static $spdashboard = NULL;

        /**
         * SPDashboard constructor.
         */
        public function __construct() {
            add_action('admin_init', array($this, 'remove_dashboard_meta'));
        }

        /**
         * @return SPDashboard|null
         */
        static function spdashboard() {
            if (self::$spdashboard === NULL) {
                self::$spdashboard = new SPDashboard();
            }
            return self::$spdashboard;
        }

        /**
         * Set widgets to remove
         */
        public function remove_dashboard_meta() {
            remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
            remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
            remove_meta_box('dashboard_primary', 'dashboard', 'side');
            remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
            //remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
            //remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
            //remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
            //remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
            //remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
        }
    }