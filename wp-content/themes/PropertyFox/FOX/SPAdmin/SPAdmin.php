<?php

    namespace FOX\SPAdmin;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class SPAdmin
     * @package FOX\SPAdmin
     */
    class SPAdmin {
        protected static $spadmin = NULL;

        /**
         * SPAdmin constructor.
         */
        public function __construct() {
            add_action('admin_menu', array($this, 'fox_admin_menu'));
            if (!file_exists(ABSPATH . '/wp-admin/.htaccess')) {
                add_action('admin_notices', array($this, 'fox_admin_notices'));
            }
        }

        /**
         * @return SPAdmin|null
         */
        static function spadmin() {
            if (self::$spadmin === NULL) {
                self::$spadmin = new SPAdmin();
            }
            return self::$spadmin;
        }

        /**
         * Admin menu items
         */
        public function fox_admin_menu() {
            add_menu_page(
                'FoxPlug Info',
                'FoxPlug',
                'manage_options',
                'fox-foxplug',
                array($this, 'fox_sp_info'),
                'dashicons-feedback');

            add_submenu_page(
                'fox-foxplug',
                'FoxPlug Security',
                'Security',
                'manage_options',
                'fox-foxplug-security',
                array($this, 'fox_sp_security')
            );

            add_submenu_page(
                'fox-foxplug',
                'FoxPlug Git',
                'Git Status and Pull',
                'manage_options',
                'fox-foxplug-gitoptions',
                array($this, 'fox_git_options')
            );

            add_action('Fox Plug Menu', 'fox_admin_menu');
        }

        /**
         * Info Admin Template
         */
        public function fox_sp_info() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }
            include('Templates/sp_info.php');
        }

        /**
         * Security Admin Template
         */
        public function fox_sp_security() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }
            $current = get_option('fox_htlocation');
            if (isset($_POST['htoptions'])) {
                if ($current) {
                    update_option('fox_htlocation', $_POST['htoptions']['path']);
                    $current = get_option('fox_htlocation');
                } else {
                    add_option('fox_htlocation', $_POST['htoptions']['path'], '', $autoload = 'no');
                    $current = get_option('fox_htlocation');
                }
            }
            include('Templates/sp_admin_security.php');
        }

        /**
         * GIT Status Admin Template
         */
        public function fox_git_options() {
            if (!current_user_can('manage_options')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }
            include('Templates/fp_git_status.php');
        }

        /**
         * Admin Notices
         */
        public function fox_admin_notices() { ?>
            <div class="notice notice-warning is-dismissible">
                <p><strong>FoxPlug Security</strong></p>
                <p>This installation has not been secured. Please
                    <a href="admin.php?page=fox-foxplug-security">click here</a> to find out how to secure this installation.
                </p>
                <button type="button" class="notice-dismiss">
                    <span class="screen-reader-text">Dismiss this notice.</span>
                </button>
            </div>
            <?php
        }
    }

