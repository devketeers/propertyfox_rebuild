<?php

    namespace FOX\Widgets;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    };

    /**
     * Class contactWidget
     * @package FOX\contactWidget
     */
    class contactWidget extends \WP_Widget {
        function __construct() {
            parent::__construct('contact_widget', __('Contact Widget', 'contact_widget'), array('description' => __('Display contact details for Litnet', 'contact_widget'),));
        }

        /*
         * Widget Frontend Code
         */
        public function widget($args, $instance) {
            $title = apply_filters('widget_title', $instance['title']);
            // START OUTPUT
            echo $args['before_widget'];
            if (!empty($title)) {
                echo '<span class="mail">' . \FOX\Translator\Translate::t()->translate($title) . ' : ';
            }
            if (isset($instance['email'])) {
                echo '<a href="mailto:' . $instance['email'] . '">' . $instance['email'] . '</a></span>';
            }
            if (isset($instance['telephone'])) {
                $clean = str_replace(array(
                    '(',
                    ')',
                    " "
                ), '', $instance['telephone']);
                echo '<span class="tel">T : <a href="tel:' . $clean . '">' . $instance['telephone'] . '</a></span>';
            }
            echo $args['after_widget'];
        }

        /*
         * Widget Backend Functions
         */
        public function form($instance) {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Contact Presse', 'contact_widget');
            }
            // Widget admin form
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo esc_attr($instance['email']); ?>" placeholder="example@company.com" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('telephone'); ?>"><?php _e('Telephone:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('telephone'); ?>" name="<?php echo $this->get_field_name('telephone'); ?>" type="text" value="<?php echo esc_attr($instance['telephone']); ?>" placeholder="012 345 6789" />
            </p>
            <?php
        }

        // Updating widget replacing old instances with new
        public function update($new_instance, $old_instance) {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
            $instance['email'] = (!empty($new_instance['email'])) ? strip_tags($new_instance['email']) : '';
            $instance['telephone'] = (!empty($new_instance['telephone'])) ? strip_tags($new_instance['telephone']) : '';

            return $instance;
        }
    }