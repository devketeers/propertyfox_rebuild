<?php

    namespace FOX\Fields\RadioSelector;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class RadioSelector
     * @package FOX\RadioSelector
     */
    class RadioSelector implements \FOX\Fields\Field {

        /**
         * RadioSelector constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';

            if (array_key_exists('options', $field)) {
                foreach ($field['options'] as $key => $val) {
                    $html .= '<input type="radio" name="' . $field['id'] . '" id="' . $field['id'] . '" ' . ($meta == $val ? "checked='checked'" : "") . 'value="' . $val . '" size="30" /><span class="description">' . $val . '</span><br/>';
                }
            } else {

                $html .= '<input type="radio" name="' . $field['id'] . '" id="' . $field['id'] . '" ' . ($meta == 1 ? "checked='checked'" : "") . 'value="1" size="30" /><span class="description">Yes</span><br/>';
                $html .= '<input type="radio" name="' . $field['id'] . '" id="' . $field['id'] . '" ' . ($meta == 0 ? "checked='checked'" : "") . 'value="0" size="30" /><span class="description">No</span><br />';
            }
            $html .= '</td></tr>';
            return $html;
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }