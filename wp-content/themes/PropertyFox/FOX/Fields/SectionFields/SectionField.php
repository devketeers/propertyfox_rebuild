<?php

    namespace FOX\Fields\SectionField;

    use Sections\enabledSections;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class SectionField
     * @package FOX\SectionField
     */
    class SectionField implements \FOX\Fields\Field {

        /**
         * CustomWidgets constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return mixed|void
         */
        public static function display($post, $field) {
            $sections = new enabledSections($field);
            $sections->enabledSections($field);
            echo '<script>var options' . $field['id'] . ' = new Array();</script>';
            echo "<script>options" . $field['id'] . ".push({'title': 'Please Select', 'value': 'null'});</script>";

            foreach ($sections->sections as $key => $val) {
                echo "<script>options" . $field['id'] . ".push({'title': '" . $val->title . "', 'value': '" . $val->unique . "'});</script>";
            }
            $meta = get_post_meta($post->ID, $field['id'], true);
            $data = array();

            if ($meta) {
                ksort($meta);
                $current = count($meta) + 1;
                foreach ($meta as $key => $val) {
                    if (is_numeric($val['order'])) {
                        $data[$val['order']] = $val;
                    } else {
                        $data[$current] = $val;
                        $current++;
                    }
                }
            }

            ksort($data);
            //Fixes the array keys so sections load properly
            $new_array = array_values($data);
            ?>
            <script>
                if (!customWidgets) {
                    var customWidgets = [];
                }
                customWidgets['<?php echo $field['id'] ?>'] = <?php echo json_encode($new_array) ?>;
            </script>
            <div class="fox-builder-window" id="builder-<?php echo $field['id'] ?>" data-fieldid="<?php echo $field['id'] ?>">
                <input type="hidden" value="" name="<?php echo $field['id'] ?>" />
                <div class="fox-builder-tools">
                    <button class="add-new-section" data-customfieldid="<?php echo $field['id'] ?>">Add Section</button>
                </div>
                <div class="fox-builder-sections">
                    <ul>
                    </ul>
                </div>
                <div class="fox-builder-tools">
                    <button class="add-new-section" data-customfieldid="<?php echo $field['id'] ?>">Add Section</button>
                </div>
            </div>
            <?php
        }

        /**
         * @param $post
         * @param $field
         * @return mixed|void
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() {
            if (is_admin()) {
                wp_enqueue_script('jquery-ui-sortable');
                wp_register_script('fox-section-field', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT'])) . '/SectionField.js', array('jquery-ui-sortable'), false, true);
                wp_enqueue_script('fox-section-field');
            }
        }
    }