jQuery(document).ready(function () {
    jQuery('.fox-builder-sections ul').each(function (x) {
        jQuery(this).sortable({
            'items': 'li',
            'axis': 'y',
            'helper': fixHelper,
            'update': function (e, ui) {
                var sorted = jQuery(this).children();
                jQuery(sorted).each(function (e) {
                    console.log(e);
                    jQuery(sorted[e]).children('.section-position').val(e);
                })
            }
        });
    });

    jQuery(document).on('click', '.add-new-section', function (e) {
        e.preventDefault();
        var customID = jQuery(this).data('customfieldid');
        addWidget(customID);
    });

    var fixHelper = function (e, ui) {
        ui.children().children().each(function () {
            jQuery(this).width(jQuery(this).width());
        });
        return ui;
    };
    // customWidgets var is made in CustomWidgets.php

    jQuery('.fox-builder-window').each(function (i) {
        var fieldID = jQuery(this).data('fieldid');
        var content = customWidgets[fieldID];

        jQuery(content).each(function (d) {
            addWidget(fieldID, content[d]);
        })
    });

    jQuery(document).on('change', '.fox-type-selector', function () {
        getTypeOptions(jQuery(this))
    });

    jQuery('.delete-section').on('click', function (e) {
        e.preventDefault();
        jQuery(this).parent().remove()
    });

});

function addWidget(fieldID, data) {
    var dataString = '';
    var append = jQuery('<li style="padding:10px;border:1px solid orange;"><span style="float:right;color:red;cursor: pointer;" class="delete-section">x</span></li>');
    if (data) {

        for (var prop in data) {
            // skip loop if the property is from prototype
            if (!data.hasOwnProperty(prop)) continue;
            jQuery(append).attr('data-' + prop, data[prop]);
        }
    }
    if (jQuery(append).data('order') === undefined) {
        jQuery(append).attr('data-order', jQuery('#builder-' + fieldID + ' .fox-builder-sections ul').children().length);
    }

    jQuery(append).attr('data-fieldid', fieldID);

    var selectoptions = '';

    // options Field ID Made is CustomWidgets.php
    var temp = window['options' + fieldID];
    jQuery(temp).each(function (i) {
        if (data) {
            selectoptions += '<option ' + (data.type && data.type == temp[i].value ? 'selected="selected"' : '') + ' value="' + temp[i].value + '">' + temp[i].title + '</option>';
        } else {
            selectoptions += '<option value="' + temp[i].value + '">' + temp[i].title + '</option>';
        }
    });

    jQuery(append).append('<input class="section-position" type="hidden" name="' + fieldID + '[' + jQuery('#builder-' + fieldID + ' .fox-builder-sections ul').children().length + '][order]" value="' + jQuery('#builder-' + fieldID + ' .fox-builder-sections ul li').length + '" size="30" />');

    jQuery(append).append('<table class="form-table"><tr><th><label>Select Content Type</label></th><td><select class="fox-type-selector" name="' + fieldID + '[' + jQuery('#builder-' + fieldID + ' .fox-builder-sections ul').children().length + '][type]">' + selectoptions + '</select></td></tr></table>');

    if (data) {
        data.action = 'fox_ajax_section';
        data.data = jQuery(append).data();
        jQuery.ajax({
            url: 'admin-ajax.php',
            type: 'post',
            data: data,
            success: function (response) {
                jQuery(append).append('<table class="form-table response-item">' + response + '</table>');
            }
        });
    } else {
        jQuery(append).append('<table class="form-table response-item"></table>');
    }
    jQuery('#builder-' + fieldID + ' .fox-builder-sections ul').append(jQuery(append));
}

function getTypeOptions(type) {
    var data = new Object();
    data.action = 'fox_ajax_section';
    data.data = jQuery(type).parents('li').data();
    data.data.type = jQuery(type).val();
    jQuery.ajax({
        url: 'admin-ajax.php',
        type: 'post',
        data: data,
        success: function (response) {
            jQuery(type).parents('li').children('.response-item').html(response);
        }
    });
}