<?php

    namespace FOX\Fields\WYSIWYG;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class WYSIWYG
     * @package FOX\WYSIWYG
     */
    class WYSIWYG implements \FOX\Fields\Field {

        /**
         * WYSIWYG constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return string
         */
        public
        static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            echo '';
            echo '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th>';
            echo '<td>';
            wp_editor($meta, $field['id']);
            echo '</td>';
            echo '</tr>';
            return '';
        }

        /**
         * @param $post
         * @param $field
         */
        public
        static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }