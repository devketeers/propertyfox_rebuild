jQuery('.remove_image_button').live('click', function (event) {
    var field;
    var img;
    event.preventDefault();
    field = jQuery(this).prev().prev();
    img = jQuery(this).prev().prev().prev().prev();
    jQuery(img).empty();
    jQuery(field).val('');
});

jQuery('.upload_image_button').live('click', function (event) {
    var uploader;
    var field_id;
    event.preventDefault();
    field_id = jQuery(this).data('field_id');
    if (uploader) {
        uploader.open();
        return;
    }
    var uploader = wp.media(
        {
            title: jQuery(this).data('uploader_title'),
            button: {
                text: jQuery(this).data('uploader_button_text')
            },
            multiple: false
        })
        .on('select', function () {
            var selection = uploader.state().get('selection');
            attachment = uploader.state().get('selection').first().toJSON();
            console.log(attachment);
            jQuery('#' + field_id).val(attachment.id);
            if (attachment.type == 'image') {
                jQuery('.' + field_id + '_holder').html('<img style="width:200px;height:auto;" src="' + attachment.url + '" /><br/>' + attachment.title);
            } else {
                jQuery('.' + field_id + '_holder').html('<img src="' + attachment.icon + '" /><br/>' + attachment.title);
            }
            console.log(jQuery('#name_' + field_id));
            console.log(attachment);
        })
        .open();
});