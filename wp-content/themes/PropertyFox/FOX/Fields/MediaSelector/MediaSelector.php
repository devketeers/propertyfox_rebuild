<?php

    namespace FOX\Fields\MediaSelector;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class MediaSelector
     * @package FOX\MediaSelector
     */
    class MediaSelector implements \FOX\Fields\Field {

        /**
         * MediaSelector constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return mixed|string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';
            $html .= '<div class="fox_image_holder ' . $field['id'] . '_holder">';
            if ($meta) {
                $attachment = get_post($meta);
                if (isset($attachment)) {
                    if (isset($attachment->post_mime_type) && $attachment->post_mime_type == 'image/jpeg') {
                        $html .= '<img src="' . $attachment->guid . '" alt="' . $attachment->post_title . '"/>';
                    } else {
                        $postMeta = wp_get_attachment_image($attachment->ID, 'thumbnail', true, '');
                        $html .= $postMeta . '<br/>';
                        $html .= $attachment->post_title;
                    }
                }
            }
            $html .= '</div>';
            $html .= '<hr/>';
            $html .= '<input type="hidden" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" />';
            $html .= '<input type="button" id="meta-image-button" data-uploader_title="Add Media" data-uploader_button_text="Select File"  data-field_id="' . $field['id'] . '" class="button upload_image_button" value="Add/Change Image" />';
            $html .= '<input type="button" class="button remove_image_button" value="Remove Image" />';
            $html .= '</td>';
            $html .= '</tr>';

            return $html;
        }

        /**
         * @param $post
         * @param $field
         * @return mixed|void
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() {
            if (is_admin()) {
                wp_enqueue_media();
                wp_register_script('custom-posts-media', str_replace('\\', '/', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))) . '/custom-posts-media-single.js', array('jquery'));
                wp_enqueue_script('custom-posts-media');
            }
        }
    }