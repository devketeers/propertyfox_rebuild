<?php

    namespace FOX\Fields\TextArea;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class TextArea
     * @package FOX\TextArea
     */
    class TextArea implements \FOX\Fields\Field {

        /**
         * TextArea constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';
            $html .= '<textarea name="' . $field['id'] . '" id="' . $field['id'] . '" rows=5 cols=80>' . $meta . '</textarea><br /><span class="description">' . $field['desc'] . '</span>';
            $html .= '</td></tr>';
            return $html;
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }