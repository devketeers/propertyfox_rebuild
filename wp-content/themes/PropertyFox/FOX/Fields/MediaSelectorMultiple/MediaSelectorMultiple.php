<?php

    namespace FOX\Fields\MediaSelectorMultiple;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class MediaSelectorMultiple
     * @package FOX\MediaSelectorMultiple
     */
    class MediaSelectorMultiple implements \FOX\Fields\Field {

        /**
         * MediaSelectorMultiple constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $images = explode(',', $meta);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';
            $html .= '<div class="multiple-images" id="images_' . $field['id'] . '">';
            if ($images) {
                foreach ($images as $key => $val) {
                    if ($val) {
                        $attachment = get_post($val);
                    }
                    if (isset($attachment)) {
                        $postMeta = wp_get_attachment_image($attachment->ID);
                        $html .= '<div class="single-image">';
                        $html .= $postMeta;
                        //$html .= '<span class="description">'.$attachment->post_title.'</span>';
                        $html .= '</div>';
                    }
                }
            }
            $html .= '</div>';
            $html .= '<hr style="clear:both;"/>';
            $html .= '<input type="hidden" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" />';
            $html .= '<input type="button" id="meta-image-button" data-uploader_title="Add Media" data-uploader_button_text="Select File"  data-field_id="' . $field['id'] . '" class="button upload_multiple_image_button" value="Add/Change Images" />';
            $html .= '<button class="remove_image_button button">Remove All Images</button>';
            $html .= '</td>';
            $html .= '</tr>';

            return $html;
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() {
            if (is_admin()) {
                wp_enqueue_media();
                wp_register_script('custom-posts-media-multiple', str_replace('\\', '/', substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))) . '/custom-posts-media-multiple.js', array('jquery'));
                wp_enqueue_script('custom-posts-media-multiple');
            }
        }
    }