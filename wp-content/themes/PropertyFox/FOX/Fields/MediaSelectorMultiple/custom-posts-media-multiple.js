jQuery('.upload_multiple_image_button').on('click', function (event) {
    event.preventDefault();
    var field_id = jQuery(this).data('field_id');

    if (uploader) {
        uploader.open();
        return;
    }

    var uploader = wp.media(
        {
            title: 'Use this media',
            button: {
                text: 'Use this media',
            },
            multiple: 'add'
        }).on('select', function () {
        var selection = uploader.state().get('selection').toJSON();
        var hiddenIds = [];
        var htmlImages = '';
        for (var key in selection) {
            hiddenIds.push(selection[key].id);
            if (selection[key].type === 'image' && selection[key].url !== '') {
                htmlImages += '<div class="single-image"><img src="' + selection[key]['sizes']['thumbnail'].url + '" /></div>';
            }
        }
        console.log(hiddenIds);
        jQuery('#images_' + field_id).html(htmlImages);
        jQuery('#' + field_id).val(hiddenIds.filter(function (v) {
            return v !== ''
        }).join(','));
    });

    if (jQuery('#' + field_id).length) {
        uploader.on('open', function () {
            var selection = uploader.state().get('selection');
            var ids = jQuery('#' + field_id).val().split(',');
            ids.forEach(function (id) {
                var attachment = wp.media.attachment(id);
                selection.add(attachment ? [attachment] : []);
            });
        });
    }

    uploader.open();

});

jQuery('.remove_image_button').on('click', function (event) {
    event.preventDefault();
    var field_id = jQuery(this).prev().data('field_id');
    jQuery('#' + field_id).val('');
    jQuery(this).prev().prev().prev().prev().html('');
});