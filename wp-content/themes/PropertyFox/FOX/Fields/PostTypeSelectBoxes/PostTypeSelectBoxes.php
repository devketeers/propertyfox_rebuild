<?php

    namespace FOX\Fields\PosttypeSelectBoxes;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class PostTypeSelectBoxes
     * @package FOX\PostTypeSelectBoxes
     */
    class PostTypeSelectBoxes implements \FOX\Fields\Field {

        /**
         * PostTypeSelectBoxes constructor.
         */
        public function __construct() { }

        /**
         * Please pass in $opt postType = The post type you would like to list here
         *
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';

            $args = array(
                'posts_per_page'   => -1,
                'orderby'          => 'post_title',
                'order'            => 'DESC',
                'post_type'        => $field['postType'],
                'post_status'      => 'publish',
                'suppress_filters' => true);
            $holder = get_posts($args);

            $html .= '<select name="' . $field['id'] . '">';
            $html .= '<option value="">Select</option>';
            if ($holder) {
                foreach ($holder as $individual) {
                    $html .= '<option value="' . $individual->ID . '" ' . ($meta == $individual->ID ? "selected='selected'" : "") . '/>' . $individual->post_title . '</option>';
                }
            }
            $html .= '</select>';
            $html .= '<br /><span class="description">' . $field['desc'] . '</span>';
            $html .= '</td></tr>';
            return $html;
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            if ($meta_values) {
                if (isset($meta_values) && is_array($meta_values)) {
                    $last = end(array_keys($meta_values));
                    foreach ($meta_values as $key => $value) {
                        $holder = get_post($key);
                        echo $holder->post_title;
                        if ($last != $key) {
                            echo ' | ';
                        }
                    }
                }
            }
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }