<?php

    namespace FOX\Fields\Text;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class Text
     * @package FOX\Text
     */
    class Text implements \FOX\Fields\Field {

        /**
         * Text constructor.
         */
        public function __construct() { }

        /**
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $meta = get_post_meta($post->ID, $field['id'], true);
            $html = '';
            $html .= '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';
            $html .= '<input type="text" name="' . $field['id'] . '" id="' . $field['id'] . '" value="' . $meta . '" size="30" /><br /><span class="description">' . $field['desc'] . '</span>';
            $html .= '</td></tr>';
            return $html;
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            echo $meta_values;
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }