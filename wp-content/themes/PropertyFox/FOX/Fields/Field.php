<?php

    namespace FOX\Fields;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }
    /**
     * Interface Field
     * @package FOX\Field
     */
    interface Field {

        /**
         * @param $post
         * @param $field
         * @return mixed
         */
        public static function display($post, $field);

        /**
         * @param $post
         * @param $field
         * @return mixed
         */
        public static function displayColumn($post, $field);

        /**
         * @return mixed
         */
        static function loadScripts();
    }