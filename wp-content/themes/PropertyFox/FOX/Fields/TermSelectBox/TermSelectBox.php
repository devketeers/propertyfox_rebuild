<?php

    namespace FOX\Fields\TermSelectBox;

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * Class TermSelectBox
     * @package FOX\TermSelectBox
     */
    class TermSelectBox implements \FOX\Fields\Field {

        /**
         * TermSelectBox constructor.
         */
        public function __construct() { }

        /**
         * Please pass in $opt postType = The post type you would like to list here
         * @param $post
         * @param $field
         * @return string
         */
        public static function display($post, $field) {
            $args = array(
                'hide_empty'       => false,
                'hierarchical'     => true,
                'taxonomy'         => $field['taxonomy'],
                'name'             => $field['id'],
                'show_option_none' => 'None'
            );
            $terms = get_terms($field['taxonomy'], $args);
            $meta = get_post_meta($post->ID, $field['id'], true);
            echo '<tr><th><label for="' . $field['id'] . '">' . $field['label'] . '</label></th><td>';
            wp_dropdown_categories($args);
            echo '</td></tr>';
        }

        /**
         * @param $post
         * @param $field
         */
        public static function displayColumn($post, $field) {
            $meta_values = get_post_meta($post->ID, $field['id'], true);
            if ($meta_values) {
                if (isset($meta_values)) {
                    $val = get_post($meta_values);
                    echo $val->post_title;
                }
            }
        }

        /**
         * @return mixed|void
         */
        static function loadScripts() { }
    }