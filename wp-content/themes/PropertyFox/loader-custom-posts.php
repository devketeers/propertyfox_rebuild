<?php
    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    /**
     * This file is used to create and load the custom post types. If you add a custom post type.
     * You will need to deactivate and re activate the theme. This will create for you the 2-3 templates you need.
     *
     * If custom tax set to false. Creates single-taxname and archive-taxname.
     *
     * If custom tax set to true. Creates single/archive and taxonomy template. Tax template is not edited it just points to archive.
     *
     * Adding custom field to page/built in posts
     *
     * $page = new FOX\CustomFields\CustomFields('page');
     * $page->addField('Call to Action', 'Enter a url for the call to action', 'Text', 'Additional Config', array(), false);
     * HERE IS AN EXAMPLE OF A FULLY FILLED IN CUSTOM POST ADDITION PLEASE READ UP ON WHAT IS REQUIRED AND NOT.
     * You can find a list of custom fields in the FOX/Fields directory or write your own by extending field.php
     *
     * Each field has 3 functions Read more in FOX/Fields/Field.php
     */

    $post = new FOX\CustomFields\CustomFields('post');
    $page = new FOX\CustomFields\CustomFields('page');

    $property = new FOX\CustomPosts\CustomPosts('property', 'Property', array(true, 'Location'), array(
        'title',
        'editor',
        'thumbnail',
        'custom-fields',
        'menu_position' => 1,
        'has_archive'   => false,
    ), array(
        'description'  => 'Add properties here',
        'icon'         => 'dashicons-admin-home',
        'archive-edit' => false,
    ));

    $property->addField('All Images', 'Gallery images for listings.', 'MediaSelectorMultiple', 'Property Images', array(), false);


/*    $sectionsExample->addField('Tags', 'Custom Layout', 'SectionField', 'Investment Tags', array(
        'enabledSections' => array(
            'ContentTags',
        ),
    ), false);*/