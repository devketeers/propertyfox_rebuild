<?php

    // Exit if accessed directly
    if (!defined('ABSPATH')) {
        exit;
    }

    $sideBars = new \FOX\Sidebars\Sidebars();

    $sideBars->addSideBar(array(
        'name'          => 'Listing Sidebar',
        'id'            => 'fox_sidebar',
        'before_widget' => '<div class="property-sidebar">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ));