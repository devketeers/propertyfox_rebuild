<?php
    /**
     * To be directed to this ajax handler
     */
    function fox_ajax() {
        switch ($_POST['task']) {
            case 'search' :

                //Search by post data
                $q1 = new WP_Query(array(
                    'post_type'      => 'property',
                    'posts_per_page' => -1,
                    's'              => stripslashes($_POST['search'])
                ));

                //Search by Meta
                $q2 = new WP_Query(array(
                    'post_type'      => 'property',
                    'posts_per_page' => -1,
                    'meta_query'     => array(
                        array(
                            'key'     => 'pf_number__c',
                            'value'   => stripslashes($_POST['search']),
                            'compare' => 'LIKE'
                        )
                    )
                ));

                $results = new WP_Query();
                $results->posts = array_unique(array_merge($q1->posts, $q2->posts), SORT_REGULAR);

                $items = [];
                if (!empty($results->posts)) {
                    foreach ($results->posts as $k => $result) {
                        $items[$k]['title'] = $result->post_title;
                    }
                }

                //Search Terms
                $terms = get_terms(array(
                    'taxonomy'   => array('location_category'), // taxonomy name
                    'orderby'    => 'id',
                    'order'      => 'ASC',
                    'hide_empty' => true,
                    'fields'     => 'names',
                    'name__like' => stripslashes($_POST['search'])
                ));

                $term = [];
                if (!empty($terms)) {
                    foreach ($terms as $k => $result) {
                        $term[$k]['title'] = $result;
                    }
                }

                $ajaxReturn = array_merge($term, $items);

                if (!empty($ajaxReturn)) {
                    echo json_encode(array_values($ajaxReturn));
                } else {
                    echo json_encode(array_values([['title' => 'No results found for: ' . $_POST['search']]]));
                }

                break;
            case 'suburbs':
                //Search Terms

                $province = term_exists($_POST['province'], 'location_category')['term_id'];

                $terms = get_terms(array(
                    'taxonomy'   => 'location_category',
                    'childless'  => true,
                    'orderby'    => 'names',
                    'order'      => 'ASC',
                    'hide_empty' => false,
                    'fields'     => 'all',
                    'name__like' => stripslashes($_POST['search'])
                ));

                $term = [];
                if (!empty($terms)) {
                    foreach ($terms as $k => $result) {
                        if (in_array($province, get_ancestors($result->term_id, 'location_category'))) {
                            $term[$k]['title'] = $result->name;
                        }
                    }
                }

                if (!empty($term)) {
                    echo json_encode(array_values($term));
                } else {
                    echo json_encode(array_values([['title' => 'No such suburb found in ' . $_POST['province']]]));
                }

                break;
            case 'delete':
                print_r($_POST);
                break;
            default :
                print_r($_POST);
                break;
        }
        wp_die();
    }

    add_action('wp_ajax_nopriv_fox_ajax', 'fox_ajax');
    add_action('wp_ajax_fox_ajax', 'fox_ajax');

    /**
     * @param $termName
     * @return int|object
     */
    function Parent($termName) {
        $termExist = term_exists($termName, 'location_category');
        if (!$termExist) {
            $term = wp_insert_term($termName, 'location_category');
            return $term['term_id'];
        } else {
            return $termExist['term_id'];
        }
    }

    /**
     * @param $child
     * @param $parent
     * @return int|object
     */
    function Child($parent, $child) {
        $termExist = term_exists($child, 'location_category', $parent);
        if (!$termExist) {
            $term = wp_insert_term($child, 'location_category', array('parent' => $parent));
            return $term['term_id'];
        } else {
            return $termExist['term_id'];
        }
    }