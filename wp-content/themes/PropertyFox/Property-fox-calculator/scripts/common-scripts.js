

;(function($){
	$(function(){
        
        if( $("div.property-price-info").length){
            // Traditional Agent Commission slider function
            $(function() {
                var progresSliderElement = $("#progressRange")
                var progressRangehandle = $("#progressRangehandle");
                progresSliderElement.slider({
                      range: "min",
                      min: 0,
                      max: 10,
                      value: 7,
                      create: function() {
                          progressRangehandle.text( progresSliderElement.slider( "value") + "%" );

                      },
                      slide: function( event, ui ) {
                        progressRangehandle.text( ui.value + "%");
                      },
                    change: function(){
                        var propertyPrice = $("#pricingRange").slider("value")
                        var tacPrcnt = $("#progressRange").slider( "value") / 100
                        var taCommission = tacPrcnt  * propertyPrice
                        var pfcPrcnt = 1.5 / 100
                        var pfCommision = pfcPrcnt * propertyPrice
                        var savePrice = Math.floor(taCommission -  pfCommision)
                        
                        netPrice.text( "R" +  savePrice)
                        
                    }
                });
       
                // Property price slider 
                var sliderElement = $("#pricingRange")
                var handle = $( "#custom-handle" );
                var netPrice = $("#netPrice")
                
                sliderElement.slider({
                  range: "max",
                  min: 2500000,
                  max: 10000000,
                  value: 1000000,
                  create: function() {
                      handle.text("R" + sliderElement.slider( "value" ) );

                  },
                  slide: function( event, ui ) {

                    $("#minRange").text( "R" + ui.value );
                    handle.text( "R" + ui.value );
                                     
                  },
                    change: function(){
                        var propertyPrice = $("#pricingRange").slider("value")
                        var tacPrcnt = $("#progressRange").slider( "value") / 100
                        var taCommission = tacPrcnt  * propertyPrice
                        var pfcPrcnt = 1.5 / 100
                        var pfCommision = pfcPrcnt * propertyPrice
        
                        var savePrice = Math.floor(taCommission -  pfCommision)
                        netPrice.text( "R" +  savePrice)
                        
                    }
                });
                $("#minRange").text("R" + sliderElement.slider("value") ); 
                
                var propertyPrice = $("#pricingRange").slider("value")
                var tacPrcnt = $("#progressRange").slider( "value") / 100
                var taCommission = tacPrcnt  * propertyPrice
                var pfcPrcnt = 1.5 / 100
                var pfCommision = pfcPrcnt * propertyPrice

                var savePrice = Math.floor(taCommission -  pfCommision)
                netPrice.text( "R" +  savePrice)
                
            });

        }

	})// End ready function.
    
})(jQuery)
