<?php

    $provinces = ['Eastern Cape', 'Free State', 'Gauteng', 'KwaZulu Natal', 'Limpopo', 'Mpumalanga', 'North West', 'Northern Cape', 'Western Cape'];

    //Read data from api
    if (($handle = fopen('https://www.property24.com/General/GetSuburbsCsv', 'r')) !== false) {
        $header = fgetcsv($handle); // Header remove
        $array_to_json = [];
        $array = [];
        while (($data = fgetcsv($handle)) !== false) {
            if ($data[0] != 'South Africa') {
                continue;
            }

            if (!in_array($data[1], $provinces)) {
                continue;
            }
            try {
                $array_to_json[$data[1]][$data[2]][] = $data[3];
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }

            unset($data);
        }
        fclose($handle);
    }

    //Create files
    foreach ($array_to_json as $province => $area) {
        $fileName = preg_replace('/[^A-z0-9]+/', '-', $province) . '.json';
        $provinceFile = fopen($fileName, 'w') or die("Can't create file");
        fwrite($provinceFile, json_encode($area));
        fclose($provinceFile);
    }